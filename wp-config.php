<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings ** //
/** The name of the database for WordPress */
define( 'DB_NAME', 'hoocak' );

/** MySQL database username */
define( 'DB_USER', 'root' );

/** MySQL database password */
define( 'DB_PASSWORD', 'mysql' );

/** MySQL hostname */
define( 'DB_HOST', 'localhost' );

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8mb4');

/** The Database Collate type. Don't change this if in doubt. */
define( 'DB_COLLATE', '' );

/**
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         '|jMdodE}zjmv5Oi6dTLd9mno/6AaQ0]qqj}=RB>y@Ico|H-PQNK0olPYu+pK_UI$');
define('SECURE_AUTH_KEY',  'i^)5_=K8:j+=ld>)Fc<L;I90iO<z*-N9-+$_Jl)*#9tIfFUDqb>I,sc_v0xQ=wJu');
define('LOGGED_IN_KEY',    '>WqVkL/.ZOJ4|_A5O<&]2|<^nW+6B-LsSBDQnn=i^Yd0+Mi I>Ej-%8CFjTDv6td');
define('NONCE_KEY',        'Ip0niXTn[X,K68A{!+Z|TW(eD;HB8P9?.>ESbk7 R!&;G*J%p3y{Sv8uRW]G|o9W');
define('AUTH_SALT',        'GE#!6O+j@2+xVSMVSMYl4{jt=@3Mm&VZ|1F@C#6=z<dzbvfY{hVGfZbwW-gKec9y');
define('SECURE_AUTH_SALT', 'Q+GLOsZlRz5cj8|3E(^j_RFR:k]lZkz_+D-Xes-_LQP-J|lZ^ba]NhNPH=r:/:p.');
define('LOGGED_IN_SALT',   'YyFYuk#xu^ozh`g-`MzuoV9mjE%zv46+M(jRp|<$$+i;I631ev]hnOQEYKxVX}o2');
define('NONCE_SALT',       'Cr`{5fs5s(j9;!``Qmsz6tK1&<6?[a`^N--t`hCZkbFyvXV$c(!_waG~09!Sf12:');

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix = 'wp_hck';

ini_set('display_errors','off');
ini_set('error_reporting', E_ALL );
define('WP_DEBUG', false);
define('WP_DEBUG_DISPLAY', false);


/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( ! defined( 'ABSPATH' ) )
	define( 'ABSPATH', dirname( __FILE__ ) . '/' );

/** Sets up WordPress vars and included files. */
require_once ABSPATH . 'wp-settings.php';
