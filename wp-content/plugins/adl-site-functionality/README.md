# Ad-Lit Site Functionality Plugin #

This is a plugin for containing all the code for a site that should be persistent between theme updates (custom post types & taxonomies, shortcodes, admin customizations, etc.).

## Installation ##

1. `git clone https://bitbucket.org/adlit/adl-site-functionality` in the `/wp-content/plugins/` directory
1. Activate the plugin through the 'Plugins' menu in WordPress

## Getting Started ##

1. Replace `::client name::` in `adl-site-functionality.php` with the client's name
2. Add url to client's logo to `assets/css/adl-admin-login.css`
3. Put all code that should not be changed if the theme were to change in the plugin

# Features #
* Use FontAwesome for CPT icons -- check out the example in `assets/css/adl-admin.css`
* Easily change WP admin login logo, just replace image path in `assets/css/adl-admin-login.css`
* Push YoastSEO meta box to the bottom of all pages
* Example code for adding taxonomy filter to CPT pages