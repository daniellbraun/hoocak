<?php

/**
 * The plugin bootstrap file
 *
 * This file is read by WordPress to generate the plugin information in the plugin
 * admin area. This file also includes all of the dependencies used by the plugin and
 * registers the activation and deactivation functions.
 *
 * @wordpress-plugin
 * Plugin Name:       Hoocak Site Functionality
 * Plugin URI:        http://www.ad-lit.com/
 * Description:       This plugin contains all persistent functionality for the Hoocak site.
 * Version:           0.5
 * Author:            Ad-Lit, Inc.
 * Author URI:        http://www.ad-lit.com/
 */

// If this file is called directly, abort.
if ( ! defined( 'WPINC' ) ) {
	die;
}

/**
 * Load required dependencies for this plugin.
 */

// Define all admin customizations
require_once plugin_dir_path( __FILE__ ) . 'includes/admin-customizations.php';

// Define all custom post types and customizations for those post types.
require_once plugin_dir_path( __FILE__ ) . 'includes/custom-post-types.php';

// Define all custom taxonomies and customizations for those taxonomies.
require_once plugin_dir_path( __FILE__ ) . 'includes/custom-taxonomies.php';

// Define all functions related to Advanced Custom Fields.
require_once plugin_dir_path( __FILE__ ) . 'includes/acf.php';

/**
 * Functionality for Sensei LMS
 */

//add_filter( 'sensei_load_default_supported_theme_wrappers', '__return_false' );
//
//global $woothemes_sensei;
//remove_action( 'sensei_before_main_content', array( $woothemes_sensei->frontend, 'sensei_output_content_wrapper' ), 10 );
//remove_action( 'sensei_after_main_content', array( $woothemes_sensei->frontend, 'sensei_output_content_wrapper_end' ), 10 );