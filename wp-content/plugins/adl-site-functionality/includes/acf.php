<?php
/**
 * Register functions related to ACF.
 *
 * All functions using ACF hooks and functions should be defined here.
 *
 * @since      0.5.0
 * @package    Adl_Site_Functionality
 * @subpackage Adl_Site_Functionality/includes
 */
class ADL_Acf {
	/**
	 * Initialize class and add our actions and hooks.
	 */
	public function __construct() {
		// Enqueue admin scripts/styles
		add_action( 'acf/input/admin_enqueue_scripts', array( $this, 'action_acf_admin_enqueue_scripts' ) );

		// Add options page (after ACF is loaded)
		add_action( 'plugins_loaded', array( $this, 'action_acf_register_options_pages' ) );

		// Add json load point
		add_filter('acf/settings/load_json', array( $this, 'filter_acf_json_load_point' ) );
	}

	/**
	 * Enqueue scripts/styles on admin pages where ACF fields are rendered.
	 */
	public function action_acf_admin_enqueue_scripts() {
		wp_enqueue_script( 'adl-admin-acf', plugin_dir_url( dirname(__FILE__) ) . 'assets/js/adl-admin-acf.js', false, '1.0.0' );
	}


	/**
	 * Add ACF json load point relative to this plugin.
	 *
	 * All custom fields that you want stored as json should be kept here.
	 *
	 *  @link https://www.advancedcustomfields.com/resources/local-json/
	 */
	public function filter_acf_json_load_point( $paths ) {
		// Append path, and allow theme to override it with custom json.
		$paths[] = plugin_dir_path( dirname( __FILE__ ) ). 'acf-json';

		return $paths;
	}


	/**
	 * Add ACF options page to admin menu.
	 *
	 * @link https://www.advancedcustomfields.com/resources/options-page/
	 */
	public function action_acf_register_options_pages() {
		if( function_exists('acf_add_options_page') ) {
			acf_add_options_page(array(
				'page_title' 	=> 'Options',
				'menu_slug' 	=> 'options-page',
				'icon_url'      => 'dashicons-admin-generic',
				'redirect'		=> false,
				'position'      => 29,
			) );
		}
	}

}

$adl_acf = new ADL_Acf();
