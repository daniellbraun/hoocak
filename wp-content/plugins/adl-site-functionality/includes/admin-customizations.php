<?php

/**
 * Register custom post types.
 *
 * @since      0.5.0
 * @package    Adl_Site_Functionality
 * @subpackage Adl_Site_Functionality/includes
 */
class ADL_Admin_Customizations {
	/**
	 * Initialize class and add our actions and hooks.
	 */
	public function __construct() {
		// Enqueue admin scripts/styles
		add_action( 'admin_enqueue_scripts', array( $this, 'action_admin_enqueue_scripts' ) );
		add_action( 'login_enqueue_scripts', array( $this, 'action_login_enqueue_scripts' ) );

		add_action( 'plugins_loaded', array( $this, 'add_medium_type_column' ) );
//		add_action( 'plugins_loaded', array( $this, 'add_module_column' ) );

		// Filter CPT
		add_action( 'restrict_manage_posts', array( $this, 'add_taxonomy_filters' ) );
		add_action( 'restrict_manage_posts', array( $this, 'add_lesson_unit_taxonomy_filters' ) );


		// Add links to admin menu
		add_action( 'admin_menu', array( $this, 'action_add_admin_menu_pages' ) );

        // Remove unnecessary admin menu items for Hoocak site
		add_action( 'admin_menu', array( $this, 'remove_menus' ) );

		// Change Yoast meta box priority, so it gets moved to bottom of all admin pages.
		add_filter( 'wpseo_metabox_prio', function () {
			return 'low';
		} );

        // change login logo url
        add_filter( 'login_headerurl', array( $this, 'hoocak_url' ) );
        // change title attribute of login logo

        add_filter( 'login_headertitle', array( $this, 'hoocak_logo_url_title' ) );
	}

    /**
     * Change the logo link on the login page
     */
    public function hoocak_url( $url ) {
        return get_bloginfo( 'url' ) . '/wp-admin';
    }

    /**
     * Change the logo title attribute on the login page
     */
    public function hoocak_logo_url_title() {
        return get_bloginfo( 'description' );
    }

    /**
     * Remove unnecessary menu items from admin menu for this site
     */
    public function remove_menus() {
        remove_menu_page( 'edit.php' ); //Posts
        remove_menu_page( 'edit-comments.php' );
//        remove_menu_page( 'tools.php' );
    }

	/**
	 * Enqueue scripts/styles for the admin.
	 *
	 * Enqueues FontAwesome for use with CPT menu_icons.
	 */
	public function action_admin_enqueue_scripts() {
		wp_enqueue_style( 'font-awesome', 'https://maxcdn.bootstrapcdn.com/font-awesome/4.6.1/css/font-awesome.min.css', '', '4.6.1', 'all' );
		wp_enqueue_style( 'adl-admin-style', plugin_dir_url( dirname( __FILE__ ) ) . 'assets/css/adl-admin.css', '', '1.0.0', 'all' );

		wp_enqueue_script( 'adl-admin-script', plugin_dir_url( dirname( __FILE__ ) ) . 'assets/js/adl-admin.js', false, '1.0.0' );
	}

	/**
	 * Enqueue styles for login page.
	 */
	public function action_login_enqueue_scripts() {
		wp_enqueue_style( 'adl-admin-style', plugin_dir_url( dirname( __FILE__ ) ) . 'assets/css/adl-admin-login.css', '', '1.0.0', 'all' );
	}




	/**
	 * Add admin menu and submenu pages.
	 *
	 * @link https://developer.wordpress.org/reference/functions/add_menu_page/
	 * @link https://developer.wordpress.org/reference/functions/add_submenu_page/
	 */
	public function action_add_admin_menu_pages() {
		add_submenu_page(
			'edit.php?post_type=CPT_SLUG',
			'My Menu Page',
			'My Menu Page',
			'edit_pages',
			'my-slug'
		);
	}



	/**
	 * Adds a taxonomy select field to the 'CPT-SLUG' post type admin
	 * page to filter posts by the'CPT-TAX-SLUG' taxonomy.
	 *
	 * @link http://wpquestions.com/question/showChrono/id/7548
	 */
	function add_taxonomy_filters() {
		global $typenow;

		// Taxonomies to display in dropdown
		$taxonomies = array( 'adl_learning_difficulty' );

		// Post type to show dropdown on
		if ( $typenow == ( 'adl_learning' ) ) {
			foreach ( $taxonomies as $tax_slug ) {
				$tax_obj  = get_taxonomy( $tax_slug );
				$tax_name = $tax_obj->labels->name;
				$terms    = get_terms( $tax_slug );

				if ( count( $terms ) > 0 ) {
					echo '<select name="' . $tax_slug . '" id="' . $tax_slug . '" class="postform">';
					echo '<option value="">All ' . $tax_name . '</option>';

					foreach ( $terms as $term ) {
						echo '<option value=' . $term->slug;

						if ( isset( $_GET[ $tax_slug ] ) ) {
							echo $_GET[ $tax_slug ] == $term->slug ? ' selected="selected"' : '';
						}

						echo '>' . $term->name . ' (' . $term->count . ')</option>';
					}

					echo '</select>';
				}
			}
		}
	}

    /**
     * Adds a taxonomy select field to the 'lesson' post type admin
     * page to filter posts by the 'module' taxonomy.
     *
     * @link http://wpquestions.com/question/showChrono/id/7548
     */
    function add_lesson_unit_taxonomy_filters() {
        global $typenow;

        // Taxonomies to display in dropdown
        $taxonomies = array( 'module' );

        // Post type to show dropdown on
        if ( $typenow == ( 'lesson' ) ) {
            foreach ( $taxonomies as $tax_slug ) {
                $tax_obj  = get_taxonomy( $tax_slug );
                $tax_name = $tax_obj->labels->name;
                $terms    = get_terms( $tax_slug );

                if ( count( $terms ) > 0 ) {
                    echo '<select name="' . $tax_slug . '" id="' . $tax_slug . '" class="postform">';
                    echo '<option value="">All Unit ' . $tax_name . '</option>';

                    foreach ( $terms as $term ) {

                        echo '<option value=' . $term->slug;

                        if ( isset( $_GET[ $tax_slug ] ) ) {
                            echo $_GET[ $tax_slug ] == $term->slug ? ' selected="selected"' : '';
                        }

                        $display_count = 0;

                        if ($term->count > 0) {
                            $display_count = $term->count - 1;
                        }

                        echo '>' . $term->name . ' (' . $display_count . ')</option>';
                    }

                    echo '</select>';
                }
            }
        }
    }

	/**
	 * Adds the learning type to colums.
	 */
	function add_medium_type_column() {
		Jigsaw::add_column( 'adl_learning', 'Type', function ( $pid ) {
			$data         = array();
			$data['post'] = new TimberPost( $pid );
			Timber::render('admin/learning-type-col.twig', $data);
		}, 3 );
	}

    /**
     * Adds module (unit) to sensei lesson CPT columns.
     */
//    function add_module_column() {
//        Jigsaw::add_column( 'lesson', 'Unit', function ( $pid ) {
//            $data         = array();
//            $data['post'] = new TimberPost( $pid );
//            Timber::render('admin/module-col.twig', $data);
//        }, 10 );
//    }
}


$adl_admin_customizations = new ADL_Admin_Customizations();
