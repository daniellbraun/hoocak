<?php
/**
 * Register custom post types.
 *
 * @since      0.5.0
 * @package    Adl_Site_Functionality
 * @subpackage Adl_Site_Functionality/includes
 */
class ADL_Custom_Post_Types {
	/**
	 * Initialize class and add our actions and hooks.
	 */
	public function __construct() {
		add_action( 'init', array( $this, 'register_learning_material_post_type' ) );
	}

	/**
	 * Register a custom post type.
	 */
	public function register_learning_material_post_type() {
		register_post_type( 'adl_learning',
			array(
				'labels'        => array(
					'name'          => 'Learning Materials',
					'singular_name' => 'Learning Material',
					'add_new'       => 'Add Learning Material',
					'add_new_item'  => 'Add New Learning Material',
					'edit_item'     => 'Edit Learning Material',
				),
				'hierarchical'  => true,
				'public'        => true,
				'has_archive'   => true,
				'rewrite'       => array( 'slug' => 'learning-material' ),
				'supports'      => array( 'title', 'revisions', 'page-attributes' ),
				'taxonomies'    => array(
					'adl_learning_difficulty',
				),
				'menu_icon'     => 'dashicons-edit',
			)
		);
	}
}

$adl_custom_post_types = new ADL_Custom_Post_Types;
