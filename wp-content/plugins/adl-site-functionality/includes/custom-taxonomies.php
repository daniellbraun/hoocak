<?php

/**
 * Register custom taxonomies.
 *
 * @since      0.5.0
 * @package    Adl_Site_Functionality
 * @subpackage Adl_Site_Functionality/includes
 */
class ADL_Custom_Taxonomies {
	/**
	 * Initialize class and add our actions and hooks.
	 */
	public function __construct() {
		add_action( 'init', array( $this, 'register_difficulty_taxonomy' ) );
	}

	/**
	 * Register a custom post type.
	 */
	public function register_difficulty_taxonomy() {
		register_taxonomy(
			'adl_learning_difficulty',
			'adl_learning',
			array(
				'labels' => array(
					'add_new_item' => 'Add New Learning Type',
					'name'         => 'Learning Type',
					'edit_item'    => 'Edit Learning Type',
					'update_item'  => 'Update Learning Type',

				),
				'rewrite'           => array( 'slug' => 'difficulty' ),
				'hierarchical'      => true,
				'show_admin_column' => true,
			)
		);
	}
}

$adl_custom_taxonomies = new ADL_Custom_Taxonomies;