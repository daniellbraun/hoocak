This plugin uses Slick-Slider in order to provide a simple slideshow for pages.
It also uses the LazySizes jquery plugin to give optimized content to the user.

After it is set up, the basic use command is as follows. Where height is optional.

```
#!twig
{{ slick_slideshow(post.slider, $height) }}
```
to insert it into the page.

# Setting Up the Plugin #
1. In your plugin directory run
```
#!bash
git clone https://bitbucket.org/adlit/adl-slick-slider.git
```
2. After it is downloaded cd to your theme directory
3. Then run 
```
#!bash
bower install slick-carousel
bower install lazysizes
```
4. Inside your config.yaml add these to the proper sections. Just above your 'src/scss' and 'app.js'

* sass:
```
#!yaml
- "bower_components/slick-carousel/slick"
- "../../plugins/adl-slick-slider"
```
* javascript:
```
#!yaml
- "bower_components/slick-carousel/slick/slick.min.js"
- "bower_components/lazysizes/lazysizes.min.js"
```
5. In your app.scss add these two imports. They will import the funcationality and the deafault adlit theme for slick slider.
If you plan to modify the css, I would suggest removing the file from the plugin and placing it in your own scss folder.

```
#!scss

@import 'slick.scss';
@import 'adl-slick';
```
6. Don't forget to activate the plugin.
7. Use the following command to place the slideshow where you need it.
```
#!twig
{{ slick_slideshow(post.slider, $height) }}
```