<?php

if ( function_exists( 'acf_add_local_field_group' ) ):

	acf_add_local_field_group( array(
		'key'                   => 'group_580668a7649dd',
		'title'                 => 'Slider Select',
		'fields'                => array(
			array(
				'key'               => 'field_580668ac95077',
				'label'             => 'Slider',
				'name'              => 'slider',
				'type'              => 'post_object',
				'instructions'      => '',
				'required'          => 0,
				'conditional_logic' => 0,
				'wrapper'           => array(
					'width' => '',
					'class' => '',
					'id'    => '',
				),
				'post_type'         => array(
					0 => 'adl-slick-slider',
				),
				'taxonomy'          => array(),
				'allow_null'        => 1,
				'multiple'          => 0,
				'return_format'     => 'object',
				'ui'                => 1,
			),
		),
		'location'              => array(
			array(
				array(
					'param'    => 'post_type',
					'operator' => '==',
					'value'    => 'page',
				),
			),
			array(
				array(
					'param'    => 'post_type',
					'operator' => '==',
					'value'    => 'adl-special',
				),
			),
			array(
				array(
					'param'    => 'post_type',
					'operator' => '==',
					'value'    => 'adl-package',
				),
			),
			array(
				array(
					'param'    => 'post_type',
					'operator' => '==',
					'value'    => 'adl-room',
				),
			),
		),
		'menu_order'            => 0,
		'position'              => 'side',
		'style'                 => 'default',
		'label_placement'       => 'top',
		'instruction_placement' => 'label',
		'hide_on_screen'        => '',
		'active'                => 1,
		'description'           => '',
	) );

	acf_add_local_field_group( array(
		'key'                   => 'group_5806675e7853d',
		'title'                 => 'Slideshow',
		'fields'                => array(
			array(
				'key'               => 'field_5806676427e84',
				'label'             => 'Slideshow Images',
				'name'              => 'slideshow',
				'type'              => 'gallery',
				'instructions'      => '',
				'required'          => 0,
				'conditional_logic' => 0,
				'wrapper'           => array(
					'width' => '',
					'class' => '',
					'id'    => '',
				),
				'min'               => '',
				'max'               => '',
				'preview_size'      => 'thumbnail',
				'insert'            => 'append',
				'library'           => 'all',
				'min_width'         => '',
				'min_height'        => '',
				'min_size'          => '',
				'max_width'         => '',
				'max_height'        => '',
				'max_size'          => '',
				'mime_types'        => '',
			),
		),
		'location'              => array(
			array(
				array(
					'param'    => 'post_type',
					'operator' => '==',
					'value'    => 'adl-slick-slider',
				),
			),
		),
		'menu_order'            => 0,
		'position'              => 'normal',
		'style'                 => 'default',
		'label_placement'       => 'top',
		'instruction_placement' => 'label',
		'hide_on_screen'        => '',
		'active'                => 1,
		'description'           => '',
	) );

	acf_add_local_field_group( array(
		'key'                   => 'group_581ce3eb8c078',
		'title'                 => 'Settings',
		'fields'                => array(
			array(
				'key'               => 'field_581ce3f395989',
				'label'             => 'Slide Delay',
				'name'              => 'slide_delay',
				'type'              => 'number',
				'instructions'      => 'Specify a time delay in seconds before each slide switches to the next.',
				'required'          => 0,
				'conditional_logic' => 0,
				'wrapper'           => array(
					'width' => '100',
					'class' => '',
					'id'    => '',
				),
				'default_value'     => 3,
				'placeholder'       => '3',
				'prepend'           => '',
				'append'            => '',
				'min'               => '',
				'max'               => '',
				'step'              => '',
				'readonly'          => 0,
				'disabled'          => 0,
			),
		),
		'location'              => array(
			array(
				array(
					'param'    => 'post_type',
					'operator' => '==',
					'value'    => 'adl-slick-slider',
				),
			),
		),
		'menu_order'            => 1,
		'position'              => 'normal',
		'style'                 => 'default',
		'label_placement'       => 'top',
		'instruction_placement' => 'label',
		'hide_on_screen'        => '',
		'active'                => 1,
		'description'           => '',
	) );

endif;