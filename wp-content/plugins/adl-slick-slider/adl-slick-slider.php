<?php
/*
	Plugin Name: Slick Slider (with ACF)
	Description: Adds a Slideshow custom post type with an ACF Gallery for it's content. Also Adds a selector to pages to define the slideshow associated with it.
	Author: Bruce Brotherton
	Version: 1.0.0
	Author URI: http://www.ad-lit.com/
*/

// Exit plugin if accessed directly
if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

if ( ! class_exists( 'ADL_Slick_Slider' ) ) {

	class ADL_Slick_Slider {

		/**
		 * default value of 3 seconds for slide delay
		 *
		 * @var int
		 */
		public $delay = 3;

		public function __construct() {
			add_action( 'wp_loaded', array( $this, 'register_fields' ) );
			add_action( 'init', array( $this, 'slick_sliders' ) );

			// Register Slick init
			add_action( 'wp_footer', array( $this, 'slick_init_js' ), 20 );

			add_filter( 'timber/twig', array( $this, 'add_to_twig' ) );
		}

		function add_to_twig( $twig ) {
			$twig->addFunction( new \Twig_SimpleFunction( 'slick_slideshow', array( $this, 'get_slideshow' ) ) );

			return $twig;
		}

		/**
		 * Register ACF field group.
		 */
		function register_fields() {
			include 'acf-fields.php';
		}

		/**
		 * Register Slideshow post type.
		 */
		public function slick_sliders() {
			$singular = 'Slideshow';
			$plural   = 'Slideshows';

			register_post_type(
				'adl-slick-slider', array(
					'labels'       => array(
						'name'               => $plural,
						'singular_name'      => $singular,
						'add_new'            => 'Add ' . $singular,
						'add_new_item'       => 'Add New ' . $singular,
						'new_item'           => 'New ' . $singular,
						'edit_item'          => 'Edit ' . $singular,
						'view_item'          => 'View ' . $singular,
						'all_items'          => 'All ' . $plural,
						'search_items'       => 'Search ' . $plural,
						'parent_item_colon'  => 'Parent ' . $plural . ':',
						'not_found'          => 'No ' . $plural . ' found.',
						'not_found_in_trash' => 'No ' . $plural . ' found in Trash.',
					),
					'public'       => true,
					'show_ui'      => true,
					'show_in_menu' => true,
					'rewrite'      => array(
						'slug' => 'example',
					),
					'has_archive'  => true,
					'hierarchical' => true,
					'menu_icon'    => 'dashicons-images-alt',
					'supports'     => array( 'title', 'revisions', 'thumbnail', 'page-attributes' ),
				)
			);
		}

		function get_slideshow( $slider_id = null, $height = 400 ) {
			if ( ! $slider_id ) {
				$slider_id   = get_field( 'slider' );
				$slides    	 = get_field( 'slideshow', $slider_id );
				$this->delay = get_field( 'slide_delay', $slider_id );
			} else {
				$slides 	 = get_field( 'slideshow', $slider_id );
				$this->delay = get_field( 'slide_delay', $slider_id );
			}

			if ( $slides ) {
				$image = array();
				foreach ( $slides as $item ) {
					$image[] = $item['url'];
				}
				$data['height']    = $height;
				$data['slide_img'] = $image;
				Timber::render( 'partials/slideshow.twig', $data );
			} else if ( $slider_id ) {
				echo '<!--The Slider ID ' . $slider_id . ' is invalid-->';
			}
		}

		function slick_init_js() {
			?>
			<script>
				jQuery(document).ready(function ($) {
					$('#adl-hero-slick-slider').slick({
						dots: false,
						infinite: true,
						autoplay: true,
						autoplaySpeed: <?php echo $this->delay * 1000; ?>,
						speed: 500,
						fade: true,
						cssEase: 'linear'
					});
				});
			</script>
			<?php
		}
	}

	$adl_slick_slider = new ADL_Slick_Slider;
}

