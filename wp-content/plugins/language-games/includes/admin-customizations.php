<?php

/**
 * Custom Columns for Language Games
 *
 */
class Language_Games_Admin_Customizations
{
    /**
     * Initialize class and add our actions and hooks.
     */
    public function __construct()
    {

        // Custom admin columns for Word Game
        add_action('manage_word_game_posts_custom_column', array($this, 'custom_game_columns'));
        add_filter('manage_word_game_posts_columns', array($this, 'word_game_cpt_columns'));

        add_action('manage_word_quiz_posts_custom_column', array($this, 'custom_quiz_columns'));
        add_filter('manage_word_quiz_posts_columns', array($this, 'word_quiz_cpt_columns'));

    }

    // custom columns

    public function word_game_cpt_columns($columns)
    {
        unset(
            $columns['title'],
            $columns['date'],
            $columns['taxonomy-language_games_word_category']
        );

        $new_columns = array(
            'image' => __('Word Image'),
            'word' => __('Word'),
            'taxonomy-language_games_word_category' => __('Word Category'),
            'date' => __('Date'),
        );
        return array_merge($columns, $new_columns);
    }

    // GET FEATURED IMAGE
    private function ADL_get_featured_image()
    {
        $image = get_field('word_image');
        $size = 'thumbnail';
        $thumb = null;

        if ($image) {
            $thumb = wp_get_attachment_image_src($image, $size)[0];
        }
        return $thumb;
    }

    public function custom_game_columns($column)
    {

        switch ($column) {

            case 'image':

                $post_featured_image = $this->ADL_get_featured_image();
                if ($post_featured_image) {
                    echo '<a href="' . get_edit_post_link() . '"><img src="' . $post_featured_image . '" /></a>';
                }
                break;

            case 'word':

                echo '<a href="' . get_edit_post_link() . '">' . get_field('word') . '</a>';
                break;
        }
    }

    public function word_quiz_cpt_columns($columns)
    {
        echo 'columns: ';
        print_r($columns);
        unset(
            $columns['title'],
            $columns['date'],
            $columns['taxonomy-language_games_word_category']
        );
        $new_columns = array(
            'image' => __('Question Image'),
            'title' => __('Title'),
            'taxonomy-language_games_word_category' => __('Word Category'),
            'date' => __('Date Published'),
        );
        return array_merge($columns, $new_columns);
    }


    public function custom_quiz_columns($column)
    {

        switch ($column) {

            case 'image':

                $post_featured_image = $this->ADL_get_featured_image();
                if ($post_featured_image) {
                    echo '<a href="' . get_edit_post_link() . '"><img src="' . $post_featured_image . '" /></a>';
                }
                break;
        }
    }
}

$language_games_admin_customizations = new Language_Games_Admin_Customizations();
