<?php

/**
 * Register games custom post types.
 */
class Language_Games_Post_Types
{

    public function __construct()
    {
        add_action( 'init', array($this, 'word_game_post_type') );
        add_action( 'init', array($this, 'word_quiz_post_type') );
        add_action( 'admin_menu', array($this, 'add_language_games_admin_menu') );
        add_filter( 'parent_file', array($this, 'language_games_set_current_menu') );
    }

    public function add_language_games_admin_menu() {

        // settings for custom admin menu
        $page_title = 'Language Games';
        $menu_title = 'Language Games';
        $capability = 'edit_others_posts';
        $menu_slug = 'language_games';
        $function = array($this, 'display_language_games_admin_page');
        $icon_url = 'dashicons-testimonial';
        $position = 6;

        add_menu_page( $page_title, $menu_title, $capability, $menu_slug, $function, $icon_url, $position );

//        $language_games_admin = array(
//            'parent_slug'   => 'language_games',
//            'page_title'    => '',
//            'menu_title'    => 'Language Games',
//            'capability'    => 'read',
//            'menu_slug'     => 'language_games',
//            'function'      => array($this, 'display_language_games_admin_page'),
//        );

        $word_game_admin = array(
            'parent_slug'   => 'language_games',
            'page_title'    => '',
            'menu_title'    => 'Word Picture Game',
            'capability'    => 'edit_posts',
            'menu_slug'     => 'edit.php?post_type=word_game',
            'function'      => null,
        );

        $word_quiz_admin = array(
            'parent_slug'   => 'language_games',
            'page_title'    => '',
            'menu_title'    => 'Word Quiz',
            'capability'    => 'edit_posts',
            'menu_slug'     => 'edit.php?post_type=word_quiz',
            'function'      => null,
        );

        // Taxonomy :: Manage Word Categories
        $word_game_cat = array(
            'parent_slug'   => 'language_games',
            'page_title'    => '',
            'menu_title'    => 'Word Categories',
            'capability'    => 'edit_posts',
            'menu_slug'     => 'edit-tags.php?taxonomy=language_games_word_category&post_type=word_game',
            'function'      => null,
        );
//        add_submenu_page(
//            $language_games_admin['parent_slug'],
//            $language_games_admin['page_title'],
//            $language_games_admin['menu_title'],
//            $language_games_admin['capability'],
//            $language_games_admin['menu_slug'],
//            $language_games_admin['function']
//        );
        add_submenu_page(
            $word_game_admin['parent_slug'],
            $word_game_admin['page_title'],
            $word_game_admin['menu_title'],
            $word_game_admin['capability'],
            $word_game_admin['menu_slug'],
            $word_game_admin['function']
        );
        add_submenu_page(
            $word_quiz_admin['parent_slug'],
            $word_quiz_admin['page_title'],
            $word_quiz_admin['menu_title'],
            $word_quiz_admin['capability'],
            $word_quiz_admin['menu_slug'],
            $word_quiz_admin['function']
        );
        add_submenu_page(
            $word_game_cat['parent_slug'],
            $word_game_cat['page_title'],
            $word_game_cat['menu_title'],
            $word_game_cat['capability'],
            $word_game_cat['menu_slug'],
            $word_game_cat['function']
        );

    }
    public function display_language_games_admin_page(){

        // Display custom admin page content from newly added custom admin menu.
        echo '<div class="wrap">'.PHP_EOL;
        echo '<h1>Language Games</h1>'.PHP_EOL;
        echo '<p><iframe style="min-width:100%;min-height:800px;" src="https://docs.google.com/document/d/1HmFUPkTCK2TElGJWF0uyWamiHjBVDWF3yMtrHS79eEE/pub?embedded=true"></iframe></p>'.PHP_EOL;
        echo '</div>'.PHP_EOL;
        echo '<div class="clear"></div>'.PHP_EOL;

    }

    public function language_games_set_current_menu($parent_file){

        global $submenu_file, $current_screen, $pagenow;

        // Set the submenu as active/current while anywhere in your Custom Post Type (nwcm_news)
        if($current_screen->post_type == 'word_game' || $current_screen->post_type == 'word_quiz') {

            if($pagenow == 'post.php'){
                $submenu_file = 'edit.php?post_type='.$current_screen->post_type;
            }

            if($pagenow == 'edit-tags.php'){
                $submenu_file = 'edit-tags.php?taxonomy=language_games_word_category&post_type='.$current_screen->post_type;
            }

            if($pagenow == 'term.php'){
                $submenu_file = 'edit-tags.php?taxonomy=language_games_word_category&post_type='.$current_screen->post_type;
            }

            $parent_file = 'language_games';

        }

        return $parent_file;

    }

    public function word_game_post_type()
    {
        register_post_type('word_game',
            array(
                'labels' => array(
                    'name' => 'Word Picture Game',
                    'singular_name' => 'Word Picture Game Piece',
                    'add_new' => 'Add Word Picture Game Piece',
                    'add_new_item' => 'Add New Game Picture',
                    'edit_item' => 'Edit Game Picture',
                ),
                'public' => true,
                'has_archive' => true,
                'rewrite' => array('slug' => 'game_piece'),
                'supports' => array('revisions', 'thumbnail'),
//				'taxonomies'  => array( 'language_games_word_category' ),
                'show_ui' => true,
                'show_in_menu' => false
            )
        );
    }

    public function word_quiz_post_type()
    {
        register_post_type('word_quiz',
            array(
                'labels' => array(
                    'name' => 'Word Quiz',
                    'singular_name' => 'Word Quiz Question',
                    'add_new' => 'Add Word Quiz Question',
                    'add_new_item' => 'Add New Quiz Question',
                    'edit_item' => 'Edit Quiz Question',
                ),
                'public' => true,
                'has_archive' => true,
                'rewrite' => array('slug' => 'word-quiz'),
                'supports' => array('revisions', 'thumbnail', 'title'),
                'show_ui' => true,
                'show_in_menu' => false
//				'taxonomies'  => array( 'example-tax' )
            )
        );
    }
}

$language_games_types = new Language_Games_Post_Types;