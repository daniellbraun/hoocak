<?php

/**
 * Register and enqueue game scripts conditionally based on page ID
 */
class GameScripts {

	const WORD_PICTURE_GAME_ID = 101;
	const WORD_QUIZ_GAME_ID = 201;
	const CALENDAR_MATCHING_GAME_ID = 301;
	const FAMILY_IN_HOME_GAME_ID = 401;
	const HOOCAK_WOOWAGAX_GAME_ID = 501;
	const COLORS_LEARNING_GAME_ID = 601;
	const NUMBERS_MATCHING_GAME_ID = 701;
	const CROSSWORD_PUZZLE_GAME_ID = 801;
	const PATHWAY_1_SLUG = 'online-pathway-program';
	const PATHWAY_2_SLUG = 'online-pathways-program-2-0';

	public function __construct() {
		add_action( 'wp_enqueue_scripts', array( $this, 'gather_word_game_data' ) );
		add_action( 'wp_ajax_save_game_progress', array( $this, 'save_game_progress_callback' ) );

		add_filter( 'terms_clauses', array( $this, 'adl_terms_clauses' ), 10, 3 );

		add_filter( 'sensei_single_lesson_content_inside_before', array(
			$this,
			'hook_into_single_lesson_before'
		), 10, 3 );

		add_action( 'init', array( $this, 'myStartSession' ), 1 );

		add_action( 'wp_ajax_save_session', array( $this, 'save_session_callback' ) );
		add_action( 'wp_ajax_nopriv_save_session', array( $this, 'save_session_callback' ) );

		add_action( 'wp_ajax_get_session_status', array( $this, 'get_session_status_callback' ) );
		add_action( 'wp_ajax_nopriv_get_session_status', array( $this, 'get_session_status_callback' ) );

	}

	public function myStartSession() {
		if ( ! session_id() ) {
			session_start();
		}
	}

	public function gather_word_game_data() {

		wp_register_script( 'createjs/js', 'https://code.createjs.com/1.0.0/createjs.min.js', [ 'jquery' ], null, true );
		wp_register_script( 'gamez-common/js', plugins_url( '../js/gamez-common.js', __FILE__ ), [], null, true );
		wp_register_script( 'gamez-wordgame/js', plugins_url( '../js/gamez-wordgame.js', __FILE__ ), [], null, true );
		wp_register_script( 'gamez-wordquiz/js', plugins_url( '../js/gamez-wordquiz.js', __FILE__ ), [], null, true );
		wp_register_script( 'gamez-savesession/js', plugins_url( '../js/gamez-savesession.js', __FILE__ ), [], null, true );
		wp_register_script( 'gamez-family/js', plugins_url( '../js/gamez-family.js', __FILE__ ), [], null, true );
		wp_register_script( 'gamez-hoocakwoowagax/js', plugins_url( '../js/gamez-hoocakwoowagax.js', __FILE__ ), [], null, true );
		wp_register_script( 'gamez-colors/js', plugins_url( '../js/gamez-colors.js', __FILE__ ), [], null, true );
		wp_register_script( 'gamez-numbers/js', plugins_url( '../js/gamez-numbers.js', __FILE__ ), [], null, true );
		wp_register_script( 'gamez-calendar/js', plugins_url( '../js/gamez-calendar.js', __FILE__ ), [], null, true );
		wp_register_script( 'gamez-crossword/js', plugins_url( '../js/gamez-crossword.js', __FILE__ ), [], null, true );
		wp_register_script( 'progress-map-1/js', plugins_url( '../js/progress-map.js', __FILE__ ), [], null, true );
		wp_register_script( 'progress-map-2/js', plugins_url( '../js/progress-map-2.js', __FILE__ ), [], null, true );

		$ajax_url  = admin_url( 'admin-ajax.php' );
		$image_url = plugins_url( '../img/', __FILE__ );
		$audio_url = plugins_url( '../audio/', __FILE__ );

		if ( is_page( 'word-quiz-game' ) ) {

			// ENQUEUE SCRIPTS FOR WORD QUIZ GAME AND GATHER/LOCALIZE GAME DATA

			wp_enqueue_script( [
				'createjs/js',
				'gamez-common/js',
				'gamez-savesession/js',
				'gamez-wordquiz/js'
			] );

			$quiz_args  = array(
				'post_type' => 'word_quiz',
				'order'     => 'ASC',
				'orderby'   => 'title'
			);
			$quiz_posts = new WP_Query( $quiz_args );

			$quiz_data = array();

			while ( $quiz_posts->have_posts() ) {

				$quiz_posts->the_post();

				$image = get_field( 'word_image' );
				$size  = 'medium';

				$quiz_data[] = array(
					'image_url_acf'  => wp_get_attachment_image_src( $image, $size )[0],
					'question_text'  => get_field( 'question_text' ),
					'question_audio' => get_field( 'question_audio' ),
					'answers'        => get_field( 'answers' ),
					'category'       => get_the_terms( $quiz_posts->post, 'language_games_word_category' ),
				);
			}
			// QUERYING FOR TERMS/CATEGORIES OF A SINGLE POST TYPE WORKS
			// BECAUSE OF adl_terms_clauses FUNCTION DEFINED BELOW

			// NEEDED HERE BECAUSE 2 CUSTOM POST TYPES ARE SHARING THE SAME SET
			// OF TERMS AND WE ONLY WANT TO SHOW THE TERMS USED (not empty) OF ONE
			// OF THE CUSTOM POST TYPES (no choice really, hide_empty = false doesn't
			// seem to work in the case of using adl_terms_clause anyway...
			$terms = get_terms( array(
				'post_type'  => array( 'word_quiz' ),
				'taxonomy'   => 'language_games_word_category',
				'hide_empty' => true,
			) );

			$categories = array();
			foreach ( $terms as $term ) {
				$categories[] = $term;
			}

			/* Restore original Post Data */
			wp_reset_postdata();

			$quiz_json = json_decode( json_encode( $quiz_data ), true );

			$save_data = array(
				'ajax_url' => $ajax_url,
				'game_id'  => self::WORD_QUIZ_GAME_ID
			);

			$game_status = $this->return_game_status( self::WORD_QUIZ_GAME_ID );

			wp_localize_script( 'gamez-wordquiz/js', 'word_quiz_data', $quiz_json );
			wp_localize_script( 'gamez-wordquiz/js', 'word_quiz_categories', $categories );
			wp_localize_script( 'gamez-wordquiz/js', 'user_data', $this->gather_user_data( self::WORD_QUIZ_GAME_ID ) );
			wp_localize_script( 'gamez-savesession/js', 'save_data', $save_data );
			wp_localize_script( 'gamez-wordquiz/js', 'game_status', $game_status );
			wp_localize_script( 'gamez-wordquiz/js', 'image_url', $image_url );
			wp_localize_script( 'gamez-wordquiz/js', 'audio_url', $audio_url );
			wp_localize_script( 'gamez-savesession/js', 'image_url', $image_url );

		} elseif ( is_page( 'word-picture-game' ) ) {

			// ENQUEUE SCRIPTS FOR WORD PICTURE GAME AND GATHER/LOCALIZE GAME DATA

			wp_enqueue_script( [
				'createjs/js',
				'gamez-common/js',
				'gamez-savesession/js',
				'gamez-wordgame/js'
			] );

			// The Query
			$args            = array(
				'post_type' => 'word_game',
			);
			$word_game_posts = new WP_Query( $args );

			$word_game_data = array();

			while ( $word_game_posts->have_posts() ) {

				$word_game_posts->the_post();

				$image = get_field( 'word_image' );
				$size  = 'medium';

				$word_game_data[] = array(
					'image_url_acf' => wp_get_attachment_image_src( $image, $size )[0],
					'english_word'  => get_field( "english_word" ),
					'word'          => get_field( "word" ),
					'audio_url'     => get_field( "voice_audio_file" )['url'],
					'category'      => get_the_terms( $word_game_posts->post, 'language_games_word_category' ),
				);
			}
			$word_game_json = json_decode( json_encode( $word_game_data ) );

			// QUERYING FOR TERMS/CATEGORIES OF A SINGLE POST TYPE WORKS
			// BECAUSE OF adl_terms_clauses FUNCTION DEFINED BELOW

			// NEEDED HERE BECAUSE 2 CUSTOM POST TYPES ARE SHARING THE SAME SET
			// OF TERMS AND WE ONLY WANT TO SHOW THE TERMS USED (not empty) OF ONE
			// OF THE CUSTOM POST TYPES (no choice really, hide_empty = false doesn't
			// seem to work in the case of using adl_terms_clause anyway...
			$terms = get_terms( array(
				'post_type'  => array( 'word_game' ),
				'taxonomy'   => 'language_games_word_category',
				'hide_empty' => 1,
			) );

			$categories = array();
			foreach ( $terms as $term ) {
				$categories[] = $term;
			}

			/* Restore original Post Data */
			wp_reset_postdata();

			$save_data = array(
				'ajax_url' => $ajax_url,
				'game_id'  => self::WORD_PICTURE_GAME_ID
			);

			$game_status = $this->return_game_status( self::WORD_PICTURE_GAME_ID );

			wp_localize_script( 'gamez-wordgame/js', 'gamez_data', $word_game_json );
			wp_localize_script( 'gamez-wordgame/js', 'gamez_categories', $categories );
			wp_localize_script( 'gamez-wordgame/js', 'user_data', $this->gather_user_data( self::WORD_QUIZ_GAME_ID ) );
			wp_localize_script( 'gamez-savesession/js', 'save_data', $save_data );
			wp_localize_script( 'gamez-wordgame/js', 'game_status', $game_status );
			wp_localize_script( 'gamez-wordgame/js', 'image_url', $image_url );
			wp_localize_script( 'gamez-wordgame/js', 'audio_url', $audio_url );
			wp_localize_script( 'gamez-savesession/js', 'image_url', $image_url );

		} elseif ( is_page( 'family-in-the-home' ) ) {

			wp_enqueue_script( [
				'createjs/js',
				'gamez-common/js',
				'gamez-savesession/js',
				'gamez-family/js'
			] );

			$save_data   = array(
				'ajax_url' => $ajax_url,
				'game_id'  => self::FAMILY_IN_HOME_GAME_ID
			);
			$game_status = $this->return_game_status( self::FAMILY_IN_HOME_GAME_ID );

			wp_localize_script( 'gamez-family/js', 'image_url', $image_url );
			wp_localize_script( 'gamez-savesession/js', 'image_url', $image_url );
			wp_localize_script( 'gamez-family/js', 'audio_url', $audio_url );
			wp_localize_script( 'gamez-family/js', 'save_data', $save_data );
			wp_localize_script( 'gamez-family/js', 'game_status', $game_status );

		} elseif ( is_page( 'hoocak-woowagax-game' ) ) {

			$save_data   = array(
				'ajax_url' => $ajax_url,
				'game_id'  => self::HOOCAK_WOOWAGAX_GAME_ID
			);
			$game_status = $this->return_game_status( self::HOOCAK_WOOWAGAX_GAME_ID );

			wp_enqueue_script( [
				'createjs/js',
				'gamez-common/js',
				'gamez-savesession/js',
				'gamez-hoocakwoowagax/js'
			] );

			wp_localize_script( 'gamez-hoocakwoowagax/js', 'game_status', $game_status );
			wp_localize_script( 'gamez-hoocakwoowagax/js', 'audio_url', $audio_url );
			wp_localize_script( 'gamez-savesession/js', 'save_data', $save_data );
			wp_localize_script( 'gamez-savesession/js', 'image_url', $image_url );

		} elseif ( is_page( 'colors-learning-game' ) ) {

			wp_enqueue_script( [
				'createjs/js',
				'gamez-savesession/js',
				'gamez-colors/js'
			] );

			$save_data   = array(
				'ajax_url' => $ajax_url,
				'game_id'  => self::COLORS_LEARNING_GAME_ID
			);
			$game_status = $this->return_game_status( self::COLORS_LEARNING_GAME_ID );

			wp_localize_script( 'gamez-colors/js', 'image_url', $image_url );
			wp_localize_script( 'gamez-colors/js', 'audio_url', $audio_url );
			wp_localize_script( 'gamez-savesession/js', 'image_url', $image_url );
			wp_localize_script( 'gamez-colors/js', 'save_data', $save_data );
			wp_localize_script( 'gamez-colors/js', 'game_status', $game_status );

		} elseif ( is_page( 'numbers-matching-game' ) ) {

			$save_data   = array(
				'ajax_url' => $ajax_url,
				'game_id'  => self::NUMBERS_MATCHING_GAME_ID
			);
			$game_status = $this->return_game_status( self::NUMBERS_MATCHING_GAME_ID );

			wp_enqueue_script( [
				'createjs/js',
				'gamez-common/js',
				'gamez-savesession/js',
				'gamez-numbers/js'
			] );

			wp_localize_script( 'gamez-numbers/js', 'audio_url', $audio_url );
			wp_localize_script( 'gamez-numbers/js', 'save_data', $save_data );
			wp_localize_script( 'gamez-numbers/js', 'game_status', $game_status );
			wp_localize_script( 'gamez-numbers/js', 'image_url', $image_url );
			wp_localize_script( 'gamez-savesession/js', 'image_url', $image_url );

		} elseif ( is_page( 'calendar-matching-game' ) ) {

			$save_data = array(
				'ajax_url' => $ajax_url,
				'game_id'  => self::CALENDAR_MATCHING_GAME_ID
			);

			$game_status = $this->return_game_status( self::CALENDAR_MATCHING_GAME_ID );

			wp_enqueue_script( [
				'createjs/js',
				'gamez-common/js',
				'gamez-savesession/js',
				'gamez-calendar/js'
			] );

			wp_localize_script( 'gamez-calendar/js', 'audio_url', $audio_url );
			wp_localize_script( 'gamez-calendar/js', 'save_data', $save_data );
			wp_localize_script( 'gamez-calendar/js', 'game_status', $game_status );
			wp_localize_script( 'gamez-calendar/js', 'image_url', $image_url );
			wp_localize_script( 'gamez-savesession/js', 'image_url', $image_url );

		} elseif ( is_page( 'hoocak-crossword-puzzle' ) ) {

			wp_enqueue_script( [
				'createjs/js',
				'gamez-common/js',
				'gamez-savesession/js',
				'gamez-crossword/js'
			] );

			$save_data = array(
				'ajax_url' => $ajax_url,
				'game_id'  => self::CROSSWORD_PUZZLE_GAME_ID
			);

			$game_status = $this->return_game_status( self::CROSSWORD_PUZZLE_GAME_ID );

			wp_localize_script( 'gamez-crossword/js', 'save_data', $save_data );
			wp_localize_script( 'gamez-crossword/js', 'game_status', $game_status );
			wp_localize_script( 'gamez-crossword/js', 'image_url', $image_url );
			wp_localize_script( 'gamez-savesession/js', 'image_url', $image_url );

		} elseif ( get_post_type() == 'course' && is_single() && is_user_logged_in() ) {

			$course_id     = get_the_ID();
			$progress_data = $this->return_user_module_progress( get_current_user_id(), $course_id );

			global $post;
			$post_slug = $post->post_name;

			$js_file = '';
			if ( $post_slug == self::PATHWAY_1_SLUG ) :
				$js_file = 'progress-map-1/js';
			elseif ( $post_slug == self::PATHWAY_2_SLUG ) :
				$js_file = 'progress-map-2/js';
			endif;

			wp_enqueue_script( [
				'createjs/js',
				$js_file,
			] );

			wp_localize_script( $js_file, 'image_url', array( 'path' => $image_url ) );
			wp_localize_script( $js_file, 'progress_data', $progress_data );
		}
	}

	/**
	 * @param int $user_id
	 * @param int $course_id
	 *
	 * @return array
	 */
	private function return_user_module_progress( $user_id = 0, $course_id = 0 ) {
		$modules_completed = array();
		$lessons_data      = array();

		// Get custom module order for course
		// returns an array of module IDs in Admin sorted order.
		$order = $this->get_course_module_order( $course_id );

		foreach ( $order as $module_id ) {

			$lessons_in_module = $this->get_lessons_in_module( $module_id, $course_id );

			$hasCompleted = false;

			// GET LESSON PROGRESS
			foreach ( $lessons_in_module as $lesson_in_module ) {
				$hasCompleted   = sensei_has_user_completed_lesson( $lesson_in_module->ID, $user_id );
				$lessons_data[] = array(
					'lesson'    => $lesson_in_module,
					'module'    => $module_id,
					'completed' => $hasCompleted,
				);
			}
			// GET MODULE PROGRESS
			foreach ( $lessons_in_module as $lesson_in_module ) {

				$hasCompleted = sensei_has_user_completed_lesson( $lesson_in_module->ID, $user_id );

				if ( $hasCompleted == false ) : break;
				endif;
			}

			if ( $hasCompleted ) : $modules_completed[] = $module_id;
			endif;
		}

		return array( $modules_completed, $lessons_data, $order );
	}

	private function get_course_module_order( $course_id = 0 ) {
		if ( $course_id ) {
			$order = get_post_meta( intval( $course_id ), '_module_order', true );

			return $order;
		}

		return false;
	}

	private function get_lessons_in_module( $module_id, $course_id ) {

		$lessons_in_module = array();

		$args    = array(
			'post_type'      => 'lesson',
			'post_status'    => 'publish',
			'posts_per_page' => - 1,
			'meta_query'     => array(
				array(
					'key'   => '_lesson_course',
					'value' => $course_id
				)
			),
			'meta_key'       => '_order_module_' . $module_id,
			'orderby'        => 'meta_value_num',
			'order'          => 'ASC',
//            'fields' => 'ids',
		);
		$lessons = get_posts( $args );

		if ( is_wp_error( $lessons ) || 0 >= count( $lessons ) ) {
			return 0;
		}

		foreach ( $lessons as $lesson ) {

			$term_for_any = wp_get_post_terms( $lesson->ID, 'module', array( 'fields' => "ids" ) );

			if ( $term_for_any[0] == $module_id ) {
				$lessons_in_module[] = $lesson;
			}
		}
        return $lessons_in_module;
	}

	// GATHER USER DATA AND GAME TRACKING FOR ALL GAMES
	private function gather_user_data( $gameID ) {
		$current_user     = wp_get_current_user();
		$user_id          = get_current_user_id();
		$wordquiz_game_id = $gameID;
		$progressStr      = get_user_meta( $user_id, 'game_tracking', true );
		$progressArr      = explode( ',', $progressStr );
		$found            = array_search( $wordquiz_game_id, $progressArr );

		if ( $found === false ):
			$completed = false;
		else:
			$completed = true;
		endif;

		$name = ( $current_user->user_firstname && $current_user->user_lastname ) ? $current_user->user_firstname . ' ' .
		                                                                            $current_user->user_lastname : $current_user->display_name;

		$user_data = array(
			'name'      => $name,
			'completed' => $completed
		);

		return $user_data;
	}

	public function save_game_progress_callback() {
		$user_id           = get_current_user_id();
		$completed_game_id = $_POST['game_id'];

		$progressStr = get_user_meta( $user_id, 'game_tracking', true );

		$progressArr = explode( ',', $progressStr );

		$found = array_search( $completed_game_id, $progressArr );

		if ( $found === false ):
			array_push( $progressArr, $completed_game_id );
			$glued   = implode( ',', $progressArr );
			$success = update_user_meta( $user_id, 'game_tracking', $glued );
			//delete_user_meta( $user_id, 'game_tracking' );
			echo 'added ' . $completed_game_id . ' to user id: ' . $user_id . ' result: ' . $success;
		else:
			echo 'id already exists';
		endif;
		wp_die();
	}

	public function save_session_callback() {
		if ( $_SESSION['game_progress'] ) {

			$found = false;

			foreach ( $_SESSION['game_progress'] as &$game_record ) {
				if ( $game_record['game_id'] == $_POST['game_id'] ) :
					// AMPERSAND (&$game_record) MAKES THIS A REFERENCE!!!!!!
					$game_record['game_status'] = $_POST['game_status'];
					$found                      = true;
					echo 'Updated game status: ' . $_POST['game_status'];
					break;
				endif;
			}

			if ( $found === false ):
				$_SESSION['game_progress'][] = array(
					'game_id'     => $_POST['game_id'],
					'game_status' => $_POST['game_status']
				);
				echo 'Saved NEW game status: ' . $_POST['game_status'];
			endif;

		} else {

			$_SESSION['game_progress'][] = array(
				'game_id'     => $_POST['game_id'],
				'game_status' => $_POST['game_status']
			);
			echo 'Saved to EMPTY session data with status: ' . $_POST['game_status'];
		}

		wp_die(); // need this to prevent the default server response of "0"
	}

	public function return_game_status( $game_id ) {
		$game_status = null;
		if ( $_SESSION['game_progress'] ) {
			foreach ( $_SESSION['game_progress'] as $game_record ) {
				if ( $game_record['game_id'] == $game_id ) :
					$game_status = $game_record['game_status'];
					break;
				endif;
			}
		}

		return $game_status;
	}

	public function get_session_status_callback() {
		$message = $this->return_game_status( $_POST['game_id'] );
		echo stripslashes( $message );
		wp_die();
	}

	// Handle the post_type parameter given in get_terms function
	public function adl_terms_clauses( $clauses, $taxonomy, $args ) {
		if ( ! empty( $args['post_type'] ) ) {
			global $wpdb;

			$post_types = array();

			foreach ( $args['post_type'] as $cpt ) {
				$post_types[] = "'" . $cpt . "'";
			}

			if ( ! empty( $post_types ) ) {
				$clauses['fields'] = 'DISTINCT ' . str_replace( 'tt.*', 'tt.term_taxonomy_id, tt.term_id, tt.taxonomy, tt.description, tt.parent', $clauses['fields'] ) . ', COUNT(t.term_id) AS count';
				$clauses['join'] .= ' INNER JOIN ' . $wpdb->term_relationships . ' AS r ON r.term_taxonomy_id = tt.term_taxonomy_id INNER JOIN ' . $wpdb->posts . ' AS p ON p.ID = r.object_id';
				$clauses['where'] .= ' AND p.post_type IN (' . implode( ',', $post_types ) . ')';
				$clauses['where'] .= " AND p.post_status NOT IN ('auto-draft','trash')";
				$clauses['orderby'] = 'GROUP BY t.term_id ' . $clauses['orderby'];
			}
		}

		return $clauses;
	}

	// WORK AROUND FOR SENSEI'S INABILITY TO DISPLAY FEEDBACK AND ADJUST UI
	// AFTER USER CLICKS COMPLETE LESSON BUTTON (THE PROBLEM IS WITH USING TIMBER + SENSEI)

	public function hook_into_single_lesson_before( $lesson_id ) {
		$user_id = get_current_user_id();
		$nukeIt  = 'no';

		// PREVENT A PAGE REFRESH FROM DISPLAYING AN EXTRA CONGRATS MESSAGE
		if ( sensei_has_user_completed_lesson( $lesson_id, $user_id ) ) {
			return;
		}
		if ( $_POST['quiz_action'] == 'lesson-complete' ) {

			$nukeIt            = 'yes';
			$next_prev_lessons = sensei_get_prev_next_lessons( $lesson_id );

			if ( $next_prev_lessons['next_lesson'] != 0 ) {
				$next_lesson = get_permalink( $next_prev_lessons['next_lesson'] );
				echo '<div class="sensei-message tick">Congratulations! You have passed this lesson!';

				// ONLY ADD NEXT LESSON BUTTON IF THERE'S ANY LEFT
				echo '<a class="next-lesson" href="';
				echo $next_lesson;
				echo '" rel="next"><span class="meta-nav"></span>Next Lesson</a>';

				// VIEW MY PROGRESS MAP BUTTON
				echo '<a class="next-lesson" href="';
				echo '/course/online-pathway-program/';
				echo '" rel="next"><span class="meta-nav"></span>View My Progress</a>';

			} else {
				echo '<div class="sensei-message tick">Congratulations! You have passed this lesson and completed the course!';
			}
			echo '</div>';
		}
		?>
        <script>
            function removeCompleteButtonForm() {

                var nukeit = "<?php echo $nukeIt; ?>";

                if (nukeit == 'yes') {
                    var $complete_lesson_button_form = $(".lesson_button_form");
                    $complete_lesson_button_form.css("display", "none");
                }
            }
        </script>
		<?php
	}
}

$game_scripts = new GameScripts;