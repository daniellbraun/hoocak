<?php

/**
 * Register custom taxonomies.
 *
 */
class Language_Games_Custom_Taxonomies {
	/**
	 * Initialize class and add our actions and hooks.
	 */
	public function __construct() {
		add_action( 'init', array( $this, 'register_language_games_taxonomy' ) );
	}

	/**
	 * Register a custom tax.
	 */
	public function register_language_games_taxonomy() {
		register_taxonomy(
			'language_games_word_category',
            array(
                'word_game',
                'word_quiz'
            ),
			array(
				'labels' => array(
					'add_new_item' => 'Add New Word Category',
					'name'         => 'Word Category',
					'edit_item'    => 'Edit Word Category',
					'update_item'  => 'Update Word Category',
				),
                'show_admin_column' => true,
                'rewrite'      => array( 'slug' => 'word-category' ),
				'hierarchical' => true,
			)
		);
	}
}

$adl_custom_taxonomies = new Language_Games_Custom_Taxonomies;