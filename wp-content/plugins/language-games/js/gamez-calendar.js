var statusMessages = {
    complete: "You have completed this game by seeing the Hoocąk word for each month of the year and each day of the week",
    partialComplete: "You've started learning Hoocąk names for the months of the year and days of the week but have yet to complete this game.",
    unstarted: "This is your first time here! In the first part of this game, match the Hoocąk names for each month of the " +
        "year by dragging a line from the Hoocąk name to the correct English one in the center. When you have completed " +
        "this, you will then be asked to match the Hoocąk names for each day of the week with the corrrect English name."
};

updateStatusMessage();

var months = [
    {
        'hochunkword': 'Hųųc wiconįra wiira',
        'englishword': 'January',
        'translate': 'First Bear Moon',
        'audio': 'january.mp3'
    },
    {
        'hochunkword': 'Hųųc wiihiraagnįra',
        'englishword': 'February',
        'translate': 'Last Bear Moon',
        'audio': 'february.mp3'
    },
    {
        'hochunkword': 'Wake hikiruxe wiira',
        'englishword': 'March',
        'translate': 'Raccoon Mating Moon',
        'audio': 'march.mp3'
    },
    {
        'hochunkword': 'Hoo hirogįnįną wiira',
        'englishword': 'April',
        'translate': 'Fish Appearing Moon',
        'audio': 'april.mp3'
    },
    {
        'hochunkword': 'Mąą hitawus hii wiira',
        'englishword': 'May',
        'translate': 'Earth Drying Moon',
        'audio': 'may.mp3'
    },
    {
        'hochunkword': 'Mąą hiną\'ų wiira',
        'englishword': 'June',
        'translate': 'Earth Cultivating Moon',
        'audio': 'june.mp3'
    },
    {
        'hochunkword': 'Waxoc wiira',
        'englishword': 'July',
        'translate': 'Corn Tasseling Moon',
        'audio': 'july.mp3'
    },
    {
        'hochunkword': 'Watajox hii wiira',
        'englishword': 'August',
        'translate': 'Corn Popping Moon',
        'audio': 'august.mp3'
    },
    {
        'hochunkword': 'Hųųwą žuuk wiira',
        'englishword': 'September',
        'translate': 'Elk Calling Moon',
        'audio': 'september.mp3'
    },
    {
        'hochunkword': 'Caa mąą hinąğo wiira',
        'englishword': 'October',
        'translate': 'Deer Pawing Moon',
        'audio': 'october.mp3'
    },
    {
        'hochunkword': 'Caa hikiruxe wiira',
        'englishword': 'November',
        'translate': 'Deer Mating Moon',
        'audio': 'november.mp3'
    },
    {
        'hochunkword': 'Caa hee wakšų wiira',
        'englishword': 'December',
        'translate': 'Deer Antler Shedding Moon',
        'audio': 'december.mp3'
    }

];
var daysOfWeek = [
    {
        'hochunkword': 'Haramįhe Hąąp',
        'englishword': 'Sunday',
        'audio': 'sunday.mp3'
    },
    {
        'hochunkword': 'Hąąpcek',
        'englishword': 'Monday',
        'audio': 'monday.mp3'
    },
    {
        'hochunkword': 'Hąąp Hinųpahą',
        'englishword': 'Tuesday',
        'audio': 'tuesday.mp3'
    },
    {
        'hochunkword': 'Hąąp Hitanįhą',
        'englishword': 'Wednesday',
        'audio': 'wednesday.mp3'
    },
    {
        'hochunkword': 'Hąąp Hijopahą',
        'englishword': 'Thursday',
        'audio': 'thursday.mp3'
    },
    {
        'hochunkword': 'Hąąp Hisacąhą',
        'englishword': 'Friday',
        'audio': 'friday.mp3'
    },
    {
        'hochunkword': 'Hąąp Hoinįge /\nWaruwįhąąp',
        'englishword': 'Saturday',
        'audio': 'saturday.mp3'
    }
];

var s = new createjs.Shape();
s.graphics.setStrokeStyle(2);
s.graphics.beginStroke('rgba(255, 255, 255, 1)');
stage.addChild(s);

var boxSize = 150;
var boxGraphics = new createjs.Graphics();
boxGraphics.beginFill('rgba(60, 60, 60, 0.7)');
boxGraphics.setStrokeStyle(2);
boxGraphics.beginStroke('rgba(100, 100, 100, 1)');
boxGraphics.drawRect(0, 0, boxSize, boxSize);
var faderGraphics = new createjs.Graphics();
faderGraphics.beginFill('rgba(255, 255, 204, 1)');
faderGraphics.drawRect(0, 0, boxSize, boxSize);

var connection = null;
var currentDragger = null;
var correctCount = 0;
var correctTotal;
var isDrawing = false;

var headerText = new createjs.Text("Hoocąk Month Names", "bold 50px Arial", "#FFF");
headerText.shadow = new createjs.Shadow('#000000', 5, 5, 5);
headerText.textAlign = 'center';
var subHeaderText = new createjs.Text("\nClick and drag a line from the Hoocąk month name to the correct month", "bold 17px Arial", "#FFF");
subHeaderText.shadow = new createjs.Shadow('#000000', 2, 2, 2);
subHeaderText.textAlign = 'center';
headerText.y = 20;
headerText.x = canvas.width / 2;
subHeaderText.y = headerText.y + headerText.getMeasuredHeight();
subHeaderText.x = headerText.x;

var gamePart = 1;

var saveInit = true;

function init() {

    correctCount = 0;
    stage.addChild(headerText, subHeaderText);
    headerText.alpha = subHeaderText.alpha = 0;
    correctTotal = months.length;

    var audio_days_manifest = [];
    for (var i = 0; i < daysOfWeek.length; i++) {
        daysOfWeek[i].audio_id = 'audio_days_' + i;
        audio_days_manifest.push(
            {
                id: 'audio_days_' + i,
                src: audio_url + 'calendar/' + daysOfWeek[i].audio
            }
        );
    }
    var audio_months_manifest = [];
    for (i = 0; i < months.length; i++) {
        months[i].audio_id = 'audio_months_' + i;
        audio_months_manifest.push(
            {
                id: 'audio_months_' + i,
                src: audio_url + 'calendar/' + months[i].audio
            }
        );
    }

    var loadingText = new createjs.Text('', "bold 45px Arial", "white");
    loadingText.textAlign = 'center';

    var preloaded_audio = new createjs.LoadQueue();
    preloaded_audio.installPlugin(createjs.Sound);
    stage.addChild(loadingText);
    preloaded_audio.on('progress', function (event) {
        var loadProgress = Math.round(100 * event.progress);
        loadingText.text = 'Loading: ' + loadProgress + '%';
        loadingText.x = canvas.width / 2;
        loadingText.y = canvas.height / 2;
        if (loadProgress == 100) {
            stage.removeChild(loadingText);
        }
        stage.update();
    });
    preloaded_audio.on('complete', function () {
        createjs.Tween.get(headerText).to({'alpha': 1}, 500);
        createjs.Tween.get(subHeaderText).to({'alpha': 1}, 500);
        placeMonthNames();
        placeMonthsGrid();
    });
    preloaded_audio.loadManifest(audio_days_manifest);
    preloaded_audio.loadManifest(audio_months_manifest);
}
function placeMonthNames() {
    var vertDivider = canvas.height / (months.length / 2);
    var vertPos = 0;
    var monthsShuffled = months.slice();

    var monthNameContainerLeft = new createjs.Container().set({name: "leftContainer"});
    var monthNameContainerRight = new createjs.Container().set({name: "rightContainer"});

    shuffle(monthsShuffled);

    for (var i = 0; i < monthsShuffled.length; i++) {
        var monthDragger = createButton(250, 70, monthsShuffled[i].hochunkword, '22px');
        monthDragger.name = "monthDragger_" + i;
        monthDragger.y = vertPos;
        monthDragger.x = 0;
        vertPos += vertDivider;
        if (i + 1 == monthsShuffled.length / 2) vertPos = 0;
        if (i < monthsShuffled.length / 2) monthNameContainerLeft.addChild(monthDragger);
        if (i >= monthsShuffled.length / 2) monthNameContainerRight.addChild(monthDragger);

        monthDragger.listener = monthDragger.on("mousedown", setLineStart);
        monthDragger.data = monthsShuffled[i];

        monthDragger.mouseEnabled = false;
    }


    monthNameContainerRight.x = canvas.width - monthNameContainerRight.getBounds().width - 20;
    monthNameContainerLeft.x = 20;
    monthNameContainerLeft.y = canvas.height / 2 - monthNameContainerLeft.getBounds().height / 2;
    monthNameContainerRight.y = canvas.height / 2 - monthNameContainerRight.getBounds().height / 2;

    stage.addChild(monthNameContainerLeft);
    stage.addChild(monthNameContainerRight);

    // animate everyone in!
    redrawOn = true;
    var tweenCount = monthNameContainerLeft.children.length;
    for (i = 0; i < monthNameContainerLeft.children.length; i++) {
        var leftDragger = monthNameContainerLeft.children[i];
        var leftHomeY = leftDragger.y;
        leftDragger.y = -(monthNameContainerLeft.y + leftDragger.getBounds().height + 1);
        createjs.Tween.get(leftDragger).wait(i * 150)
            .to({'y': leftHomeY}, 1000, createjs.Ease.getPowIn(4))
            .call(function () {
                this.mouseEnabled = true;
                tweenCount--;
                if (tweenCount == 0) {
                    redrawOn = false;
                    stage.update();
                }
            });
    }

    for (i = 0; i < monthNameContainerRight.children.length; i++) {
        var rightDragger = monthNameContainerRight.children[i];
        var rightHomeY = rightDragger.y;
        rightDragger.y = -(monthNameContainerRight.y + rightDragger.getBounds().height + 1);
        createjs.Tween.get(rightDragger).wait(i * 150)
            .to({'y': rightHomeY}, 1000, createjs.Ease.getPowIn(4))
            .call(function () {
                this.mouseEnabled = true;
            });
    }


}
function placeMonthsGrid() {
    var vertPos = 0;
    var horiPos = 0;
    var monthBoxesContainer = new createjs.Container();
    for (var i = 0; i < months.length; i++) {
        var monthBoxContainer = new createjs.Container();
        var monthBox = new createjs.Shape(boxGraphics).set({
            name: "target"
        });
        monthBox.setBounds(0, 0, boxSize, boxSize);
        monthBox.data = months[i];

        var faderBox = new createjs.Shape(faderGraphics);
        faderBox.alpha = 0;
        monthBox.fader = faderBox;
        monthBox.complete = false;

        setFadeBehavior(monthBox);

        var monthText = new createjs.Text(months[i].englishword, "bold 16px Arial", "white");
        monthText.name = "monthText";
        monthText.textAlign = "center";
        monthText.maxWidth = boxSize - 10;
        monthText.x = boxSize / 2;
        monthText.y = boxSize / 2;
        monthText.textBaseline = "middle";
        monthText.shadow = new createjs.Shadow('#000000', 2, 2, 2);
        monthBox.textField = monthText;

        monthBoxContainer.setBounds(0, 0, boxSize, boxSize);
        monthBoxContainer.addChild(faderBox, monthBox, monthText);

        if (horiPos % 4 == 0 && horiPos > 0) {
            horiPos = 0;
            vertPos++;
        }
        monthBoxContainer.x = horiPos * boxSize;
        monthBoxContainer.y = vertPos * boxSize;
        horiPos++;

        monthBoxesContainer.addChild(monthBoxContainer);
    }
    stage.addChild(monthBoxesContainer);
    monthBoxesContainer.regX = monthBoxesContainer.getBounds().width / 2;
    monthBoxesContainer.regY = monthBoxesContainer.getBounds().height / 2;
    monthBoxesContainer.x = canvas.width / 2;
    monthBoxesContainer.y = canvas.height - (monthBoxesContainer.getBounds().height / 2) - 20;

    monthBoxesContainer.alpha = 0;
    createjs.Tween.get(monthBoxesContainer).to({'alpha': 1}, 500);
}

var playingMonthAudio;
function setLineStart(event) {

    currentDragger = event.target;

    if (playingMonthAudio) {
        playingMonthAudio.stop();
    }
    playingMonthAudio = createjs.Sound.play(currentDragger.data.audio_id,
        {
            interrupt: createjs.Sound.INTERRUPT_NONE,
            loop: 0,
            volume: 1
        }
    );

    var globalCoords = event.target.parent.localToGlobal(event.target.x, event.target.y);

    var parentContainer = event.target.parent.name;

    if (parentContainer == "leftContainer") globalCoords.x += event.target.getBounds().width;

    if (parentContainer == "dayDraggersContainer") {
        globalCoords.x += event.target.getBounds().width / 2;
        globalCoords.y -= event.target.getBounds().height / 2;
    }

    globalCoords.y += event.target.getBounds().height / 2;

    connection = new createjs.Shape().set({
        x: globalCoords.x,
        y: globalCoords.y,
        mouseEnabled: false
        // graphics: new createjs.Graphics().s("#FFF").dc(0,0,50)
    });
    stage.addChild(connection);
    stage.addEventListener("stagemousemove", drawLine);
    stage.addEventListener("stagemouseup", endDraw);
}
function drawLine(event) {
    isDrawing = true;
    connection.graphics.clear()
        .s("#01a2a6")
        .ss(3, "round")
        .mt(0, 0).lt(stage.mouseX - connection.x, stage.mouseY - connection.y);
    stage.update();
}
function endDraw() {
    isDrawing = false;
    var target, targets = stage.getObjectsUnderPoint(stage.mouseX, stage.mouseY);

    for (var i = 0; i < targets.length; i++) {
        if (targets[i].name == "target") {
            target = targets[i];
            break;
        }
    }

    if (target != null && currentDragger.data.englishword == target.data.englishword) {
        var globalCoords = target.parent.localToGlobal(target.x, target.y);

        globalCoords.x += target.getBounds().width / 2;
        globalCoords.y += target.getBounds().height / 2;

        currentDragger.off("mousedown", currentDragger.listener);
        currentDragger.indicator.alpha = 1;
        currentDragger.mouseEnabled = false;

        connection.graphics.clear()
            .s("#01a2a6")
            .ss(5, "round")
            .mt(0, 0).lt(globalCoords.x - connection.x, globalCoords.y - connection.y);
        connection.alpha = .5;
        stage.addChildAt(connection, 0);
        stage.update();

        var dialogString = "Thats right!\n" +
            target.data.hochunkword +
            " is the Hoocąk word for " + target.data.englishword;

        if (target.data.translate) dialogString += "\nLiterally translated it means: " +
            target.data.translate;

        dialogbox(dialogString, setTargetText);

        // UPDATING SESSION TO SHOW GAME IS IN PROGRESS
        // BUT NOT COMPLETE

        if (saveInit) {
            saveInit = false;
            if (game_status != 'in progress' && game_status != 'complete') {
                saveSession('in progress');
            }
        }

        function setTargetText() {
            target.textField.alpha = 0;
            target.complete = true;
            target.textField.text = target.data.hochunkword + "\n\n(" + target.data.englishword + ")";
            target.textField.color = "#01a2a6";

            target.textField.y = target.getBounds().height / 2 - target.textField.getMeasuredHeight() / 2;

            createjs.Tween.get(target.textField, {override: true}).to({'alpha': 1}, 300).wait(1500).call(isEnd);

            correctCount++;

            function isEnd() {
                if (correctCount == correctTotal) {
                    stage.cache(0, 0, canvas.width, canvas.height, 2);
                    stage.regX = canvas.width / 2;
                    stage.regY = canvas.height / 2;
                    stage.x = canvas.width / 2;
                    stage.y = canvas.height / 2;

                    var stageScaleTween = createjs.Tween.get(stage).to({
                        'scaleX': 10,
                        'scaleY': 10,
                        'alpha': 0
                    }, 1000, createjs.Ease.getPowIn(4))
                        .call(resetStage);
                    stageScaleTween.on('change', function(){
                        stage.update();
                    })
                }
            }
        }
    } else {
        if (!target || target.complete || target.currentTxtTween) {
            stage.removeChild(connection);
        } else if (!target.currentTxtTween && !target.complete) {
            stage.removeChild(connection);
            target.textField.alpha = 0;
            target.priorText = target.textField.text;
            target.textField.text = "Incorrect";
            target.textField.color = "darkred";
            target.currentTxtTween = createjs.Tween
                .get(target.textField, {override: true})
                .to({'alpha': 1}, 300)
                .wait(2000)
                .to({'alpha': 0}, 300)
                .call(
                    function () {
                        target.textField.text = target.priorText;
                        target.textField.color = "white";
                    })
                .to({'alpha': 1}, 300).call(
                    function () {
                        target.currentTxtTween = null
                    });
        }
    }
    stage.removeEventListener("stagemousemove", drawLine);
    stage.removeEventListener("stagemouseup", endDraw);
}

function resetStage() {
    stage.uncache();
    stage.removeAllChildren();
    stage.alpha = 1;
    stage.scaleX = 1;
    stage.scaleY = 1;
    correctCount = 0;
    if (gamePart == 1) {
        placeHoocakDaysOfWeek();
        correctTotal = daysOfWeek.length;
        gamePart = 2;
    } else if (gamePart = 2) {
        finishGame();
        if (game_status != 'complete') {
            saveSession('complete');
            game_status = 'complete';
        }
    }
}

function placeHoocakDaysOfWeek() {

    headerText.text = "Hoocąk Days Of The Week Names";
    subHeaderText.text = "\nClick and drag a line up from the Hoocąk day of the week name to the correct day";
    stage.addChild(headerText, subHeaderText);

    headerText.alpha = subHeaderText.alpha = 0;

    createjs.Tween.get(headerText).to({'alpha': 1}, 500);
    createjs.Tween.get(subHeaderText).to({'alpha': 1}, 500);

    var dayDraggersContainer = new createjs.Container();
    dayDraggersContainer.name = "dayDraggersContainer";

    var daysOfWeekShuffled = daysOfWeek.slice();
    shuffle(daysOfWeekShuffled);

    for (var i = 0; i < daysOfWeekShuffled.length; i++) {
        var dayDragger = createButton(140, 60, daysOfWeekShuffled[i].hochunkword, '16px');
        dayDragger.x = (dayDragger.getBounds().width + 10) * i;
        dayDraggersContainer.addChild(dayDragger);
        dayDragger.textField.textBaseline = 'top';
        dayDragger.textField.y = dayDragger.getBounds().height / 2 - dayDragger.textField.getMeasuredHeight() / 2;
        dayDragger.listener = dayDragger.on("mousedown", setLineStart);
        dayDragger.data = daysOfWeekShuffled[i];
    }
    stage.addChild(dayDraggersContainer);
    dayDraggersContainer.regX = dayDraggersContainer.getBounds().width / 2;
    dayDraggersContainer.x = canvas.width / 2;
    dayDraggersContainer.y = canvas.height / 1.2;

    // animate everyone in!

    for (i = 0; i < dayDraggersContainer.children.length; i++) {
        var dayBoxCtr = dayDraggersContainer.children[i];
        var homeY = dayBoxCtr.y;
        dayBoxCtr.alpha = 0;
        dayBoxCtr.y += 100;
        var hochunkDaysTween = createjs.Tween.get(dayBoxCtr).wait(100 * i).to({'y': homeY, 'alpha': 1}, 1000, createjs.Ease.getPowIn(4));
        hochunkDaysTween.on('change', function(){
            stage.update();
        });
    }

    placeEnglishDaysOfWeek()
}

function placeEnglishDaysOfWeek() {

    var daysEnglishContainer = new createjs.Container();

    for (var i = 0; i < daysOfWeek.length; i++) {

        var dayBoxContainer = new createjs.Container();
        var dayBox = new createjs.Shape(boxGraphics).set({
            name: "target"
        });
        dayBox.setBounds(0, 0, boxSize, boxSize);
        dayBox.data = daysOfWeek[i];

        var faderBox = new createjs.Shape(faderGraphics);
        faderBox.alpha = 0;
        dayBox.fader = faderBox;

        setFadeBehavior(dayBox);

        var dayText = new createjs.Text(daysOfWeek[i].englishword, "bold 16px Arial", "white");
        dayText.name = "dayText";
        dayText.maxWidth = boxSize - 10;
        dayText.textAlign = "center";
        dayText.x = boxSize / 2;
        dayText.y = boxSize / 2;
        dayText.textBaseline = "middle";
        dayText.shadow = new createjs.Shadow('#000000', 2, 2, 2);
        dayBox.textField = dayText;

        dayBoxContainer.setBounds(0, 0, boxSize, boxSize);
        dayBoxContainer.addChild(faderBox, dayBox, dayText);

        dayBoxContainer.x = i * (boxSize + 10);

        daysEnglishContainer.addChild(dayBoxContainer);
    }
    stage.addChild(daysEnglishContainer);
    daysEnglishContainer.regX = daysEnglishContainer.getBounds().width / 2;
    daysEnglishContainer.x = canvas.width / 2;
    daysEnglishContainer.y = canvas.height / 3.5;

    // animate everyone in!

    for (i = 0; i < daysEnglishContainer.children.length; i++) {
        var dayBoxCtr = daysEnglishContainer.children[i];
        var homeY = dayBoxCtr.y;
        dayBoxCtr.alpha = 0;
        dayBoxCtr.y -= 100;
        var englishDaysTween = createjs.Tween.get(dayBoxCtr).wait(100 * i).to({'y': homeY, 'alpha': 1}, 1000, createjs.Ease.getPowIn(4));
        englishDaysTween.on('change', function(){
            stage.update();
        });
    }
}

function setFadeBehavior(d) {

    var displayObject = d;
    var overTween;
    var outTween;
    var tweenMouseUp;

    displayObject.on("mouseover", function () {
        if (isDrawing && !displayObject.complete) {
            overTween = createjs.Tween.get(this.fader, {override: true}).to({'alpha': 1}, 300);
            overTween.on('change', updateStage);
        }
    });
    displayObject.on("mouseout", function () {
        outTween = createjs.Tween.get(this.fader, {override: true}).to({'alpha': 0}, 300);
        outTween.on('change', updateStage);
    });
    stage.on("stagemouseup", function () {
        tweenMouseUp = createjs.Tween.get(displayObject.fader, {override: true}).to({'alpha': 0}, 300);
        tweenMouseUp.on('change', updateStage);
    });

    function updateStage() {
        stage.cursor = "pointer";
        stage.update();
    }
}
function finishGame() {
    headerText.text = "Now you've learned about the Hoocąk words\nfor months and days of the week";
    subHeaderText.text = "\nContinue to familiarize yourself with these words by clicking \"Replay\"" +
        "or learn something else by clicking \"Game Menu\"";
    subHeaderText.y = headerText.y + headerText.getBounds().height + 20;
    stage.addChild(headerText, subHeaderText);

    headerText.alpha = subHeaderText.alpha = 0;

    createjs.Tween.get(headerText).to({'alpha': 1}, 500);
    createjs.Tween.get(subHeaderText).to({'alpha': 1}, 500);

    var replayButton = createButton(150, 70, "Replay", "20px");
    var gameMenu = createButton(150, 70, "Game Menu", "20px");

    replayButton.y = 300;
    gameMenu.y = 300;

    replayButton.alpha = gameMenu.alpha = 0;

    gameMenu.x = (canvas.width / 2) + 10;
    replayButton.x = (canvas.width / 2) - replayButton.getBounds().width - 10;

    createjs.Tween.get(replayButton).to({'alpha': 1}, 500);

    var oneOfTheTweens = createjs.Tween.get(gameMenu).to({'alpha': 1}, 500);
    oneOfTheTweens.on('change', function(){
        stage.update();
    });

    replayButton.on("click", function() {
        replay();
    });
    gameMenu.on("click", function() {
        window.location.href = gamesURL;
    });
    function replay() {
        gamePart = 1;
        stage.removeAllChildren();
        updateStatusMessage();
        correctCount = 0;
        correctTotal = months.length;
        stage.addChild(headerText, subHeaderText);
        headerText.text = "Hoocąk Month Names";
        subHeaderText.text = "\nClick and drag a line from the Hoocąk month name to the correct month";
        headerText.y = 20;
        headerText.x = canvas.width / 2;
        subHeaderText.y = headerText.y + headerText.getMeasuredHeight();
        headerText.alpha = subHeaderText.alpha = 0;
        createjs.Tween.get(headerText).to({'alpha': 1}, 500);
        createjs.Tween.get(subHeaderText).to({'alpha': 1}, 500);
        placeMonthNames();
        placeMonthsGrid();
    }
    stage.addChild(replayButton, gameMenu);
}
setTimeout(function () {
    init();
}, 3000);