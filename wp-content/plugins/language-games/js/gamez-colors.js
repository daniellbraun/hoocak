var statusMessages = {
    complete: "You have completed this game by correctly painting with the correct colors for each of the 6 colors tested.",
    partialComplete: "You've started learning the Hoocąk words for the colors above, but have yet to correctly choose every color.",
    unstarted: "This is your first time here! Above you will be presented with the Hoocąk words for 6 colors. Choose the correct color " +
    "by clicking on it and then painting the hand on the right. Click \"All Done\" when you are done painting and after 6 tries " +
    "you will be shown which ones you selected correctly."
};

updateStatusMessage();

var canvasBG, canvasDrawing, stageDrawing, stage;
canvasBG = document.getElementById("gamezCanvas");
stage = new createjs.Stage(canvasBG);

var drawingCanvas;
var oldPt;
var oldMidPt;
var color;
var stroke;

var saveButton;
var resetColorButton;


var Mouse = {'x': 0, 'y': 0};

var canvasBGWrapper;

var colorPalette = [
    {
        'name': 'šuuc',
        'english': 'red',
        'hex': '#FF0000',
        'audio': 'suuc - red.mp3'
    },
    {
        'name': 'caasakšokąwą',
        'english': 'pink',
        'hex': '#FB8DF0',
        'audio': 'caasaksokawa - pink.mp3'
    },
    {
        'name': 'haapsįcco',
        'english': 'purple',
        'hex': '#8F42F4',
        'audio': 'haapsicco - purple.mp3'
    },
    {
        'name': 'zii',
        'english': 'yellow',
        'hex': '#FFFF00',
        'audio': 'zii - yellow.mp3'
    },
    {
        'name': 'wažązi',
        'english': 'orange',
        'hex': '#FF9900',
        'audio': 'wazazi - orange.mp3'
    },
    {
        'name': 'seep',
        'english': 'black',
        'hex': '#000000',
        'audio': 'seep - black.mp3'
    }
];

var savedThumbnails = [];
var brush;
var colorNames = [];
var currentQuestion = 0;
var questionContainer = new createjs.Container();
var questionField1 = new createjs.Text("", "30px Arial", "#FFFFFF");
var questionField2 = new createjs.Text("", "bold 60px Arial", "#FFFFFF");
var questionField3 = new createjs.Text("", "30px Arial", "#FFFFFF");
var questionField4 = new createjs.Text("", "bold 20px Arial", "#FFFFFF");
questionField1.textBaseline = "hanging";
questionField2.textBaseline = "hanging";
questionField3.textBaseline = "hanging";
questionField4.textBaseline = "hanging";
questionField1.textAlign = questionField2.textAlign = questionField3.textAlign = questionField4.textAlign = "center";
questionContainer.addChild(questionField1);
questionContainer.addChild(questionField2);
questionContainer.addChild(questionField3);
questionContainer.addChild(questionField4);
questionField1.shadow = new createjs.Shadow("#000000", 2, 2, 2);
questionField2.shadow = new createjs.Shadow("#000000", 3, 3, 3);
questionField3.shadow = new createjs.Shadow("#000000", 2, 2, 2);
questionField4.shadow = new createjs.Shadow("#000000", 2, 2, 2);

var buttonsContainer = new createjs.Container();
var handBitmap;
var chipsContainer;

var resultTextContainer = new createjs.Container();

var saveInit = true;
var redrawOn = false;

var loadingText = new createjs.Text('', "bold 45px Arial", "white");
loadingText.textAlign = 'center';

function init() {
    canvasBGWrapper = $("#gamezCanvas");
    canvasBGWrapper.after('<canvas id="gamezCanvasOverlay" width="600" height="600"></canvas>');

    canvasDrawing = $("#gamezCanvasOverlay");

    handBitmap = new createjs.Bitmap(image_url + 'hand.png');
    handBitmap.x = 600;

    stage.addChild(handBitmap);
    stage.addChild(questionContainer);

    saveButton = createButton(220, 50, "All done!");
    stage.addChild(saveButton);
    saveButton.on("click", saveThumbnail);
    saveButton.alpha = 0.5;
    saveButton.mouseEnabled = false;
    buttonsContainer.addChild(saveButton);

    createjs.Ticker.addEventListener("tick", tick);

    function tick(event) {
        if (redrawOn) {
            stage.update(event);
        }
    }

    var lsnr = stage.on('drawstart', function (evt) {
        //console.log('getMeasuredFPS: ', createjs.Ticker.getMeasuredFPS());
    });

    // enable touch interactions if supported on the current device:
    //createjs.Touch.enable(stage);

    // enabled mouse over / out events. 20 is the default
    stage.enableMouseOver(20);
    stage.mouseMoveOutside = true; // keep tracking the mouse even when it leaves the canvas

    // stage.update();

    stageDrawing = new createjs.Stage(canvasDrawing.get(0));

    stageDrawing.autoClear = false;
    stageDrawing.enableDOMEvents(true);

    createjs.Touch.enable(stageDrawing);
    createjs.Ticker.setFPS(30);

    drawingCanvas = new createjs.Shape();
    drawingCanvas.name = 'drawing_canvas';

    stageDrawing.addEventListener("stagemousedown", handleMouseDown);
    stageDrawing.addEventListener("stagemouseup", handleMouseUp);

    stageDrawing.addChild(drawingCanvas);
    stageDrawing.update();

    chipsContainer = new createjs.Container();
    for (var i = 0; i < colorPalette.length; i++) {
        var indChipContainer = new createjs.Container();
        var chip = new createjs.Shape();
        chip.graphics.beginFill(colorPalette[i].hex);
        chip.graphics.drawRect(0, 0, 70, 70);
        indChipContainer.addChild(chip);
        chip.x = i * 90;

        var chipIndicator = new createjs.Shape();
        chipIndicator.graphics.setStrokeStyle(4, 'round', 'round');
        chipIndicator.graphics.beginStroke('#FFFFFF');
        chipIndicator.graphics.drawRect(0, 0, 70, 70);
        indChipContainer.addChild(chipIndicator);
        chipIndicator.x = i * 90;
        chipIndicator.setBounds(0, 0, 70, 70);

        indChipContainer.indicator = chipIndicator;
        indChipContainer.color = colorPalette[i].hex;
        indChipContainer.indicator.alpha = 0;

        indChipContainer.on("click", setColor);
        indChipContainer.cursor = "pointer";
        chip.shadow = new createjs.Shadow("#000000", 3, 3, 5);

        chipsContainer.addChild(indChipContainer);

        colorNames.push(
            {
                name: colorPalette[i].name,
                audio_id: 'audio_' + i
            });

    }

    shuffle(colorNames);

    function setColor(event) {
        brush.colorSet = true;
        brush.graphics.clear();
        brush.graphics.beginFill(event.currentTarget.color);
        brush.graphics.drawCircle(0, 0, 20);
        for (var i = 0; i < chipsContainer.children.length; i++) {
            if (chipsContainer.children[i] == event.currentTarget) {
                chipsContainer.children[i].indicator.alpha = 1;
                color = chipsContainer.children[i].color;
            } else {
                var unused = chipsContainer.children[i];
                unused.indicator.alpha = 0;
                unused.mouseEnabled = false;
                var removeChipTween = createjs.Tween.get(unused).wait(i * 100).to({'y': canvasBG.height}, 500, createjs.Ease.backIn);
                removeChipTween.on('change', stageUpdate);
            }
        }
        buttonsContainer.alpha = 1;
    }

    stage.addChild(chipsContainer);
    chipsContainer.regX = chipsContainer.getBounds().width / 2;
    chipsContainer.x = canvasBG.width / 4;
    chipsContainer.y = canvasBG.height - chipsContainer.getBounds().height - 20;

    resetColorButton = createButton(220, 50, "Change my color!");
    resetColorButton.on("click", resetColorPalette);
    resetColorButton.x = saveButton.x + saveButton.getBounds().width + 30;
    buttonsContainer.addChild(resetColorButton);
    stage.addChild(buttonsContainer);
    buttonsContainer.y = chipsContainer.y - buttonsContainer.getBounds().height - 100;
    buttonsContainer.x = canvasBG.width / 4 - buttonsContainer.getBounds().width / 2;
    buttonsContainer.alpha = 0;

    brush = new createjs.Shape();
    brush.graphics.beginFill('#333333');
    brush.graphics.drawCircle(0, 0, 20);
    brush.alpha = 0;
    stage.addChild(brush);
    stage.on("stagemousemove", updateBrush);

    stageDrawing.on("mouseenter", enterDrawing);
    stageDrawing.on("mouseleave", exitDrawing);
    function enterDrawing() {
        if (brush.colorSet) brush.alpha = 1;
    }

    function exitDrawing() {
        brush.alpha = 0;
    }

    updateCanvasDimensions();
    askQuestion();
    // var audio_manifest = [];
    // for (i = 0; i < colorPalette.length; i++) {
    //     audio_manifest.push(
    //         {
    //             id: 'audio_' + i,
    //             src: audio_url + 'colors/' + colorPalette[i].audio
    //         }
    //     )
    // }
    // var preloaded_audio = new createjs.LoadQueue();
    // preloaded_audio.installPlugin(createjs.Sound);
    // stage.addChild(loadingText);
    // preloaded_audio.on('progress', function (event) {
    //     var loadProgress = Math.round(100 * event.progress);
    //     loadingText.text = 'Loading: ' + loadProgress + '%';
    //     loadingText.x = canvasBG.width / 2;
    //     loadingText.y = canvasBG.height / 2;
    //     if (loadProgress == 100) {
    //         stage.removeChild(loadingText);
    //     }
    //     stage.update();
    // });
    // preloaded_audio.on('complete', function () {
    //     askQuestion();
    // });
    // preloaded_audio.loadManifest(audio_manifest);
}
function askQuestion() {
    questionContainer.alpha = 0;
    var str1 = "Select the color below called:\n";
    var str2 = colorNames[currentQuestion].name;
    var str3 = "\nin the Hoocąk language.";
    var str4 = "\nPaint the hand with that color and\nclick \"All Done!\" when you are finished.";
    str4 += "\n\nWhen you have completed 6 hands,\nwe'll show you what you got right.";
    questionField1.text = str1;
    questionField2.text = str2;
    questionField3.text = str3;
    questionField4.text = str4;
    questionField2.y = questionField1.y + questionField1.getMeasuredHeight();
    questionField3.y = questionField2.y + questionField2.getMeasuredHeight();
    questionField4.y = questionField3.y + questionField3.getMeasuredHeight();

    questionContainer.x = canvasBG.width / 4;
    questionContainer.y = 40;
    var qContTween = createjs.Tween.get(questionContainer).to({'alpha': 1}, 500);
    qContTween.on('change', stageUpdate);
    setTimeout(function () {
        createjs.Sound.play(colorNames[currentQuestion].audio_id,
            {
                interrupt: createjs.Sound.INTERRUPT_NONE,
                loop: 0,
                volume: 1
            }
        );
    }, 1000);
}
function stageUpdate() {
    stage.update();
}
function saveThumbnail() {

    // UPDATING SESSION TO SHOW GAME IS IN PROGRESS
    // BUT NOT COMPLETE
    if (saveInit) {
        saveInit = false;
        if (game_status != 'in progress' && game_status != 'complete') {
            saveSession('in progress');
        }
    }

    var thumbnailContainer = new createjs.Container();
    var dupHand = new createjs.Bitmap(image_url + 'hand.png');

    thumbnailContainer.addChild(dupHand);

    var thumbnail = saveCanvasThumbnail(1);

    var returned = new createjs.Bitmap(thumbnail);
    thumbnailContainer.addChild(returned);

    var correct = false;
    var correctName = colorNames[currentQuestion].name;
    if (getChosenColorName().name == colorNames[currentQuestion].name) {
        correct = true;
    }

    savedThumbnails.push(
        {
            'chosen': getChosenColorName().name,
            'chosenEnglish': getChosenColorName().english,
            'chosenHex': getChosenColorName().hex,
            'thumb': thumbnailContainer,
            'correct': correct,
            'correctColor': correctName
        });

    thumbnailContainer.x = handBitmap.x;
    thumbnailContainer.y = handBitmap.y;

    stage.addChild(thumbnailContainer);

    stageDrawing.clear();
    color = null;
    currentQuestion++;
    var thumbnailTween;
    if (currentQuestion == colorPalette.length) {
        thumbnailTween = createjs.Tween.get(thumbnailContainer).to(
            {
                'scaleX': 0.1,
                'scaleY': 0.1,
                'x': 80 * currentQuestion,
                'y': buttonsContainer.y + buttonsContainer.getBounds().height + 20
            }, 500, createjs.Ease.quadIn).call(finished)
    } else {
        thumbnailTween = createjs.Tween.get(thumbnailContainer).to(
            {
                'scaleX': 0.1,
                'scaleY': 0.1,
                'x': 80 * (currentQuestion),
                'y': buttonsContainer.y + buttonsContainer.getBounds().height + 20
            }, 500, createjs.Ease.quadIn).call(resetColorPalette).call(askQuestion);
    }
    thumbnailTween.on('change', stageUpdate);
}
function getChosenColorName() {
    for (var i = 0; i < colorPalette.length; i++) {
        if (colorPalette[i].hex == color) {
            return colorPalette[i];
        }
    }
}

function finished() {
    buttonsContainer.mouseEnabled = false;
    brush.colorSet = false;
    redrawOn = true;

    createjs.Tween.get(buttonsContainer).to({'alpha': 0}, 500);
    createjs.Tween.get(questionContainer).to({'alpha': 0}, 500);
    createjs.Tween.get(chipsContainer).to({'alpha': 0}, 500);
    createjs.Tween.get(handBitmap).to({'alpha': 0}, 500).call(displayResults);

    canvasDrawing.get(0).height = 0;
}
function replay() {
    var resultContTween = createjs.Tween.get(resultTextContainer).to({'alpha': 0}, 500).call(function () {
        resultTextContainer.removeAllChildren()
    });
    resultContTween.on('change', stageUpdate);
    var savedThumbnailsCount = 0;
    for (var i = 0; i < savedThumbnails.length; i++) {
        createjs.Tween.get(savedThumbnails[i].thumb).to({'alpha': 0}, 500).call(removeThumbsChildren);

    }
    function removeThumbsChildren() {
        savedThumbnailsCount++;
        if (savedThumbnailsCount == savedThumbnails.length) {
            for (var i = 0; i < savedThumbnails.length; i++) {
                stage.removeChild(savedThumbnails[i].thumb)
            }
            resetInterface();
        }

    }

    function resetInterface() {
        savedThumbnails = [];
        canvasDrawing.get(0).height = canvasBG.height;

        buttonsContainer.alpha = 0;
        buttonsContainer.mouseEnabled = true;

        shuffle(colorNames);

        currentQuestion = 0;
        saveButton.alpha = 0.5;
        saveButton.mouseEnabled = false;
        for (var i = 0; i < chipsContainer.children.length; i++) {
            var chip = chipsContainer.children[i];
            chip.indicator.alpha = 0;
            chip.mouseEnabled = true;
            chip.y = 0;
        }
        redrawOn = true;
        createjs.Tween.get(questionContainer).to({'alpha': 1}, 500);
        createjs.Tween.get(chipsContainer).to({'alpha': 1}, 500);
        createjs.Tween.get(handBitmap).to({'alpha': 1}, 500).call(
            function () {
                redrawOn = false;
                stage.update();
            }
        );
        askQuestion();
    }

}

function displayResults() {
    var handDisplayCount = 0;
    var newHandPositions = [];
    for (var i = 0; i < savedThumbnails.length; i++) {
        savedThumbnails[i].thumb.regX = savedThumbnails[i].thumb.getBounds().width / 2;
        var xPos = (canvasBG.width / 7) * (i + 1);
        newHandPositions.push(xPos);
        var thumbTween = createjs.Tween.get(savedThumbnails[i].thumb).to(
            {
                'x': xPos,
                'y': 320,
                'scaleX': 0.3,
                'scaleY': 0.3
            }, 500).call(handTweenComplete);

        function handTweenComplete() {
            handDisplayCount++;
            if (handDisplayCount == savedThumbnails.length) {
                listAnswers();
            }
        }

        thumbTween.on('change', stageUpdate);
    }

    function listAnswers() {

        var numCorrect = 0;
        for (var i = 0; i < savedThumbnails.length; i++) {
            var correct = savedThumbnails[i].correct;
            var resultStr = "Incorrect :(";
            if (correct) resultStr = "Correct!";
            if (correct) numCorrect++;

            var detailsStr1 = "You painted with:\n";
            var detailsStr2 = savedThumbnails[i].chosen;
            var detailsStr3 = "\nwhich in Hoocąk\nmeans " + savedThumbnails[i].chosenEnglish + ".\n";
            var detailsStr4;
            var resultText_color;
            if (correct) {
                detailsStr4 = "The correct\ncolor was " + savedThumbnails[i].correctColor + "!\n\nGood Job!";
                resultText_color = '#0F0';
            } else {
                detailsStr4 = "But the correct\ncolor was " + savedThumbnails[i].correctColor;
                resultText_color = '#F00';
            }
            var detailsField1 = new createjs.Text(detailsStr1, 'bold 18px Arial', '#FFFFFF');
            var detailsField2 = new createjs.Text(detailsStr2, 'bold 28px Arial', savedThumbnails[i].chosenHex);
            detailsField2.maxWidth = 160;

            var detailsField3 = new createjs.Text(detailsStr3, 'bold 18px Arial', '#FFFFFF');
            var detailsField4 = new createjs.Text(detailsStr4, '18px Arial', '#FFFFFF');
            detailsField4.maxWidth = 160;

            detailsField1.textAlign = detailsField2.textAlign = detailsField3.textAlign = detailsField4.textAlign = 'center';

            detailsField1.x = detailsField2.x = detailsField3.x = detailsField4.x = newHandPositions[i];
            detailsField1.y = 100;
            detailsField2.y = detailsField1.y + detailsField1.getMeasuredHeight();
            detailsField3.y = detailsField2.y + detailsField2.getMeasuredHeight();
            detailsField4.y = detailsField3.y + detailsField3.getMeasuredHeight();

            var detailsField2ShadowClr = '#000000';
            if (savedThumbnails[i].chosenEnglish == "black") detailsField2ShadowClr = '#FFFFFF';

            var resultText = new createjs.Text(resultStr, 'bold 18px Arial', resultText_color);
            resultText.textAlign = 'center';
            resultText.x = newHandPositions[i];
            resultText.y = canvasBG.height - 100;

            detailsField1.shadow = new createjs.Shadow("#000000", 2, 2, 2);
            detailsField2.shadow = new createjs.Shadow(detailsField2ShadowClr, 2, 2, 2);
            detailsField3.shadow = new createjs.Shadow("#000000", 2, 2, 2);
            detailsField4.shadow = new createjs.Shadow("#000000", 2, 2, 2);
            resultText.shadow = new createjs.Shadow("#000000", 2, 2, 2);
            resultTextContainer.addChild
            (
                resultText,
                detailsField1,
                detailsField2,
                detailsField3,
                detailsField4
            );

        }
        var yourResults = "Your results: " + numCorrect + " out of " + savedThumbnails.length + " correct.";
        var resultsHeaderText = new createjs.Text(yourResults, 'bold 40px Arial', '#FFFFFF');
        resultsHeaderText.shadow = new createjs.Shadow("#000000", 3, 3, 3);
        resultsHeaderText.textAlign = 'center';
        resultsHeaderText.x = canvasBG.width / 2;
        resultsHeaderText.y = 20;
        resultTextContainer.addChild(resultsHeaderText);
        stage.addChild(resultTextContainer);
        resultTextContainer.alpha = 0;

        var resultTextTween = createjs.Tween.get(resultTextContainer).to({'alpha': 1}, 500).call(displayReplayButton);
        resultTextTween.on('change', stageUpdate);

        var replayButton = createButton(140, 50, "Replay");
        replayButton.x = canvasBG.width / 2 - replayButton.getBounds().width / 2;
        replayButton.y = canvasBG.height - replayButton.getBounds().height - 10;
        resultTextContainer.addChild(replayButton);
        replayButton.alpha = 0;
        replayButton.on("click", replay, null, true);

        redrawOn = false;
        function displayReplayButton() {
            var replayButtonTween = createjs.Tween.get(replayButton).to({'alpha': 1}, 500);
            replayButtonTween.on('change', stageUpdate);
        }

        if (numCorrect == savedThumbnails.length) {
            if (game_status != 'complete') {
                saveSession('complete');
                game_status = 'complete';
            }
        }
    }
}
function resetColorPalette() {
    brush.colorSet = false;
    brush.graphics.clear();

    saveButton.alpha = 0.5;
    saveButton.mouseEnabled = false;

    for (var i = 0; i < chipsContainer.children.length; i++) {
        var chip = chipsContainer.children[i];
        chip.indicator.alpha = 0;
        chip.mouseEnabled = true;
        var resetPaletteTween = createjs.Tween.get(chip).to({'y': 0}, 500);
        resetPaletteTween.on('change', stageUpdate);
    }
    buttonsContainer.alpha = 0;
    stageDrawing.clear();
}
function saveCanvasThumbnail(scale) {

    if (scale === undefined) scale = .3;

    var canvas = document.getElementById("gamezCanvasOverlay");
    var bitmap = new createjs.Bitmap(canvas);

    bitmap.cache(0, 0, canvas.width, canvas.height, scale);
    var base64 = bitmap.getCacheDataURL();

    return base64;
}

function handleMouseDown(event) {

    if (!event.primary) {
        return;
    }
    if (saveButton.mouseEnabled == false) {
        saveButton.alpha = 1;
        saveButton.mouseEnabled = true;
    }
    stroke = 40;
    oldPt = new createjs.Point(stageDrawing.mouseX, stageDrawing.mouseY);
    oldMidPt = oldPt.clone();
    stageDrawing.addEventListener("stagemousemove", handleMouseMove);
}
function updateBrush() {

    brush.x = stage.mouseX;
    brush.y = stage.mouseY;
    stage.update();
}

function handleMouseMove(event) {

    if (!event.primary) {
        return;
    }
    var midPt = new createjs.Point(oldPt.x + stageDrawing.mouseX >> 1, oldPt.y + stageDrawing.mouseY >> 1);

    drawingCanvas.graphics.clear()
        .setStrokeStyle(stroke, 'round', 'round')
        .beginStroke(color)
        .moveTo(midPt.x, midPt.y)
        .curveTo(oldPt.x, oldPt.y, oldMidPt.x, oldMidPt.y);

    oldPt.x = stageDrawing.mouseX;
    oldPt.y = stageDrawing.mouseY;

    oldMidPt.x = midPt.x;
    oldMidPt.y = midPt.y;

    stageDrawing.update();

}

function handleMouseUp(event) {
    if (!event.primary) {
        return;
    }
    stageDrawing.removeEventListener("stagemousemove", handleMouseMove);
}

function onWindowResize(e) {
    //right now, the stage instance, doesnt expose the canvas
    //context, so we have to get a reference to it ourselves
    var context = canvasDrawing.get(0).getContext("2d");

    //copy the image data from the current canvas
    var data = context.getImageData(0, 0,
        canvasDrawing.attr("width"),
        canvasDrawing.attr("height"));


    //update the canvas dimensions since the window
    //has resized. Note that changing canvas dimensions,
    //will cause it to be cleared
    updateCanvasDimensions();

    //copy the data back onto the resized canvas. It is possible
    //that if the previous canvas was larger than the newly sized one
    //that we will loose some pixels (as they will be cut off)
    context.putImageData(data, 0, 0);

    //note, we only have to do this because we have stage.autoClear set
    //to false, otherwise, the stage instance could redraw the entire canvas

    // //only rerender the stage if the Tick manager is not paused
    // if (!Tick.getPaused()) {
    //     //render stage
    //     stage.tick();
    // }
    stageDrawing.update();
}
//function that updates the size of the canvas based on the window size
// var canvasOffset;
function updateCanvasDimensions() {
    //note that changing the canvas dimensions clears the canvas.

    canvasDrawing.attr("height", $('#gamez').height());
    canvasDrawing.attr("width", $('#gamez').width() / 2);

    // canvasDrawing.attr("left", $('#gamez').width() / 2);
    canvasDrawing.css('left', $('#gamez').width() / 2);

    //save the canvas offset
    //console.log("canvasDrawing.offset():", canvasDrawing.offset());
    // canvasOffset = canvasDrawing.offset();
}
$(window).resize(onWindowResize);
function createButton(w, h, text) {
    var button_bg = new createjs.Shape();
    button_bg.graphics.beginFill('#01a2a6');
    button_bg.graphics.setStrokeStyle(2, 'round').beginStroke('#01a2a6');
    button_bg.graphics.drawRoundRect(0, 0, w, h, 5);
    button_bg.alpha = .5;

    var indicator = new createjs.Shape();
    indicator.graphics.beginFill('#01a2a6');
    indicator.graphics.setStrokeStyle(2, 'round').beginStroke('#01a2a6');
    indicator.graphics.drawRoundRect(0, 0, w, h, 5);
    indicator.alpha = 0;

    var button_text = new createjs.Text(text, "26px Arial", "#FFF");
    button_text.maxWidth = w;
    button_text.textAlign = "center";
    button_text.textBaseline = "middle";

    button_text.y = h / 2;
    button_text.x = w / 2;

    var buttonContainer = new createjs.Container();
    buttonContainer.mouseChildren = false;
    buttonContainer.cursor = "pointer";
    buttonContainer.addChild(indicator);
    buttonContainer.addChild(button_bg);
    buttonContainer.addChild(button_text);
    buttonContainer.indicator = indicator;

    handleMouse = function (event) {
        var button_target_alpha = (event.type == "mouseover") ? 1 : 0.5;
        createjs.Tween.get(button_bg, {override: true}).to({alpha: button_target_alpha}, 300);
    };

    buttonContainer.on("mouseover", handleMouse);
    buttonContainer.on("mouseout", handleMouse);

    buttonContainer.setBounds(0, 0, w, h);

    return buttonContainer;
}

function shuffle(array) {
    var currentIndex = array.length, temporaryValue, randomIndex;

    // While there remain elements to shuffle...
    while (0 !== currentIndex) {

        // Pick a remaining element...
        randomIndex = Math.floor(Math.random() * currentIndex);
        currentIndex -= 1;

        // And swap it with the current element.
        temporaryValue = array[currentIndex];
        array[currentIndex] = array[randomIndex];
        array[randomIndex] = temporaryValue;
    }

    return array;
}

setTimeout(function () {
    var audio_manifest = [];
    for (var i = 0; i < colorPalette.length; i++) {
        audio_manifest.push(
            {
                id: 'audio_' + i,
                src: audio_url + 'colors/' + colorPalette[i].audio
            }
        )
    }
    var preloaded_audio = new createjs.LoadQueue();
    preloaded_audio.installPlugin(createjs.Sound);
    stage.addChild(loadingText);
    preloaded_audio.on('progress', function (event) {
        var loadProgress = Math.round(100 * event.progress);
        loadingText.text = 'Loading: ' + loadProgress + '%';
        loadingText.x = canvasBG.width / 2;
        loadingText.y = canvasBG.height / 2;
        if (loadProgress == 100) {
            stage.removeChild(loadingText);
        }
        stage.update();
    });
    preloaded_audio.on('complete', function () {
        init();
    });
    preloaded_audio.loadManifest(audio_manifest);

}, 3000);
