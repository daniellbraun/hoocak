var stage;
var canvas;
canvas = document.getElementById("gamezCanvas");
stage = new createjs.Stage(canvas);

createjs.Ticker.timingMode = createjs.Ticker.RAF;
createjs.Ticker.setFPS(30);
//createjs.Ticker.addEventListener("tick", stage);
createjs.Ticker.addEventListener("tick", tick);
var redrawOn = false;
function tick(event) {
    if (redrawOn) {
        stage.update(event);
    }
}


var lsnr = stage.on('drawstart', function(evt) {
    //console.log('getMeasuredFPS: ', createjs.Ticker.getMeasuredFPS());
});

// setTimeout(function(){
//     redrawOn = false;
// }, 5000);

// enable touch interactions if supported on the current device:
// If we want to allow touch-scrolling from the canvas element we
// have to allow default gesture actions and to NOT allow EaselJS
// to prevent selection touches.
createjs.Touch.enable(stage, false, true);
stage.preventSelection = false;

// enabled mouse over / out events. 20 is the default
stage.enableMouseOver(60);
stage.mouseMoveOutside = true; // keep tracking the mouse even when it leaves the canvas

createjs.Sound.alternateExtensions = ["mp3"];

var gamesURL;
gamesURL = location.origin + "/learning-materials/?tab=games";

function createModal() {

    var modal_container = new createjs.Container();

    // warning box / modal dialog for user infos
    var warnbox = new createjs.Shape();
    warnbox.graphics.beginFill('#000');
    warnbox.alpha = 0.85;
    warnbox.graphics.drawRect(0, 0, canvas.width, canvas.height);
    warnbox.graphics.endFill();
    modal_container.addChild(warnbox);
    warnbox.on('mousedown', function (evt) {
        // do nothing
    });

    // confirm button for modal dialog box
    var buttonok = createButton(170, 50, "Continue");
    buttonok.x = canvas.width / 2 - 85;
    buttonok.y = canvas.height / 2 + 70;
    buttonok.name = 'continue_button';
    modal_container.addChild(buttonok);

    var label_info = new createjs.Text("Your message here", "30px Arial", "#F0F0F0");
    label_info.x = canvas.width / 2;
    label_info.y = canvas.height / 2 - 110;
    label_info.textAlign = 'center';
    label_info.lineWidth = 700;
    label_info.lineHeight = 50;
    label_info.name = "modal_text";
    modal_container.addChild(label_info);

    return modal_container;
}
var modal_container = createModal();

function dialogbox(msg, functionToCall) {
    var modal = stage.addChild(modal_container);
    var textChild = modal_container.getChildByName("modal_text");
    textChild.text = msg;
    modal.alpha = 0;

    var modalTween = createjs.Tween.get(modal).to({"alpha":1}, 500).call(setButton);
    modalTween.on("change", function(){
        stage.update();
    });

    var buttonok = modal.getChildByName("continue_button");
    buttonok.y = textChild.y + textChild.getMeasuredHeight();

    function setButton() {
        // 4th parameter marked true (once=true) to remove event
        // listener after clicked once and prevent listener from being
        // added with each successive call to setButton()
        buttonok.on("click", buttonFunction, null, true);
    }
    function buttonFunction(evt) {
        stage.removeChild(modal);
        stage.update();
        if (functionToCall) {
            functionToCall();
        }
    }
}
function createButton(w, h, text, f) {

    var fontSize = '26px';
    if (f) fontSize = f;

    var button_bg = new createjs.Shape();
    button_bg.graphics.beginFill('#01a2a6');
    button_bg.graphics.setStrokeStyle(2, 'round').beginStroke('#01a2a6');
    button_bg.graphics.drawRoundRect(0, 0, w, h, 5);
    button_bg.alpha = .5;

    var indicator = new createjs.Shape();
    indicator.graphics.beginFill('#01a2a6');
    indicator.graphics.setStrokeStyle(2, 'round').beginStroke('#01a2a6');
    indicator.graphics.drawRoundRect(0, 0, w, h, 5);
    indicator.alpha = 0;

    var button_text = new createjs.Text(text, fontSize + " Arial", "#FFF");
    button_text.maxWidth = w;
    button_text.textAlign = "center";
    button_text.textBaseline = "middle";
    button_text.shadow = new createjs.Shadow('#000000', 2, 2, 2);

    button_text.y = h / 2;
    button_text.x = w / 2;

    var buttonContainer = new createjs.Container();
    buttonContainer.mouseChildren = false;
    buttonContainer.cursor = "pointer";
    buttonContainer.addChild(indicator);
    buttonContainer.addChild(button_bg);
    buttonContainer.addChild(button_text);
    buttonContainer.indicator = indicator;
    buttonContainer.textField = button_text;
    buttonContainer.buttonBg = button_bg;

    handleMouse = function(event) {
        //redrawOn = true;
        var button_target_alpha = (event.type == "mouseover") ? 1 : 0.5;
        var rolloverTween = createjs.Tween.get(button_bg, {override: true}).to({alpha: button_target_alpha}, 300);
        rolloverTween.on('change', function(){
            stage.update();
        })
    };

    buttonContainer.on("mouseover", handleMouse);
    buttonContainer.on("mouseout", handleMouse);

    buttonContainer.setBounds(0, 0, w, h);

    return buttonContainer;
}

function getAudioIDsList(arr, prefix) {
    // DUPLICATE SOUND FOUND CANNOT BE LOADED WITH LoadQueue
    // DUE TO BUG WITH preloadjs, NOT TO MENTION INEFFICIENCY
    // https://github.com/CreateJS/PreloadJS/issues/210
    // var audio_manifest = [];
    var soundsURLsWithDuplicates = [];
    var foundIndex = 0;
    var soundsIdsArray = [];

    soundsURLsWithDuplicates = arr;

    var uniqueSoundsURLsArray = soundsURLsWithDuplicates.filter(function (item, pos) {
        return soundsURLsWithDuplicates.indexOf(item) == pos;
    });

    // WE NEED TO CREATE AN ARRAY, soundsIdsArray, THAT LISTS REFERENCES TO EACH
    // OF THE AUDIO FILES LOADED FROM THE UNIQUE SET CREATED ABOVE, HOWEVER,
    // THIS ARRAY NEEDS TO REFLECT THE DUPLICATES AS ORIGINALLY ENTERED
    // FROM THE CMS.

    for (var i = 0; i < soundsURLsWithDuplicates.length; i++) {
        // GRAB EACH VALUE FROM ARRAY OF SOUND URLS THAT
        // MAY CONTAIN DUPLICATES
        var soundURLFromCMS = soundsURLsWithDuplicates[i];

        for (var j = 0; j < uniqueSoundsURLsArray.length; j++) {
            // GRAB VALUE FROM ARRAY OF SOUND URLS
            // WITH THE DUPLICATES REMOVED
            var previouslyGatheredURL = uniqueSoundsURLsArray[j];
            // COMPARE WITH EACH OF THE VALUES FROM THE
            // ARRAY THAT MAY CONTAIN DUPLICATES AND NOTE
            // THE INDEX OF MATCHES
            if (previouslyGatheredURL == soundURLFromCMS) {
                foundIndex = j;
                break;
            }
        }
        // USE THE COLLECTED INDEXES OF MATCHES TO BUILD AN ARRAY
        // TO STORE REFERENCES TO IDS THAT INCLUDE DUPLICATE SOUNDS
        // IF ANY EXISTED. FOR EXAMPLE:
        // ["sound0", "sound0", "sound1", "sound2", "sound2", "sound1"]
        if (prefix) {
            soundsIdsArray.push(prefix + 'sound' + foundIndex);
        } else {
            soundsIdsArray.push('sound' + foundIndex);
        }
    }
    return soundsIdsArray;
}
function getUniqueAudioManifest(arr, prefix) {

    var soundsURLsWithDuplicates = arr;

    var uniqueSoundsURLsArray = soundsURLsWithDuplicates.filter(function (item, pos) {
        return soundsURLsWithDuplicates.indexOf(item) == pos;
    });

    var audio_manifest = [];

    for (var i = 0; i < uniqueSoundsURLsArray.length; i++) {
        if (prefix) {
            audio_manifest.push({id: prefix + "sound" + i, src: uniqueSoundsURLsArray[i]});
        } else {
            audio_manifest.push({id: "sound" + i, src: uniqueSoundsURLsArray[i]});
        }
    }
    return audio_manifest;
}
function shuffle(array) {
    var currentIndex = array.length, temporaryValue, randomIndex;

    // While there remain elements to shuffle...
    while (0 !== currentIndex) {

        // Pick a remaining element...
        randomIndex = Math.floor(Math.random() * currentIndex);
        currentIndex -= 1;

        // And swap it with the current element.
        temporaryValue = array[currentIndex];
        array[currentIndex] = array[randomIndex];
        array[randomIndex] = temporaryValue;
    }

    return array;
}