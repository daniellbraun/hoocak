var statusMessages = {
    complete: "You have completed the crossword puzzle. Good job!",
    partialComplete: "You've started this crossword puzzle but have yet to complete it.",
    unstarted: "This is your first time here! Follow the instructions above to solve the crossword puzzle."
};

updateStatusMessage();
var saveInit = true;
var correctCount = 0;
var crosswordContent = [
    {
        'hochunk': 'hoocąk',
        'english': 'The Ho-Chunk Language',
        'clue': 'The Sacred Language, Master Language, Parent Tongue',
        'location': 0,
        'direction': 'down',
        'found': false
    },
    {
        'hochunk': 'hirawaxiri',
        'english': 'butter',
        'clue': 'Something to spread on toast with a knife',
        'location': 0,
        'direction': 'across',
        'found': false
    },
    {
        'hochunk': 'cąąnį',
        'english': 'fall',
        'clue': 'The trees change color in Wisconsin this time of the year',
        'location': 30,
        'direction': 'across',
        'found': false
    },
    // {
    //     'hochunk': 'wiiwagax',
    //     'english': 'pencil',
    //     'clue': 'You write with this, and can erase it',
    //     'location': 24,
    //     'direction': 'down',
    //     'found': false
    // },
    {
        'hochunk': 'waguje',
        'english': 'shoe',
        'clue': 'You wear it on your foot',
        'location': 24,
        'direction': 'across',
        'found': false
    },
    {
        'hochunk': 'wagax',
        'english': 'paper/book',
        'clue': 'This is something you would write on or into',
        'location': 54,
        'direction': 'down',
        'found': false
    },
    {
        'hochunk': 'hira',
        'english': 'teeth',
        'clue': 'You need these to chew your food with',
        'location': 81,
        'direction': 'across',
        'found': false
    },
    {
        'hochunk': 'nįį',
        'english': 'water',
        'clue': 'This pours out of a faucet',
        'location': 87,
        'direction': 'across',
        'found': false
    },
    {
        'hochunk': 'mąąco',
        'english': 'grizzly bear',
        'clue': 'A very large brown bear',
        'location': 22,
        'direction': 'down',
        'found': false
    },
    {
        'hochunk': 'mąąnį',
        'english': 'winter',
        'clue': 'The season of the year when it snows',
        'location': 48,
        'direction': 'down',
        'found': false
    },
    {
        'hochunk': 'weeną',
        'english': 'spring',
        'clue': 'During this season the snow melts, the flowers bloom and everything begins anew',
        'location': 54,
        'direction': 'across',
        'found': false
    },
    {
        'hochunk': 'nąąnį',
        'english': 'Mother',
        'clue': 'The Hoocąk word for mother',
        'location': 57,
        'direction': 'down',
        'found': false
    },
    {
        'hochunk': 'rezi',
        'english': 'tongue',
        'clue': 'This lives in your mouth and helps you to speak and taste things',
        'location': 45,
        'direction': 'down',
        'found': false
    }
];
var woowagax = [
    'a',
    'aa',
    'ą',
    'ąą',
    'b',
    'c',
    'e',
    'ee',
    'g',
    'ğ',
    'h',
    'i',
    'ii',
    'į',
    'įį',
    'j',
    'k',
    'k\'',
    'm',
    'n',
    'o',
    'oo',
    'p',
    'p\'',
    'r',
    's',
    's\'',
    'š',
    'š\'',
    't',
    't\'',
    'u',
    'uu',
    'ų',
    'ųų',
    'w',
    'x',
    'x\'',
    'y',
    'z',
    'ž',
    '\''
];

var charBoxSize = 40;
var crosswordBoxSize = 45;
var charGridContainer = new createjs.Container();
var crosswordGridContainer = new createjs.Container();

var boxGraphics = new createjs.Graphics();
boxGraphics.beginStroke("rgba(1, 162, 166, 1)");
boxGraphics.beginFill("rgba(1, 162, 166, 0.7)");
boxGraphics.drawRect(0, 0, charBoxSize, charBoxSize);

var crosswordBoxGraphics = new createjs.Graphics();
// crosswordBoxGraphics.beginStroke("rgba(0, 0, 0, 1)");
crosswordBoxGraphics.beginFill("rgba(255, 255, 255, 0.7)");
crosswordBoxGraphics.drawRect(0, 0, crosswordBoxSize, crosswordBoxSize);

var crosswordBoxStroke = new createjs.Graphics();
crosswordBoxStroke.beginStroke("rgba(0, 0, 0, 1)");
crosswordBoxStroke.drawRect(0, 0, crosswordBoxSize, crosswordBoxSize);

var blackBoxGraphics = new createjs.Graphics();
blackBoxGraphics.beginFill("rgba(0, 0, 0, 1)");
blackBoxGraphics.drawRect(0, 0, crosswordBoxSize, crosswordBoxSize);

var popupContainer = new createjs.Container();

var popupBg = new createjs.Graphics();
popupBg.beginStroke("#000000");
popupBg.beginFill("rgba(0,0,0,0.7)");
popupBg.drawRoundRect(0, 0, 300, 170, 10);
var popupBackground = new createjs.Shape(popupBg);
popupBackground.setBounds(0, 0, 300, 170);

var popupField = new createjs.Text("", "bold 24px Arial", "#FFFFFF");
popupField.maxWidth = 280;
popupField.textAlign = "center";
popupField.textBaseline = "top";
popupField.lineHeight = 30;
popupField.x = popupBackground.getBounds().width / 2;
popupField.y = 20;
var yesButton = createButton(70, 30, "Yes", '18px');
var noButton = createButton(70, 30, "No", '18px');
yesButton.y = 125;
noButton.y = 125;
yesButton.x = 20;
noButton.x = 300 - noButton.getBounds().width - 20;
popupContainer.addChild(popupBackground);
popupContainer.addChild(popupField);
popupContainer.addChild(yesButton);
popupContainer.addChild(noButton);
var crosswordBoxToClear = null;
yesButton.on("click", function () {
    stage.removeChild(popupContainer);
    crosswordBoxToClear.char.text = '';
    stage.update();
});
noButton.on("click", function () {
    stage.removeChild(popupContainer);
    stage.update();
});

var hintContainer = new createjs.Container();
var hintField = new createjs.Text("", "18px Arial", "#FFFFFF");
hintField.lineWidth = 180;
hintField.textAlign = "center";
// hintField.textBaseline = "bottom";
hintField.lineHeight = 20;

var hintBg = new createjs.Graphics();
var hintBackground = new createjs.Shape(hintBg);

hintContainer.addChild(hintBackground);
hintContainer.addChild(hintField);

var headerText = "Solve The Crossword Puzzle By Dragging The Hoocąk" +
    " Letters From The Bottom To The Correct Positions To The Left";
var headerTextField = new createjs.Text(headerText, "Bold 22px Arial", "#FFF");
headerTextField.textAlign = "center";
headerTextField.y = 20;
headerTextField.x = 850;
headerTextField.lineWidth = 650;
headerTextField.lineHeight = 28;
headerTextField.shadow = new createjs.Shadow('#000000', 4, 4, 4);
stage.addChild(headerTextField);

var subHeaderText = "(Hover your mouse over the clues for a hint. To erase a letter, click on it and answer \"Yes\".)";
var subHeaderTextField = new createjs.Text(subHeaderText, "Italic 16px Arial", "#BBB");
subHeaderTextField.textAlign = "center";
subHeaderTextField.y = headerTextField.y + headerTextField.getMeasuredHeight() + 20;
subHeaderTextField.x = 850;
subHeaderTextField.lineWidth = 800;
subHeaderTextField.shadow = new createjs.Shadow('#000000', 2, 2, 2);
stage.addChild(subHeaderTextField);

headerTextField.alpha = subHeaderTextField.alpha = 0;

var checkmarkQueue = new createjs.LoadQueue();
checkmarkQueue.loadManifest([
    {
        id: "checkmark", src: image_url + "checkmark.png"
    }
]);

function init() {

    redrawOn = true;

    createjs.Tween.get(headerTextField).wait(2200).to({'alpha': 1}, 500);
    createjs.Tween.get(subHeaderTextField).wait(2500).to({'alpha': 1}, 500);

    var yPos = 0;
    var xPos = 0;

    for (var i = 0; i < woowagax.length; i++) {

        var charContainer = new createjs.Container();
        charContainer.name = "gridLetter_" + i;
        var charBox;

        var char = new createjs.Text(woowagax[i], '25px Arial', '#FFFFFF');
        char.textAlign = "center";
        char.textBaseline = "hanging";


        charBox = new createjs.Shape(boxGraphics);


        charBox.setBounds(0, 0, charBoxSize, charBoxSize);
        charContainer.addChild(charBox);

        charContainer.mouseChildren = false;
        charContainer.cursor = "pointer";

        charContainer.on("mousedown", function (event) {
            this.offsetPt = this.globalToLocal(stage.mouseX, stage.mouseY);
            this.parent.addChild(this); // move to the top of the stack
        });

        charContainer.dragListener = charContainer.on("pressmove", copyAndDragIt, null, false,
            {
                'letter': char.text
            });
        charContainer.on("pressup", stopDrag, null, false,
            {
                'something': ''
            });

        char.x = charBox.x + charBox.getBounds().width / 2;
        char.y = (charBox.getBounds().height / 2 - char.getMeasuredHeight() / 2);
        charContainer.addChild(char);

        charContainer.x = xPos * charBoxSize;
        charContainer.y = yPos;
        xPos++;

        if ((i + 1) % 21 == 0) {
            yPos += charBoxSize;
            xPos = 0;
        }

        charGridContainer.addChild(charContainer);
        //charContainer.alpha = 0;
        //createjs.Tween.get(charContainer).wait(i * 100).to({alpha: 1}, 100).call(woowagaxPlaced);
    }
    stage.addChild(charGridContainer);
    //charGridContainer.y = canvas.height - charGridContainer.getBounds().height - 20;
    //charGridContainer.x = canvas.width / 2 - charGridContainer.getBounds().width / 2;

    charGridContainer.regX = charGridContainer.getBounds().width / 2;
    charGridContainer.regY = charGridContainer.getBounds().height / 2;
    charGridContainer.y = canvas.height - (charGridContainer.getBounds().height / 2) - 20;
    charGridContainer.x = canvas.width / 2;

    charGridContainer.alpha = 0;
    charGridContainer.scaleX = .5;
    charGridContainer.scaleY = .5;
    createjs.Tween.get(charGridContainer).wait(1000).to({
        'alpha': 1,
        'scaleX': 1,
        'scaleY': 1
    }, 500, createjs.Ease.quadOut);
    buildCrosswordGrid();
}
var cloned = null;
function copyAndDragIt(event, data) {

    if (!cloned) {
        cloned = event.currentTarget.clone(true);
        cloned.offsetPt = this.offsetPt;
        cloned.letter = data.letter;
        stage.addChild(cloned);
        cloned.mouseEnabled = false;
    }

    charGridContainer.globalToLocal(event.stageX, event.stageY);

    cloned.x = event.stageX - cloned.offsetPt.x;
    cloned.y = event.stageY - cloned.offsetPt.y;
    stage.update();
    //isColliding = checkIntersection(dragged, dragged.destination);

    // data.bg_shape.graphics.clear();
    // data.bg_shape.graphics.beginFill("rgba(1, 162, 166, 0.3)")
    //     .beginStroke("rgba(1, 162, 166, 1)")
    //     .drawRect(0, 0, charBoxSize, charBoxSize);
}
function stopDrag(event, data) {
    //this.off("pressmove", this.dragListener);
    if (cloned != null) {
        if (cloned.crosswordSquare != null) {
            cloned.crosswordSquare.char.text = cloned.letter;
            checkForCompleteWord();

            // UPDATING SESSION TO SHOW GAME IS IN PROGRESS
            // BUT NOT COMPLETE
            if (saveInit) {
                saveInit = false;
                if (game_status != 'in progress' && game_status != 'complete') {
                    saveSession('in progress');
                }
            }
        }
        stage.removeChild(cloned);
        stage.update();
        cloned = null;
    }
}
function buildCrosswordGrid() {
    var yPos = 0;
    var xPos = 0;
    for (var i = 0; i < (10 * 10); i++) {
        var charContainer = new createjs.Container();
        charContainer.name = "crosswordLetter_" + i;

        var char = new createjs.Text('', '25px Arial', '#000000');
        char.textAlign = "center";
        char.textBaseline = "hanging";

        var charBox = new createjs.Shape(crosswordBoxGraphics);
        charBox.setBounds(0, 0, crosswordBoxSize, crosswordBoxSize);
        charContainer.addChild(charBox);

        var charBoxStroke = new createjs.Shape(crosswordBoxStroke);
        charBoxStroke.setBounds(0, 0, crosswordBoxSize, crosswordBoxSize);
        charContainer.addChild(charBoxStroke);

        charContainer.bg = charBox;
        charContainer.char = char;
        charContainer.occupied = false;
        charContainer.rolloverHandler = charContainer.on("rollover", displayDraggedLetter);
        function displayDraggedLetter() {
            if (cloned != null) {
                if (this.char.text == '') {
                    this.char.text = cloned.letter;
                    cloned.crosswordSquare = this;
                    stage.update();
                } else {
                    this.occupied = true;
                }
            }
        }

        charContainer.rolloutHandler = charContainer.on("rollout", removedDraggedLetter);
        function removedDraggedLetter() {
            if (cloned != null) {
                if (!this.occupied) {
                    this.char.text = '';
                    cloned.crosswordSquare = null;
                }
            }
        }

        charContainer.on('click', function () {
            console.log(this.name);
            if (this.char.text != '') {
                crosswordBoxToClear = this;
                displayPopup("Would you like to\nremove \"" + this.char.text + '\"\nfrom this box?');
            }
        });

        char.x = charBox.x + charBox.getBounds().width / 2;
        char.y = (charBox.getBounds().height / 2 - char.getMeasuredHeight() / 2);
        charContainer.addChild(char);

        charContainer.x = xPos * crosswordBoxSize;
        charContainer.y = yPos;
        xPos++;

        if ((i + 1) % 10 == 0) {
            yPos += crosswordBoxSize;
            xPos = 0;
        }

        crosswordGridContainer.addChild(charContainer);

        var black = new createjs.Shape(blackBoxGraphics);
        black.name = 'black_' + i;
        black.x = charContainer.x;
        black.y = charContainer.y;
        crosswordGridContainer.addChild(black);

        charContainer.mouseEnabled = false;
    }
    stage.addChild(crosswordGridContainer);
    crosswordGridContainer.regX = crosswordGridContainer.getBounds().width / 2;
    crosswordGridContainer.regY = crosswordGridContainer.getBounds().height / 2;
    crosswordGridContainer.y = (crosswordGridContainer.getBounds().height / 2) + 20;
    crosswordGridContainer.x = (crosswordGridContainer.getBounds().width / 2) + 20;

    crosswordGridContainer.alpha = 0;
    crosswordGridContainer.scaleX = .5;
    crosswordGridContainer.scaleY = .5;
    createjs.Tween.get(crosswordGridContainer).wait(1000).to({
        'alpha': 1,
        'scaleX': 1,
        'scaleY': 1
    }, 500, createjs.Ease.quadOut);
    doIHaveALetter();
}
function displayPopup(msg) {
    popupContainer.x = stage.mouseX;
    popupContainer.y = stage.mouseY;
    stage.addChild(popupContainer);
    if (popupContainer.getBounds().width + stage.mouseX > canvas.width) {
        popupContainer.x = stage.mouseX - popupContainer.getBounds().width;
    }
    if (popupContainer.getBounds().height + stage.mouseY > canvas.height) {
        popupContainer.y = stage.mouseY - popupContainer.getBounds().height;
    }
    popupField.text = msg;
    popupContainer.alpha = 0;
    redrawOn = true;
    createjs.Tween.get(popupContainer, {override: true}).to({'alpha': 1}, 300).call(function () {
        redrawOn = false;
    });
}
function checkForCompleteWord() {
    for (var i = 0; i < crosswordContent.length; i++) {
        var direction = crosswordContent[i].direction;
        var location = crosswordContent[i].location;
        var word = crosswordContent[i].hochunk;
        var wordLength = word.length;
        var gotIt = true;
        var checkThisBox = location;
        var foundWordSet = [];
        var color1 = {redOffset: 255, greenOffset: 255, blueOffset: 255, alphaOffset: 255};
        var color2 = {redOffset: 1, greenOffset: 162, blueOffset: 166, alphaOffset: 255};

        for (var j = 0; j < wordLength; j++) {

            var boxChecked = crosswordGridContainer.getChildByName('crosswordLetter_' + checkThisBox);
            //console.log('checkThisBox:', checkThisBox, 'should be:', word[j], 'got:', boxChecked.char.text, 'at', boxChecked.name);
            if (word[j] != boxChecked.char.text) {
                gotIt = false;
                break;
            } else {
                foundWordSet.push(boxChecked.name);
            }
            if (direction == 'down') {
                checkThisBox += 10;
            } else if (direction == 'across') {
                checkThisBox++;
            }
        }

        if (gotIt) {

            if (!crosswordContent[i].found) {
                crosswordContent[i].found = true;
                console.log("that's correct, you've found:", word);

                var checkmark = crosswordContent[i].checkmark;
                var checkmarkTween = createjs.Tween.get(checkmark).to({'alpha': 0.5}, 500);
                checkmarkTween.on("change", function () {
                    stage.update();
                });

                correctCount++;

                if (correctCount == crosswordContent.length) {

                    if (game_status != 'complete') {
                        saveSession('complete');
                        game_status = 'complete';
                    }

                    setTimeout(function () {
                        dialogbox("Hurray!!!\nYou've completed the crossword puzzle. Not bad!");
                    }, 1000);
                }

                for (j = 0; j < foundWordSet.length; j++) {

                    var foundWordLetterBox = crosswordGridContainer.getChildByName(foundWordSet[j]);
                    foundWordLetterBox.mouseEnabled = false;
                    foundWordLetterBox.char.color = "#FFFFFF";
                    var filter = new createjs.ColorFilter(0, 0, 0, 0.7, 255, 255, 255);
                    foundWordLetterBox.bg.filters = [filter];
                    foundWordLetterBox.bg.cache(0, 0, crosswordBoxSize, crosswordBoxSize);

                    var tween = createjs.Tween.get(filter)
                        .to(color1, 200)
                        .to(color2, 200)
                        .to(color1, 200)
                        .to(color2, 200);
                    tween.bg = foundWordLetterBox.bg;
                    tween.on("change", function () {
                        stage.update();
                        this.bg.updateCache();
                    });
                }
            }
        }
    }
}

function doIHaveALetter() {
    for (var i = 0; i < crosswordContent.length; i++) {
        var direction = crosswordContent[i].direction;
        var location = crosswordContent[i].location;
        var word = crosswordContent[i].hochunk;
        var wordLength = word.length;
        var checkThisBox = location;

        for (var j = 0; j < wordLength; j++) {
            var boxChecked = crosswordGridContainer.getChildByName('crosswordLetter_' + checkThisBox);

            // DISABLED EVENT HANDLERS AND BLACK NON-LETTER BOXES ARE ADDED TO EVERY BOX IN THE GRID BY DEFAULT
            // HERE WE CHECK FOR THE LOCATION OF WORD LETTERS AND REMOVE THE BLACK BOX AND RE ENABLE THE EVENT HANDLERS
            boxChecked.mouseEnabled = true;
            crosswordGridContainer.removeChild(crosswordGridContainer.getChildByName('black_' + checkThisBox));

            if (direction == 'down') {
                checkThisBox += 10;
            } else if (direction == 'across') {
                checkThisBox++;
            }
        }
    }
    placeClueNumbers();
}
function placeClueNumbers() {

    var allLocations = [];

    for (var i = 0; i < crosswordContent.length; i++) {
        allLocations.push(crosswordContent[i].location);
    }
    var uniqueLocations = allLocations.filter(getUniqueItems);

    function getUniqueItems(item, pos, arr) {
        return arr.indexOf(item) == pos;
    }

    for (i = 0; i < crosswordContent.length; i++) {
        var clueNumber = uniqueLocations.indexOf(crosswordContent[i].location) + 1;
        crosswordContent[i].clueNumber = clueNumber;
        var clueNumberText = new createjs.Text(clueNumber, '12px Arial', '#000000');
        clueNumberText.x = 2;
        clueNumberText.y = 2;
        var crosswordBox = crosswordGridContainer.getChildByName('crosswordLetter_' + crosswordContent[i].location);
        crosswordBox.addChild(clueNumberText);

    }
    listClues();
}
function listClues() {
    var clueTextArr = [];
    var leftColPos = 520;
    var rightColPos = 860;
    var colWidth = 300;
    var across = [];
    var down = [];
    var lHeight = 20;
    var colFont = "18px Arial";
    var clueSpacing = 15;
    for (var i = 0; i < crosswordContent.length; i++) {
        if (crosswordContent[i].direction == 'down') {
            down.push(crosswordContent[i]);
        } else if (crosswordContent[i].direction == 'across') {
            across.push(crosswordContent[i]);
        }
    }
    var acrossHeader = new createjs.Text("Across", "Bold 17px Arial", "#FFF");
    var downHeader = new createjs.Text("Down", "Bold 17px Arial", "#FFF");

    clueTextArr.push(acrossHeader);

    acrossHeader.shadow = new createjs.Shadow('#000000', 2, 2, 2);
    downHeader.shadow = new createjs.Shadow('#000000', 2, 2, 2);
    acrossHeader.x = leftColPos;
    downHeader.x = rightColPos;
    acrossHeader.y = downHeader.y = 140;
    stage.addChild(acrossHeader, downHeader);
    var prevYPosAcross = acrossHeader.y + acrossHeader.getMeasuredHeight() + clueSpacing;
    for (i = 0; i < across.length; i++) {
        var acrossColText = new createjs.Text(across[i].clueNumber + ". " + across[i].clue, colFont, "#FFF");

        clueTextArr.push(acrossColText);

        acrossColText.shadow = new createjs.Shadow('#000000', 2, 2, 2);
        acrossColText.lineWidth = colWidth;
        acrossColText.lineHeight = lHeight;
        acrossColText.x = acrossHeader.x;
        acrossColText.y = prevYPosAcross;
        prevYPosAcross += acrossColText.getMeasuredHeight() + clueSpacing;
        var hitAreaAcross = new createjs.Shape();
        hitAreaAcross.graphics.beginFill("#000");
        hitAreaAcross.graphics.drawRect(0, 0, colWidth, acrossColText.getMeasuredHeight());
        acrossColText.hitArea = hitAreaAcross;
        acrossColText.englishHint = across[i].english;
        acrossColText.on("mouseover", function () {
            this.color = "rgba(1, 162, 166, 1)";
            stage.update();
            displayHint("Hint: English word is " + this.englishHint);
        });
        acrossColText.on("mouseout", function () {
            this.color = "white";
            stage.removeChild(hintContainer);
            stage.update();
        });
        stage.addChild(returnCheckmark(acrossColText.x, acrossColText.y, across[i].hochunk), acrossColText);
    }
    var prevYPosDown = downHeader.y + downHeader.getMeasuredHeight() + clueSpacing;
    clueTextArr.push(downHeader);
    for (i = 0; i < down.length; i++) {
        var downColText = new createjs.Text(down[i].clueNumber + ". " + down[i].clue, colFont, "#FFF");

        clueTextArr.push(downColText);

        downColText.shadow = new createjs.Shadow('#000000', 2, 2, 2);
        downColText.lineWidth = colWidth;
        downColText.lineHeight = lHeight;
        downColText.x = downHeader.x;
        downColText.y = prevYPosDown;
        prevYPosDown += downColText.getMeasuredHeight() + clueSpacing;
        var hitAreaDown = new createjs.Shape();
        hitAreaDown.graphics.beginFill("#000");
        hitAreaDown.graphics.drawRect(0, 0, colWidth, downColText.getMeasuredHeight());
        downColText.hitArea = hitAreaDown;
        downColText.englishHint = down[i].english;
        downColText.on("mouseover", function () {
            this.color = "rgba(1, 162, 166, 1)";
            stage.update();
            displayHint("Hint: English word is " + this.englishHint);
        });
        downColText.on("mouseout", function () {
            this.color = "white";
            stage.removeChild(hintContainer);
            stage.update();
        });
        stage.addChild(returnCheckmark(downColText.x, downColText.y, down[i].hochunk), downColText);
    }
    for (i = 0; i < clueTextArr.length; i++) {
        clueTextArr[i].alpha = 0;
        clueTextArr[i].x -= 50;
    }
    var clueCount = clueTextArr.length;
    setTimeout(function () {
        for (i = 0; i < clueTextArr.length; i++) {
            createjs.Tween.get(clueTextArr[i]).wait(i * 150).to({
                'alpha': 1,
                'x': clueTextArr[i].x + 50
            }, 300, createjs.Ease.quadOut).call(clueMoved);
        }
    }, 1000);
    function clueMoved() {
        clueCount--;
        if (clueCount == 0) {

            redrawOn = false;
        }
    }
}
function returnCheckmark(x, y, hochunk) {
    var checkmark = new createjs.Bitmap(checkmarkQueue.getResult("checkmark"));
    checkmark.alpha = 0;
    checkmark.regX = checkmark.image.width / 2;
    checkmark.regY = checkmark.image.height / 2;
    checkmark.x = x - 5;
    checkmark.y = y + 10;
    for (var i = 0; i < crosswordContent.length; i++) {
        if (crosswordContent[i].hochunk == hochunk) {
            crosswordContent[i].checkmark = checkmark;
            break;
        }
    }
    return checkmark;
}
function displayHint(msg) {

    hintField.text = msg;
    hintBg.clear();
    hintBg.beginStroke("#000000");
    hintBg.beginFill("rgba(0,0,0,1)");
    hintBg.drawRoundRect(0, 0, hintField.lineWidth + 10, hintField.getMeasuredHeight() + 10, 10);

    hintBackground.setBounds(0, 0, hintField.lineWidth + 10, hintField.getMeasuredHeight() + 10);

    hintField.x = hintBackground.getBounds().width / 2;
    hintField.y = hintBackground.getBounds().height / 2 - hintField.getMeasuredHeight() / 2;

    hintContainer.x = stage.mouseX;
    hintContainer.y = stage.mouseY;
    stage.addChild(hintContainer);
    if (hintContainer.getBounds().width + stage.mouseX > canvas.width) {
        hintContainer.x = stage.mouseX - hintContainer.getBounds().width;
    }
    if (hintContainer.getBounds().height + stage.mouseY > canvas.height) {
        hintContainer.y = stage.mouseY - hintContainer.getBounds().height;
    }
    // hintField.text = msg;
    hintContainer.alpha = 0;
    redrawOn = true;
    createjs.Tween.get(hintContainer, {override: true}).to({'alpha': 1}, 300).call(function () {
        redrawOn = false;
    });
}
checkmarkQueue.on("complete", function () {
    console.log('loaded');
    setTimeout(function () {
        init();
    }, 2000);
});