var statusMessages = {
    complete: "You have completed this game by finding and clicking on each of the 39 parts of the drawing above to " +
    "see and hear the Hoocąk word for that item.",
    partialComplete: "You've started learning Hoocąk words for the each 39 parts of the drawing above but have not yet " +
    "found them all.",
    unstarted: "This is your first time here! In the drawing above, there are 39 click-able regions. Click on each one of " +
    "them to see and hear the Hoocąk word for what or who you clicked on."
};

var imagesWords = [
    {
        'img': 'family_layers2__wall.png',
        'hochunk': 'cii ruhi',
        'english': 'wall',
        'audio': '10_ciiruhi.mp3',
        'clicked': false
    },
    {
        'img': 'Family_1200__0000_dog.png',
        'hochunk': 'šųųk',
        'english': 'dog',
        'audio': '20_shuuk.mp3',
        'clicked': false
    },
    {
        'img': 'Family_1200__0001_clock.png',
        'hochunk': 'wiigųs',
        'english': 'clock',
        'audio': '5_wiigus.mp3',
        'clicked': false
    },
    {
        'img': 'Family_1200__0002_cat.png',
        'hochunk': 'wijuk',
        'english': 'cat',
        'audio': '19_wijuk.mp3',
        'clicked': false
    },
    {
        'img': 'Family_1200__0003_puppy.png',
        'hochunk': 'šųųknįk',
        'english': 'puppy',
        'audio': '21_shuknik.mp3',
        'clicked': false
    },
    {
        'img': 'Family_1200__0005_lamp.png',
        'hochunk': 'hataža',
        'english': 'lamp',
        'audio': '3_hatazha.mp3',
        'clicked': false
    },
    {
        'img': 'Family_1200__0008_door.png',
        'hochunk': 'ciirop',
        'english': 'door',
        'audio': '7_ciirop.mp3',
        'clicked': false
    },
    {
        'img': 'Family_1200__0009_window.png',
        'hochunk': 'hirohąp',
        'english': 'window',
        'audio': '1_hirohap.mp3',
        'clicked': false
    },
    {
        'img': 'Family_1200__0010_table.png',
        'hochunk': 'waaruc',
        'english': 'table',
        'audio': '11_waaruc.mp3',
        'clicked': false
    },
    {
        'img': 'Family_1200__0011_chair.png',
        'hochunk': 'waamįnąk',
        'english': 'chair',
        'audio': '9_waaminak.mp3',
        'clicked': false
    },
    {
        'img': 'Family_1200__0012_flowers1.png',
        'hochunk': 'xąąwį hoxere',
        'english': 'flowers',
        'audio': '2_xawioxere.mp3',
        'clicked': false
    },
    {
        'img': 'Family_1200__0013_flowers2.png',
        'hochunk': 'xąąwį hoxere',
        'english': 'flowers',
        'audio': '2_xawioxere.mp3',
        'clicked': false
    },
    {
        'img': 'Family_1200__0014_couch.png',
        'hochunk': 'waamįnąkserec',
        'english': 'couch',
        'audio': '18_waminakserec.mp3',
        'clicked': false
    },
    {
        'img': 'Family_1200__0015_buck.png',
        'hochunk': 'caaxete',
        'english': 'buck',
        'audio': '30_caaxete.mp3',
        'clicked': false
    },
    {
        'img': 'Family_1200__0016_siblings.png',
        'hochunk': 'wąąk kinųp',
        'english': 'brothers',
        'audio': '28_waak_kinup.mp3',
        'clicked': false
    },
    {
        'img': 'Family_1200__0017_child.png',
        'hochunk': '​hocįcįnįk',
        'english': 'little boy',
        'audio': '16_hocicinik.mp3',
        'clicked': false
    },
    {
        'img': 'Family_1200__0018_old-man.png',
        'hochunk': 'Cooka',
        'english': 'grandfather',
        'audio': '22_cooka.mp3',
        'clicked': false
    },
    {
        'img': 'Family_1200__0020_boy.png',
        'hochunk': 'hoocįcį, hoocįcįnįk',
        'english': 'boy',
        'audio': '15_hocici.mp3',
        'clicked': false
    },
    {
        'img': 'Family_1200__0021_woman.png',
        'hochunk': 'Nąąnį',
        'english': 'Mother',
        'audio': '24_naani.mp3',
        'clicked': false
    },
    {
        'img': 'Family_1200__0022_man.png',
        'hochunk': 'Jaaji',
        'english': 'father',
        'audio': '25_jaaji.mp3',
        'clicked': false
    },
    {
        'img': 'Family_1200__0023_girl.png',
        'hochunk': 'hinųknįk',
        'english': 'little girl',
        'audio': '14_hinuknik.mp3',
        'clicked': false
    },
    {
        'img': 'family_layers2__0001_TV.png',
        'hochunk': 'hokiwagax hixgąxgą kook',
        'english': 'television',
        'audio': '12_hikiwagax_hixgaxga_kook.mp3',
        'clicked': false
    },
    {
        'img': 'family_layers2__0002_woman,-girl.png',
        'hochunk': 'hinųk',
        'english': 'woman, girl',
        'audio': '13_hinuk.mp3',
        'clicked': false
    },
    {
        'img': 'family_layers2__0003_chair.png',
        'hochunk': 'waamįnąk',
        'english': 'chair',
        'audio': '9_waaminak.mp3',
        'clicked': false
    },
    {
        'img': 'family_layers2__0004_chair-blue.png',
        'hochunk': 'waamįnąk',
        'english': 'chair',
        'audio': '9_waaminak.mp3',
        'clicked': false
    },
    {
        'img': 'family_layers2__0005_man-with-gun.png',
        'hochunk': 'wąak',
        'english': 'man',
        'audio': '17_waak.mp3',
        'clicked': false
    },
    {
        'img': 'family_layers2__0006_picture-big.png',
        'hochunk': 'hokiwagax',
        'english': 'picture',
        'audio': '4_hokiwagax.mp3',
        'clicked': false
    },
    {
        'img': 'family_layers2__0007_grandma.png',
        'hochunk': 'Gaaga/Kaaka',
        'english': 'Grandma',
        'audio': '23_gaaga.mp3',
        'clicked': false
    },
    {
        'img': 'family_layers2__0008_doll.png',
        'hochunk': '​nįkjąk hiʹų',
        'english': 'doll',
        'audio': '26_nikjak_hi_39_u.mp3',
        'clicked': false
    },
    {
        'img': 'family_layers2__0009_closet.png',
        'hochunk': '​wąįnį horucgus',
        'english': 'closet',
        'audio': '6_waini_horucgus.mp3',
        'clicked': false
    },
    {
        'img': 'family_layers2__0010_rug.png',
        'hochunk': 'cii woomįš',
        'english': 'rug',
        'audio': '8_cii_womish.mp3',
        'clicked': false
    },
    {
        'img': 'family_layers2__0011_chair-3.png',
        'hochunk': 'waamįnąk',
        'english': 'chair',
        'audio': '9_waaminak.mp3',
        'clicked': false
    },
    {
        'img': 'family_layers2__0012_teacher.png',
        'hochunk': 'wagigus',
        'english': 'teacher',
        'audio': '29_wagigus.mp3',
        'clicked': false
    },
    {
        'img': 'family_layers2__0013_table-coffee-table.png',
        'hochunk': 'waaruc',
        'english': 'table',
        'audio': '11_waaruc.mp3',
        'clicked': false
    },
    {
        'img': 'family_layers2__0014_toy1.png',
        'hochunk': 'wišgac',
        'english': 'toy',
        'audio': '27_wishgac.mp3',
        'clicked': false
    },
    {
        'img': 'family_layers2__0015_toy2.png',
        'hochunk': 'wišgac',
        'english': 'toy',
        'audio': '27_wishgac.mp3',
        'clicked': false
    },
    {
        'img': 'family_layers2__0016_window2.png',
        'hochunk': 'hirohąp',
        'english': 'window',
        'audio': '1_hirohap.mp3',
        'clicked': false
    },
    {
        'img': 'family_layers2__0018_picture.png',
        'hochunk': 'hokiwagax',
        'english': 'picture',
        'audio': '4_hokiwagax.mp3',
        'clicked': false
    }
];

var backgroundImage = 'Family-In-The-Home-Artwork-1200w-bg.png';

var messageField;
var popupContainer;
var popupField;

var soundsIdsArray;

var saveInit = true;

function init() {

    redrawOn = false;

    $('#moreGames').remove();

    messageField = new createjs.Text("", "bold 24px Arial", "#FFFFFF");
    messageField.maxWidth = 1000;
    messageField.textAlign = "center";
    messageField.textBaseline = "middle";
    messageField.x = canvas.width / 2;
    messageField.y = 50;
    stage.addChild(messageField);

    popupContainer = new createjs.Container();

    var g = new createjs.Graphics();
    g.beginStroke("#000000");
    g.beginFill("rgba(0,0,0,0.7)");
    g.drawRoundRect(0, 0, 300, 170, 10);
    var popupBackground = new createjs.Shape(g);
    popupBackground.setBounds(0, 0, 300, 170);

    popupField = new createjs.Text("Hochunk Word\n\nEnglish Word", "bold 24px Arial", "#FFFFFF");
    popupField.maxWidth = 280;
    popupField.textAlign = "center";
    popupField.textBaseline = "top";
    popupField.x = popupBackground.getBounds().width / 2;
    popupField.y = popupBackground.getBounds().height / 2 - popupField.getMeasuredHeight() / 2;

    popupContainer.addChild(popupBackground);
    popupContainer.addChild(popupField);

    var image_manifest = [];
    image_manifest.push({id: 'background', src: image_url + backgroundImage});
    for (var i = 0; i < imagesWords.length; i++) {
        image_manifest.push({id: 'image_' + i, src: image_url + imagesWords[i].img});
    }

    var soundsURLsWithDuplicates = [];

    for (i = 0; i < imagesWords.length; i++) {
        soundsURLsWithDuplicates.push(audio_url + imagesWords[i].audio);
    }
    soundsIdsArray = getAudioIDsList(soundsURLsWithDuplicates);

    var audio_manifest = getUniqueAudioManifest(soundsURLsWithDuplicates);

    var preloaded_assets = new createjs.LoadQueue();
    preloaded_assets.on("complete", handlePreloadComplete, this);
    preloaded_assets.on("error", handleError, this);
    preloaded_assets.on("progress", progressHandler, this);

    createjs.Sound.registerPlugins([createjs.HTMLAudioPlugin]);  // need this so it doesn't default to Web Audio

    preloaded_assets.installPlugin(createjs.Sound);
    preloaded_assets.loadManifest(image_manifest);
    preloaded_assets.loadManifest(audio_manifest);

    function progressHandler(event) {
        var loadProgress = Math.round(100 * event.progress);
        messageField.text = "Loading: " + loadProgress + "%";
        messageField.x = canvas.width / 2;
        stage.update();
    }

    function handlePreloadComplete(event) {
        redrawOn = true;
        var bg = new createjs.Bitmap(preloaded_assets.getResult('background'));
        stage.addChild(bg);
        bg.alpha = 0;

        createjs.Tween.get(bg).to({alpha: 1}, 500);
        messageField.text = "";

        for (var i = 0; i < imagesWords.length; i++) {
            var imageForWord = new createjs.Bitmap(preloaded_assets.getResult('image_' + i));
            imageForWord.alpha = .01;
            stage.addChild(imageForWord);

            //imageForWord.cache(0, 0, canvas.width, canvas.height);

            imageForWord.cursor = 'pointer';

            imageForWord.on('mouseover', handleOverOut, null, false);
            imageForWord.on('mouseout', handleOverOut, null, false);
            imageForWord.on('click', imageForWord_handleClick, null, false,
                {
                    'hochunk_word': imagesWords[i].hochunk,
                    'english_word': imagesWords[i].english,
                    'audio_word': soundsIdsArray[i],
                    'index': i
                }
            );
        }
        updateStatusMessage();
        dialogWelcome();

        var moreGamesButton = createButton(150, 70, "More Games", "22px");
        moreGamesButton.x = canvas.width - moreGamesButton.getBounds().width - 20;
        moreGamesButton.y = canvas.height - moreGamesButton.getBounds().height - 20;
        stage.addChildAt(moreGamesButton, stage.children.length - 1);
        moreGamesButton.on('click', function () {
            window.location = location.origin + '/learning-materials/?tab=games';
        })
    }

    function dialogWelcome() {
        dialogbox('Welcome to our home!\n\nClick on different parts of this picture to learn the Hoocąk word for what or who you clicked on.');
        redrawOn = false;
    }

    function handleError(event) {
        console.log("ERROR:", event.title);
    }
}

function handleOverOut(event) {
    var button_target_alpha = (event.type == "mouseover") ? 1 : .01;
    event.currentTarget.alpha = button_target_alpha;
    stage.update();
    // var overTween = createjs.Tween.get(event.currentTarget, {override: true}).to({alpha: button_target_alpha}, 200);
    //
    // overTween.on("change", function(){
    //     //event.currentTarget.updateCache();
    //     stage.update();
    // });
}

function imageForWord_handleClick(event, data) {

    // UPDATING SESSION TO SHOW GAME IS IN PROGRESS
    // BUT NOT COMPLETE
    if (saveInit) {
        saveInit = false;
        if (game_status != 'in progress' && game_status != 'complete') {
            saveSession('in progress');
        }
    }

    if (!imagesWords[data.index].clicked) imagesWords[data.index].clicked = true;

    popupContainer.x = stage.mouseX;
    popupContainer.y = stage.mouseY;
    stage.addChild(popupContainer);
    if (popupContainer.getBounds().width + stage.mouseX > canvas.width) {
        popupContainer.x = stage.mouseX - popupContainer.getBounds().width;
    }
    if (popupContainer.getBounds().height + stage.mouseY > canvas.height) {
        popupContainer.y = stage.mouseY - popupContainer.getBounds().height;
    }
    popupField.text = data.hochunk_word + '\n\n' + '(' + data.english_word + ')';
    popupContainer.alpha = 0;

    var popupTween = createjs.Tween.get(popupContainer, {override: true})
        .to({'alpha': 1}, 300)
        .wait(3000)
        .to({'alpha': 0}, 300);
    popupTween.on('change', function () {
        stage.update();
    });
    createjs.Sound.play(data.audio_word,
        {
            interrupt: createjs.Sound.INTERRUPT_NONE,
            loop: 0,
            volume: 1
        }
    );

    for (var i = 0; i < imagesWords.length; i++) {
        if (!imagesWords[i].clicked) {
            return;
        }
    }

    if (game_status != 'complete') {
        game_status = 'complete';
        stage.mouseEnabled = false;
        stage.mouseChildren = false;
        setTimeout(function () {
            dialogbox("Congratulations! You've found everything!\nFeel free to continue learning or choose a different game below.");
            stage.mouseChildren = true;
            stage.mouseEnabled = true;
            saveSession('complete');
        }, 2000);
    }
}

setTimeout(function () {
    init();
}, 1000);
