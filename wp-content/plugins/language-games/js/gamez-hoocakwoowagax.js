var statusMessages = {
    complete: "You have completed the missing Hoocąk letters puzzle at least one time. Good job!",
    partialComplete: "You've started the missing Hoocąk letters puzzle but have yet to complete the puzzle completely.",
    unstarted: "This is your first time here! Above is the Hoocąk alphabet, though it is missing several letters. Drag " +
        "the missing letters on the right to their correct positions on the left."
};

updateStatusMessage();
redrawOn = false;
var woowagax = [
    'a',
    'aa',
    'ą',
    'ąą',
    'b',
    'c',
    'e',
    'ee',
    'g',
    'ğ',
    'h',
    'i',
    'ii',
    'į',
    'įį',
    'j',
    'k',
    'k\'',
    'm',
    'n',
    'o',
    'oo',
    'p',
    'p\'',
    'r',
    's',
    's\'',
    'š',
    'š\'',
    't',
    't\'',
    'u',
    'uu',
    'ų',
    'ųų',
    'w',
    'x',
    'x\'',
    'y',
    'z',
    'ž',
    '\''
];
var woowagax_audio = [
    '1 a.mp3',
    '2 aa.mp3',
    '3 a.mp3',
    '4 aa.mp3',
    '5 b.mp3',
    '6 c.mp3',
    '7 e.mp3',
    '8 ee.mp3',
    '9 g.mp3',
    '10 g.mp3',
    '11 h.mp3',
    '12 i.mp3',
    '13 ii.mp3',
    '14 i.mp3',
    '15 ii.mp3',
    '16 j.mp3',
    '17 k.mp3',
    '18 k.mp3',
    '19 m.mp3',
    '20 n.mp3',
    '21 o.mp3',
    '22 oo.mp3',
    '23 p.mp3',
    '24 p.mp3',
    '25 r.mp3',
    '26 s.mp3',
    '27 s.mp3',
    '28 s.mp3',
    '29 s.mp3',
    '30 t.mp3',
    '31 t.mp3',
    '32 u.mp3',
    '33 uu.mp3',
    '34 u.mp3',
    '35 uu.mp3',
    '36 w.mp3',
    '37 x.mp3',
    '38 x.mp3',
    '39 y.mp3',
    '40 z.mp3',
    '41 z.mp3',
    '42 \'.mp3'
];
var headerText = new createjs.Text("", "bold 30px Arial", "white");
headerText.text = "Place the missing Hoocąk letters\nby dragging them to the left";
headerText.textAlign = "center";
headerText.x = canvas.width / 1.33333333333;
headerText.y = 40;
headerText.alpha = 0;
stage.addChild(headerText);

var winTextHeader = new createjs.Text("You won!!!", "bold 45px Arial", "white");
winTextHeader.name = 'win_text_header';
winTextHeader.textAlign = "center";
winTextHeader.x = canvas.width / 1.33333333333;
winTextHeader.y = canvas.height / 3;
var winTextMessage = new createjs.Text("\nClick reset button below to try another puzzle.", "bold 20px Arial", "white");
winTextMessage.name = 'win_text_message';
winTextMessage.textAlign = "center";
winTextMessage.x = canvas.width / 1.33333333333;
winTextMessage.y = winTextHeader.y + (winTextMessage.getMeasuredHeight() / 2) + 20;
stage.addChild(winTextHeader);
stage.addChild(winTextMessage);
winTextHeader.alpha = 0;
winTextMessage.alpha = 0;

var resetButton = createButton(380, 60, "Reset Puzzle And Start Over");
resetButton.on("click", explodeWoowagax);

var charBoxSize = 80;
var charGridContainer = new createjs.Container();
var randomMissingSequence;
var charPileContainer = new createjs.Container();
var isColliding = false;

var boxGraphics = new createjs.Graphics();
boxGraphics.beginStroke("rgba(1, 162, 166, 1)");
boxGraphics.beginFill("rgba(1, 162, 166, 0.7)");
boxGraphics.drawRect(0, 0, charBoxSize, charBoxSize);

var boxGraphicsEmpty = new createjs.Graphics();
boxGraphicsEmpty.beginStroke("rgba(1, 162, 166, 1)");
boxGraphicsEmpty.beginFill("rgba(0, 0, 0, 0.7)");
boxGraphicsEmpty.drawRect(0, 0, charBoxSize, charBoxSize);

var boxAvailableGraphics = new createjs.Graphics();
boxAvailableGraphics.beginFill("rgba(1, 162, 166, 0.7)");
boxAvailableGraphics.beginStroke("rgba(1, 162, 166, 1)");
boxAvailableGraphics.drawRect(0, 0, charBoxSize, charBoxSize);

var missingLetterPlacementCount = 0;
var woowagaxLetterPlaced = 0;

var correctCount = 0;

var saveInit = true;
var preloaded_audio = new createjs.LoadQueue();

var loadingText = new createjs.Text('', "bold 45px Arial", "white");
loadingText.textAlign = 'center';

function init() {
    var audio_manifest = [];
    for (var i = 0; i < woowagax_audio.length; i++) {
        audio_manifest.push(
            {
                id: 'alpha_' + i,
                src: audio_url + 'alphabet/' + woowagax_audio[i]
            });
    }
    preloaded_audio.installPlugin(createjs.Sound);
    preloaded_audio.on("complete", build, this);
    preloaded_audio.on("progress", handleFileLoad, this);
    preloaded_audio.loadManifest(audio_manifest);

    stage.addChild(loadingText);
}
function handleFileLoad(event) {
    var loadProgress = Math.round(100 * event.progress);
    loadingText.text = 'Loading: ' + loadProgress + '%';
    loadingText.x = canvas.width / 2;
    loadingText.y = canvas.height / 2;
    if (loadProgress == 100) {
        stage.removeChild(loadingText);
    }
    stage.update();
}
function build() {

    var headerTween = createjs.Tween.get(headerText).to({alpha : 1}, 500);
    headerTween.on("change", function(){
        stage.update();
    });

    var yPos = 0;
    var xPos = 0;

    var incrementMissing = 0;
    randomMissingSequence = returnRandomSet(woowagax, 10);

    for (var i = 0; i < woowagax.length; i++) {

        var charContainer = new createjs.Container();
        charContainer.name = "gridLetter_" + i;
        var charBox;

        var char = new createjs.Text(woowagax[i], '50px Arial', '#FFFFFF');
        char.textAlign = "center";
        char.textBaseline = "hanging";

        // randomMissingSequence has to be in order... meh.
        if (i == randomMissingSequence[incrementMissing]) {
            charBox = new createjs.Shape();
            charBox.graphics = boxGraphicsEmpty;
            char.alpha = 0;
            incrementMissing++;

            charBox.name = 'ima_unique_bg_' + i;
            charContainer.bgShp = charBox;
        } else {
            charBox = new createjs.Shape(boxGraphics);
            charContainer.audio_id = 'alpha_' + i;
            charContainer.on("mousedown", function() {
                createjs.Sound.play(this.audio_id,
                    {
                        interrupt: createjs.Sound.INTERRUPT_NONE,
                        loop: 0,
                        volume: 1
                    }
                );
            })
        }

        charBox.setBounds(0, 0, charBoxSize, charBoxSize);
        charContainer.addChild(charBox);

        char.x = charBox.x + charBox.getBounds().width / 2;
        char.y = (charBox.getBounds().height / 2 - char.getMeasuredHeight() / 2);
        charContainer.addChild(char);

        charContainer.x = xPos * charBoxSize;
        charContainer.y = yPos;
        xPos++;

        if ((i + 1) % 6 == 0) {
            yPos += charBoxSize;
            xPos = 0;
        }

        charGridContainer.addChild(charContainer);
        charContainer.alpha = 0;
        var charContTween = createjs.Tween.get(charContainer).wait(i * 100).to({alpha: 1}, 100).call(woowagaxPlaced);
        charContTween.on("change", function(){
            stage.update();
        });
    }
    stage.addChild(charGridContainer);
    charGridContainer.y = canvas.height / 2 - charGridContainer.getBounds().height / 2;
    charGridContainer.x = 20;

}

function placeMissing() {

    var xPos = 0;
    var yPos = 0;

    var charContainers = [];
    var charContainerLocs = [];

    for (var i = 0; i < randomMissingSequence.length; i++) {

        var charContainer = new createjs.Container();
        charContainer.name = "missingLetter_" + i;

        var char = new createjs.Text(woowagax[randomMissingSequence[i]], '50px Arial', '#FFFFFF');
        char.textAlign = "center";
        char.textBaseline = "hanging";

        var charBox = new createjs.Shape(boxGraphics);
        charBox.setBounds(0, 0, charBoxSize, charBoxSize);

        charContainer.addChild(charBox);

        var bgGraphics = new createjs.Graphics();
        bgGraphics.beginFill("rgba(1, 162, 166, 1)");
        bgGraphics.drawRect(0, 0, charBoxSize, charBoxSize);

        var buttonBg = new createjs.Shape(bgGraphics);
        buttonBg.alpha = 0;
        charContainer.addChild(buttonBg);

        char.x = charBox.x + charBox.getBounds().width / 2;
        char.y = (charBox.getBounds().height / 2 - char.getMeasuredHeight() / 2);
        charContainer.addChild(char);

        // COLLECT A "MISSING LETTER LOCATIONS" SET OF OBJECTS TO BE APPLIED TO
        // SHUFFLED ARRAY OF CHARACTER CONTAINERS (charContainers)
        charContainerLocs.push({'x': xPos * charBoxSize, 'y': yPos});
        xPos++;
        if ((i + 1) % 4 == 0) {
            yPos += charBoxSize;
            xPos = 0;
        }

        charContainer.mouseChildren = false;
        charContainer.cursor = "pointer";

        charContainer.mouseOver_evt = charContainer.on("mouseover", handleMouse);
        charContainer.mouseOut_evt = charContainer.on("mouseout", handleMouse);
        charContainer.bg = buttonBg;

        charContainer.audio_id = 'alpha_' + randomMissingSequence[i];

        function handleMouse(event) {
            var button_target_alpha = (event.type == "mouseover") ? 1 : 0;
            var overTween = createjs.Tween.get(this.bg, {override: true}).to({alpha: button_target_alpha}, 300);
            overTween.on("change", function(){
                stage.update();
            });
        }

        charContainer.on("mousedown", function (event) {
            this.offsetPt = this.globalToLocal(stage.mouseX, stage.mouseY);
            this.parent.addChild(this); // move to the top of the stack
            createjs.Sound.play(this.audio_id,
                {
                    interrupt: createjs.Sound.INTERRUPT_NONE,
                    loop: 0,
                    volume: 1
                }
            );
        });

        charContainer.destination = charGridContainer.children[randomMissingSequence[i]];
        // console.log('charContainer.destination.bgShp:', charContainer.destination.bgShp);

        charContainer.dragListener = charContainer.on("pressmove", dragIt, null, false,
            {
                'bg_shape': charContainer.destination.bgShp
            });
        charContainer.pressupListener = charContainer.on("pressup", stopDrag, null, false,
            {
                'bg_shape': charContainer.destination.bgShp
            });
        charContainer.destinationX = charGridContainer.children[randomMissingSequence[i]].x;
        charContainer.destinationY = charGridContainer.children[randomMissingSequence[i]].y;

        charContainers.push(charContainer);
        charContainer.mouseEnabled = false;
    }

    // SHUFFLE THE ARRAY OF CONTAINERS CREATED ABOVE AND THEN
    // APPLY THE SET OF LOCATIONS CREATED EARLIER SO THAT THEY
    // FADE IN FROM LEFT TO RIGHT TOP TO BOTTOM EVEN THOUGH THEIR
    // LOCATIONS ARE SHUFFLED
    shuffle(charContainers);

    for (i = 0; i < charContainers.length; i++) {
        var c_container = charContainers[i];
        c_container.x = charContainerLocs[i].x;
        c_container.y = charContainerLocs[i].y;
        charPileContainer.addChild(c_container);
        c_container.alpha = 0;
        // SAVE OUR START LOCATIONS SO WE CAN TWEEN BACK ON STOP DRAG
        c_container.startX = c_container.x;
        c_container.startY = c_container.y;
    }
    for (i = 0; i < charContainers.length; i++) {
        var missingLtrTween = createjs.Tween.get(charContainers[i]).wait(i * 200).to({alpha: 1}, 300).call(missingLetterPlaced);
        missingLtrTween.on("change", function(){
            stage.update();
        });
    }
    stage.addChild(charPileContainer);
    charPileContainer.y = canvas.height / 2 - charPileContainer.getBounds().height / 2;
    charPileContainer.x = canvas.width / 1.33333333333 - charPileContainer.getBounds().width / 2;

}

function dragIt(event, data) {

    var dragged = event.currentTarget;
    var pt = charPileContainer.globalToLocal(event.stageX, event.stageY);

    dragged.x = pt.x - dragged.offsetPt.x;
    dragged.y = pt.y - dragged.offsetPt.y;
    stage.update();
    isColliding = checkIntersection(dragged, dragged.destination);

    data.bg_shape.graphics.clear();
    data.bg_shape.graphics.beginFill("rgba(1, 162, 166, 0.3)")
        .beginStroke("rgba(1, 162, 166, 1)")
        .drawRect(0, 0, charBoxSize, charBoxSize);
}

function stopDrag(event, data) {

    data.bg_shape.graphics.clear();
    data.bg_shape.graphics.beginStroke("rgba(1, 162, 166, 1)")
        .beginFill("rgba(0, 0, 0, 0.7)")
        .drawRect(0, 0, charBoxSize, charBoxSize);
    var placeItTween;
    if (isColliding) {
        isColliding = false;
        correctCount++;
        this.cursor = "default";
        this.off("pressmove", this.dragListener);
        this.off("mouseover", this.mouseOver_evt);
        this.off("mouseout", this.mouseOut_evt);
        this.off("pressup", this.pressupListener);
        var homeX = this.destinationX - charPileContainer.x + charGridContainer.x;
        var homeY = this.destinationY - charPileContainer.y + charGridContainer.y;
        placeItTween = createjs.Tween.get(this).to({'x': homeX, 'y': homeY}, 100, createjs.Ease.getPowIn(2.2));

        // UPDATING SESSION TO SHOW GAME IS IN PROGRESS
        // BUT NOT COMPLETE
        if (saveInit) {
            saveInit = false;
            if (game_status != 'in progress' && game_status != 'complete') {
                saveSession('in progress');
            }
        }
        if (correctCount == randomMissingSequence.length) {
            winMessage();
            if (game_status != 'complete') {
                saveSession('complete');
                game_status = 'complete';
            }
        }
    } else {
        placeItTween = createjs.Tween.get(this).to({'x': this.startX, 'y': this.startY}, 300, createjs.Ease.getPowIn(2.2));
    }
    placeItTween.on("change", function(){
        stage.update();
    });
}

function winMessage() {
    redrawOn = true;
    createjs.Tween.get(winTextHeader).to({'alpha': 1}, 300);
    createjs.Tween.get(winTextMessage).wait(300).to({'alpha': 1}, 300).call(function(){
        redrawOn = false;
    });
    createjs.Tween.get(headerText).to({'alpha': 0}, 300);
}

function checkIntersection(rect1, rect2) {

    var rect2X = charGridContainer.x + rect2.x | 0;
    var rect2Y = charGridContainer.y + rect2.y | 0;
    var rect1X = charPileContainer.x + rect1.x | 0;
    var rect1Y = charPileContainer.y + rect1.y | 0;

    var rect1Width = charBoxSize;
    var rect1Height = charBoxSize;
    var rect2Width = charBoxSize;
    var rect2Height = charBoxSize;

    return !(rect1X >= rect2X + rect2Width ||
    rect1X + rect1Width <= rect2X ||
    rect1Y >= rect2Y + rect2Height ||
    rect1Y + rect1Height <= rect2Y)
}
function returnRandomSet(arr, limit) {

    var indexSet = [];
    var indexResult = [];
    for (var i = 0; i < arr.length; i++) {
        indexSet.push(i);
    }
    for (i = 0; i < limit; i++) {
        indexResult.push(indexSet.splice(Math.random() * indexSet.length | 0, 1)[0]);
    }
    indexResult.sort(function (a, b) {
        return a - b
    });
    return indexResult;
}
function missingLetterPlaced() {
    missingLetterPlacementCount++;
    if (missingLetterPlacementCount == randomMissingSequence.length) {
        resetButton.regX = resetButton.getBounds().width / 2;
        resetButton.x = charPileContainer.getBounds().width / 2;
        resetButton.y = charPileContainer.getBounds().height + 40;
        resetButton.alpha = 0;
        charPileContainer.addChild(resetButton);
        var resetBtnTween = createjs.Tween.get(resetButton).to({'alpha': 1}, 500);
        resetBtnTween.on('change', function(){
            stage.update();
        });

        for (var i = 0; i < charPileContainer.children.length; i++) {
            var c_container = charPileContainer.children[i];
            c_container.mouseEnabled = true;
        }
    }
}
function woowagaxPlaced() {
    woowagaxLetterPlaced++;
    if (woowagaxLetterPlaced == woowagax.length) {
        placeMissing();
    }
}
function reset() {
    correctCount = 0;
    createjs.Tween.get(headerText).to({'alpha': 1}, 300);
    createjs.Tween.get(winTextHeader).to({'alpha': 0}, 300);
    createjs.Tween.get(winTextMessage).to({'alpha': 0}, 300);
    charGridContainer.removeAllChildren();
    charPileContainer.removeAllChildren();
    missingLetterPlacementCount = 0;
    woowagaxLetterPlaced = 0;
    build();
}

function explodeWoowagax() {

    redrawOn = true;

    for (var i = 0; i < charPileContainer.children.length; i++) {
        var c_container = charPileContainer.children[i];
        c_container.mouseEnabled = true;
    }

    resetButton.alpha = 0;
    var partsCount = 0;
    for (i = 0; i < charGridContainer.children.length; i++) {
        var child = charGridContainer.children[i];
        var randX = Math.random() < 0.5 ? -1000 : 1000;
        var randY = Math.random() < 0.5 ? -1000 : 1000;

        if (randY < 0) {
            randY = randY-Math.random()*1000;
        } else {
            randY = randY+Math.random()*1000;
        }
        createjs.Tween.get(child).to({'x': randX, 'y': randY}, 1000, createjs.Ease.quartOut).call(countParts);
    }
    for (i = 0; i < charPileContainer.children.length; i++) {
        var child2 = charPileContainer.children[i];
        var randX2 = Math.random() < 0.5 ? -1000 : 1000;
        var randY2 = Math.random() < 0.5 ? -1000 : 1000;

        if (randY2 < 0) {
            randY2 = randY2-Math.random()*1000;
        } else {
            randY2 = randY+Math.random()*1000;
        }
        createjs.Tween.get(child2).to({'x': randX2, 'y': randY2}, 1000, createjs.Ease.quartIn).call(countParts);
    }
    function countParts() {
        partsCount++;
        if (partsCount == charGridContainer.children.length + charPileContainer.children.length) {
            redrawOn = false;
            reset();
        }
    }
}
setTimeout(function () {
    init();
}, 3000);
