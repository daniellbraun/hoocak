var statusMessages = {
    complete: "You have completed this game by correctly choosing the correct Hoocąk word for the number of balls displayed 10 times in a row.",
    partialComplete: "You've started learning the Hoocąk words for the first 10 numbers. However, you have yet to correctly choose the Hoocąk " +
    "word for the number of balls displayed 10 times in a row.",
    unstarted: "This is your first time here! Above will be displayed 1 - 10 balls. Count the balls and choose the correct Hoocąk word for " +
    "that number. See if you can guess 10 in a row."
};
updateStatusMessage();

var numbers = [
    {
        'digit': 1,
        'hochunkword': 'hižąkiira',
        'pronounce': 'hee-zh(ah)-kee-rdah',
        'englishword': 'one',
        'audio': '1.mp3'
    },
    {
        'digit': 2,
        'hochunkword': 'nųųp',
        'pronounce': 'n(oo)mp',
        'englishword': 'two',
        'audio': '2.mp3'
    },
    {
        'digit': 3,
        'hochunkword': 'taanį',
        'pronounce': 'dah-n(ee)',
        'englishword': 'three',
        'audio': '3.mp3'
    },
    {
        'digit': 4,
        'hochunkword': 'joop',
        'pronounce': 'joep',
        'englishword': 'four',
        'audio': '4.mp3'
    },
    {
        'digit': 5,
        'hochunkword': 'saacą',
        'pronounce': 'sah-ch(ah)',
        'englishword': 'five',
        'audio': '5.mp3'
    },
    {
        'digit': 6,
        'hochunkword': 'hakewe',
        'pronounce': 'hah-kay-way',
        'englishword': 'six',
        'audio': '6.mp3'
    },
    {
        'digit': 7,
        'hochunkword': 'šaagowį',
        'pronounce': 'shah-go-w(ee)',
        'englishword': 'seven',
        'audio': '7.mp3'
    },
    {
        'digit': 8,
        'hochunkword': 'haruwąk',
        'pronounce': 'hah-rdoo-w(ah)k',
        'englishword': 'eight',
        'audio': '8.mp3'
    },
    {
        'digit': 9,
        'hochunkword': 'hižąkicųšgųnį',
        'pronounce': 'hee-zh(ah)-kee-ch(oo)sh-g(oo)-n(ee)',
        'englishword': 'nine',
        'audio': '9.mp3'
    },
    {
        'digit': 10,
        'hochunkword': 'kerepąnąižą',
        'pronounce': 'kay-rdah-p(ah)-ny-zh(ah)',
        'englishword': 'ten',
        'audio': '10.mp3'
    }
];
var balls = [];
var smallestY;
var buttonContainer = new createjs.Container();
var ballsContainer = new createjs.Container();
stage.addChild(ballsContainer);
var buttonSpacing = 10;
var ballRadius = 50;
var randomNum;
var correctInARow = 0;
var lastCorrectInARow = 0;

var boundsShape;

var headingStr = "Count the balls and choose the correct\nHoocąk word for the number below:";
var headingText = new createjs.Text(headingStr, "bold 36px Arial", "#FFFFFF");
headingText.shadow = new createjs.Shadow("#000", 3, 3, 3);
stage.addChild(headingText);
headingText.x = canvas.width / 2;
headingText.textAlign = "center";
headingText.y = 20;
headingText.alpha = 0;

var correctText = new createjs.Text("", "bold 60px Arial", "#FFFFFF");
correctText.shadow = new createjs.Shadow("#000", 3, 3, 5);
stage.addChild(correctText);
correctText.x = canvas.width / 2;
correctText.textAlign = "center";
correctText.alpha = 0;

var scoreTextHeader = new createjs.Text("Correct In A Row:", "bold 20px Arial", "#CCC");
scoreTextHeader.textAlign = "center";
scoreTextHeader.shadow = new createjs.Shadow("#000", 3, 3, 3);
scoreTextHeader.setBounds(0, 0, scoreTextHeader.getMeasuredWidth(), scoreTextHeader.getMeasuredHeight());

var scoreTextNumber = new createjs.Text("0", "bold 50px Arial", "#CCC");
scoreTextNumber.textAlign = "center";
scoreTextNumber.shadow = new createjs.Shadow("#000", 3, 3, 3);
scoreTextNumber.setBounds(0, 0, scoreTextNumber.getMeasuredWidth(), scoreTextNumber.getMeasuredHeight());
// scoreTextNumber.regX = scoreTextNumber.getBounds().width / 2;
scoreTextNumber.regY = scoreTextNumber.getBounds().height / 2;
scoreTextNumber.y = scoreTextHeader.getBounds().height + (scoreTextNumber.getBounds().height / 2);
scoreTextNumber.x = 0;

var scoreContainer = new createjs.Container();
scoreContainer.addChild(scoreTextHeader, scoreTextNumber);
scoreContainer.alpha = 0;
scoreContainer.y = 20;
scoreContainer.x = (scoreContainer.getBounds().width / 2) + 20;

var randomNumberOfBallsPool = [];

var saveInit = true;

function init() {
    shuffle(numbers);

    if (!randomNumberOfBallsPool.length) refillPool();

    if (!stage.contains(scoreContainer)) stage.addChild(scoreContainer);
    createjs.Tween.get(scoreContainer).to({'alpha': 1}, 500);

    var yPos = 0;
    var xPos = 0;
    for (var i = 0; i < numbers.length; i++) {
        var btnWidth = (canvas.width / 5) - 20;
        var btn = createButton(btnWidth, 50, numbers[i].hochunkword);
        if (i % 5 == 0 && i != 0) {
            yPos += 50 + buttonSpacing;
            xPos = 0;
        }
        btn.x = xPos;
        xPos += btnWidth + buttonSpacing;
        btn.y = yPos;
        buttonContainer.addChild(btn);
        btn.data = numbers[i];
        btn.audio_id = 'audio_' + (numbers[i].digit - 1);
        btn.on("click", checkAnswer, null, true);
    }

    if (!stage.contains(buttonContainer)) stage.addChild(buttonContainer);

    buttonContainer.alpha = 0;
    var buttonCtnrWidth = buttonContainer.getBounds().width;
    var buttonCtnrHeight = buttonContainer.getBounds().height;
    buttonContainer.x = canvas.width / 2 - buttonCtnrWidth / 2;
    var bottomBtnSpace = (canvas.width - buttonCtnrWidth) / 2;
    buttonContainer.y = canvas.height - buttonCtnrHeight - bottomBtnSpace;

    var buttonsTween = createjs.Tween.get(buttonContainer).to({'alpha': 1}, 500);
    buttonsTween.on('change', function () {
        stage.update();
    });

    placeBalls();
}
function refillPool() {
    for(var i = 1; i <= numbers.length; i++) {
        randomNumberOfBallsPool.push(i);
    }
    shuffle(randomNumberOfBallsPool);
}
function checkAnswer(event) {
    var data = event.currentTarget.data;
    createjs.Sound.play(event.currentTarget.audio_id,
        {
            interrupt: createjs.Sound.INTERRUPT_NONE,
            loop: 0,
            volume: 1
        }
    );

    // UPDATING SESSION TO SHOW GAME IS IN PROGRESS
    // BUT NOT COMPLETE
    if (saveInit) {
        saveInit = false;
        if (game_status != 'in progress' && game_status != 'complete') {
            saveSession('in progress');
        }
    }
    if (data.digit != randomNum) dropButton(event.currentTarget);
    if (data.digit == randomNum) youWin(event.currentTarget);
}
function dropButton(btn) {

    correctInARow = 0;
    animateScore();
    lastCorrectInARow = correctInARow;

    var hochunkword = btn.data.hochunkword;
    var digit = btn.data.digit;
    var redSign = createRedSign(btn.getBounds().width, btn.getBounds().height, hochunkword + " means " + digit + ",\nthat's incorrect");
    redSign.x = btn.x;
    redSign.y = btn.y;
    buttonContainer.addChild(redSign);
    buttonContainer.swapChildren(btn, redSign);
    var dropTween = createjs.Tween.get(btn).to({'y': canvas.height - buttonContainer.y + 10}, 500, createjs.Ease.backIn);
    dropTween.on('change', stageUpdate);
}
function youWin(btn) {

    correctInARow++;
    animateScore();
    lastCorrectInARow = correctInARow;

    createjs.Tween.get(headingText).to({'alpha': 0}, 500);
    buttonContainer.mouseEnabled = false;
    correctText.text = "That's Correct!!!" +
        "\n\n" + btn.data.hochunkword + " means " + btn.data.digit;
    correctText.regY = correctText.getMeasuredHeight() / 2;
    correctText.scaleX = correctText.scaleY = 10;
    correctText.cache(
        -correctText.getMeasuredWidth() / 2,
        0,
        correctText.getMeasuredWidth(),
        correctText.getMeasuredHeight() + 10
    );

    correctText.y = canvas.height / 2 - correctText.getMeasuredHeight() / 2;
    var youWinTween = createjs.Tween.get(correctText).to(
        {
            'scaleX': 1,
            'scaleY': 1,
            'alpha': 1
        }, 500, createjs.Ease.quadIn).to(
        {
            'scaleX': 1.1,
            'scaleY': 1.1
        }, 6000).to(
        {
            'alpha': 0
        }, 500).call(clearStage);
    youWinTween.on('change', stageUpdate);
}
function animateScore() {
    var scoreTween;
    if (correctInARow == lastCorrectInARow) return;
    if (correctInARow == 0) {
        scoreTextNumber.color = "darkred";
        scoreTween = createjs.Tween.get(scoreTextNumber).to(
            {
                'scaleX': 0,
                'scaleY': 0
            }, 400, createjs.Ease.quadOut).call(function () {
            scoreTextNumber.color = "#CCC";
            scoreTextNumber.text = correctInARow;
            scoreTextNumber.setBounds(0, 0, scoreTextNumber.getMeasuredWidth(), scoreTextNumber.getMeasuredHeight());
            scoreTextNumber.x = 0;
            createjs.Tween.get(scoreTextNumber).to(
                {
                    'scaleX': 1,
                    'scaleY': 1
                }, 200, createjs.Ease.quadIn);
        });
    } else {
        scoreTextNumber.color = "#00FF00";
        scoreTween = createjs.Tween.get(scoreTextNumber).to(
            {
                'scaleX': 0,
                'scaleY': 0
            }, 400, createjs.Ease.getBackIn(4)).call(function () {
            // scoreTextNumber.color = "#CCC";
            scoreTextNumber.text = correctInARow;
            scoreTextNumber.setBounds(0, 0, scoreTextNumber.getMeasuredWidth(), scoreTextNumber.getMeasuredHeight());
            scoreTextNumber.x = 0;
            scoreTween = createjs.Tween.get(scoreTextNumber).to(
                {
                    'scaleX': 1,
                    'scaleY': 1
                }, 200, createjs.Ease.quadIn).call(blink);
        });
        if (correctInARow == 10) {
            if (game_status != 'complete') {
                saveSession('complete');
                game_status = 'complete';
            }
        }
    }
    scoreTween.on('change', stageUpdate);
    function blink() {
        var blinkTween = createjs.Tween.get(this)
            .to({'alpha': 0}, 300)
            .to({'alpha': 1}, 300)
            .to({'alpha': 0}, 300).call(function () {
                scoreTextNumber.color = "#CCC"
            })
            .to({'alpha': 1}, 300);
        blinkTween.on('change', stageUpdate);
    }
}
function clearStage() {
    redrawOn = true;
    var ballDropCount = 0;
    for (var i = 0; i < balls.length; i++) {
        var belowStage = (smallestY - ballsContainer.y) + canvas.height;
        createjs.Tween.get(balls[i]).wait(i * 150).to({'y': belowStage}, 500, createjs.Ease.quadIn).call(countBalls);
    }
    function countBalls() {
        ballDropCount++;
        if (ballDropCount == balls.length) {

            createjs.Tween.get(buttonContainer).to({'alpha': 0}, 500).call(restart);
        }
    }
}
function restart() {
    redrawOn = false;
    balls = [];
    buttonContainer.removeAllChildren();
    buttonContainer.mouseEnabled = true;
    init();
}
function placeBalls() {

    ballsContainer.removeAllChildren();
    var numOfBalls = getNewRandom();
    randomNum = numOfBalls;

    var xVals = [];
    var yVals = [];

    for (var i = 0; i < numOfBalls; i++) {
        var ballGraphic = new createjs.Graphics();
        ballGraphic.beginRadialGradientFill
        (
            [getRandomColor(), "#000"],
            [0, 1],
            -10,
            -10,
            0,
            -10,
            -10,
            ballRadius
        ).drawCircle(0, 0, ballRadius);
        var ball = new createjs.Shape(ballGraphic);
        ball.cache(-ballRadius, -ballRadius, ballRadius * 2 + 2, ballRadius * 2);
        ball.setBounds(-ballRadius, -ballRadius, ballRadius * 2, ballRadius * 2);
        ball.name = 'ball_' + i;
        balls.push(ball);
        ball.on('mousedown', function () {
            console.log(this.name)
        })
    }

    function createXYValues() {
        if (xVals.length) {
            xVals = [];
            yVals = [];
        }
        for (var i = 0; i < numOfBalls; i++) {
            xVals.push((canvas.width - 300) * Math.random() | 0);
            yVals.push((canvas.height - 400) * Math.random() | 0);

        }
    }

    createXYValues();

    while (checkMyBalls(xVals, yVals, balls)) {
        createXYValues();
    }

    for (i = 0; i < numOfBalls; i++) {
        balls[i].x = xVals[i];
        balls[i].y = yVals[i];
        ballsContainer.addChild(balls[i]);
    }

    var smallestX = Math.min.apply(null, xVals);
    var smallestXIndex = xVals.indexOf(smallestX);
    smallestY = Math.min.apply(null, yVals);
    var smallestYIndex = yVals.indexOf(smallestY);

    ballsContainer.regX = ballsContainer.children[smallestXIndex].x - ballRadius;
    ballsContainer.regY = ballsContainer.children[smallestYIndex].y - ballRadius;

    // var ballsWidth = ballsContainer.getBounds().width;
    // var ballsHeight = ballsContainer.getBounds().height;
    //
    // if (stage.contains(boundsShape)) stage.removeChild(boundsShape);
    //
    // boundsShape = new createjs.Shape();
    // boundsShape.graphics.beginFill('rgba(1,1,1,0.3)');
    // boundsShape.graphics.drawRect(0, 0, ballsWidth, ballsHeight);
    //
    // stage.addChild(boundsShape);
    //
    // ballsContainer.x = boundsShape.x = canvas.width / 2 - ballsContainer.getBounds().width / 2;
    // ballsContainer.y = boundsShape.y = 100;
    ballsContainer.x = canvas.width / 2 - ballsContainer.getBounds().width / 2;
    ballsContainer.y = 150;

    redrawOn = true;
    var tweenCount = numOfBalls;
    for (i = 0; i < numOfBalls; i++) {
        // MOVE BALLS TO THE TOP OF THE STAGE.
        // IF THIS DOENS'T MAKE SENSE IN THE FUTURE CONSIDER THESE EXPRESSIONS:
        // (3-8)+10 = 5 ... (3+10)-8 = 5 BOTH EQUAL FIVE :)
        balls[i].y = (smallestY - ballsContainer.y) - ballRadius * 2;

        createjs.Tween.get(balls[i]).wait(i * 130).to({'y': yVals[i]}, 2000, createjs.Ease.bounceOut).call(
            function () {
                tweenCount--;
                if (tweenCount == 0) {
                    redrawOn = false;
                }
            }
        );
    }
    var headingTween = createjs.Tween.get(headingText).wait(2000).to({'alpha': 1}, 500);
    headingTween.on('change', stageUpdate);
}

function getRandomColor() {
    var hex = Math.floor(Math.random() * 16777215).toString(16);

    while (hex.length < 6) {
        hex += 0;
    }
    return '#' + hex;
}

function checkMyBalls(xArray, yArray, balls) {
    for (var i = 0; i < balls.length; i++) {
        var xValToCheck = xArray[i];
        var yValToCheck = yArray[i];
        for (var j = 0; j < balls.length; j++) {
            if (Math.abs(xValToCheck - xArray[j]) < ballRadius
                && Math.abs(yValToCheck - yArray[j]) < ballRadius
                && balls[i] != balls[j]) {

                return balls[i].name;
            }
        }
    }
    return false;
}

function getNewRandom() {
    var randomNumArr = randomNumberOfBallsPool.splice(0, 1);
    return randomNumArr[0];
}
function createRedSign(w, h, text) {
    var button_bg = new createjs.Shape();
    button_bg.graphics.beginFill('darkred');
    button_bg.graphics.setStrokeStyle(2, 'round').beginStroke('darkred');
    button_bg.graphics.drawRoundRect(0, 0, w, h, 5);
    button_bg.alpha = .5;

    var button_text = new createjs.Text(text, "20px Arial", "#FFF");
    button_text.maxWidth = w;
    button_text.textAlign = "center";
    button_text.textBaseline = "bottom";
    button_text.shadow = new createjs.Shadow('#000000', 2, 2, 2);

    button_text.y = h / 2;
    button_text.x = w / 2;

    var signContainer = new createjs.Container();
    signContainer.addChild(button_bg);
    signContainer.addChild(button_text);

    signContainer.setBounds(0, 0, w, h);

    return signContainer;
}
function stageUpdate() {
    stage.update();
}
setTimeout(function () {
    var loadingText = new createjs.Text('', "bold 45px Arial", "white");
    loadingText.textAlign = 'center';
    var audio_manifest = [];
    for (var i = 0; i < numbers.length; i++) {
        audio_manifest.push(
            {
                id: 'audio_' + i,
                src: audio_url + 'numbers/' + numbers[i].audio
            }
        )
    }
    var preloaded_audio = new createjs.LoadQueue();
    preloaded_audio.installPlugin(createjs.Sound);
    stage.addChild(loadingText);
    preloaded_audio.on('progress', function (event) {
        var loadProgress = Math.round(100 * event.progress);
        loadingText.text = 'Loading: ' + loadProgress + '%';
        loadingText.x = canvas.width / 2;
        loadingText.y = canvas.height / 2;
        if (loadProgress == 100) {
            stage.removeChild(loadingText);
        }
        stage.update();
    });
    preloaded_audio.on('complete', function () {
        init();
    });
    preloaded_audio.loadManifest(audio_manifest);

}, 2000);