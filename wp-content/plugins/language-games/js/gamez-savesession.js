saveSession = function (status) {
    var data = {
        'action': 'save_session',
        'game_id': save_data.game_id,
        'game_status': status
    };
    jQuery.post(save_data.ajax_url, data, function (response) {
        // console.log('saveSession from the server:', response);
        updateStatusMessage();
    });
};
updateStatusMessage = function () {
    var data = {
        'action': 'get_session_status',
        'game_id': save_data.game_id
    };
    jQuery.post(save_data.ajax_url, data, function (response) {
        // console.log('updateStatusMessage from the server:', response);
        var message;
        if (response == 'in progress') {
            message = statusMessages.partialComplete;
        } else if (response == 'complete') {
            message = statusMessages.complete;
        } else {
            message = statusMessages.unstarted;
        }
        displayProgressMessage(message, response);
    });
};
displayProgressMessage = function (message, response) {
    var gameProgress = $("#game-progress");

    if (gameProgress.find("div").length) {
        gameProgress.find("div").remove();
    }

    if (response == '') {
        gameProgress.append('<div class="callout primary"><h4>Welcome!</h4><p>' + message + '</p></div>');
    } else if (response == 'in progress') {
        gameProgress.append('<div class="callout alert"><h4>Game In Progress</h4><p>' + message + '</p></div>');
    } else if (response == 'complete') {
        displayRibbon();
        gameProgress.append('<div class="callout success"><h4>Game Completed!</h4><p>' + message + '</p></div>');
    }

};
function displayRibbon() {

    var ribbonQueue = new createjs.LoadQueue();
    ribbonQueue.loadManifest([
        {
            id: "ribbon", src: image_url + "banner-3b.png"
        }
    ]);
    ribbonQueue.on("complete", function(){
        var ribbon = new createjs.Bitmap(ribbonQueue.getResult("ribbon"));
        ribbon.x = 1200 - ribbon.image.width;
        if (stage == undefined) {
            stage = new createjs.Stage(document.getElementById("gamezCanvas"));
        }
        // console.log('stage:', stage);
        stage.addChild(ribbon);
        ribbon.alpha = 0;
        var ribbonTween = createjs.Tween.get(ribbon).to({'alpha': 1}, 500);
        ribbonTween.on('change', function(){
            stage.update();
        })
    });
}
// updateCards = function() {
//     var url = window.location.href;
//
//     var $gamesTab = $("#games");
//     var $infoCardLinks = $gamesTab.find(".info-card-link");
//     var $links = $infoCardLinks.find("a");
//
//     for (var i = 0; i < $links.length; i++) {
//         if (url == $links[i].href) {
//             var $infoCardLink = $($infoCardLinks[i]);
//             var $previous = $infoCardLink.prev();
//             $previous.append('<p>I Found You!</p>').css({
//                 'color': 'blue',
//                 'line-height': '0'
//             });
//         }
//
//     }
// };