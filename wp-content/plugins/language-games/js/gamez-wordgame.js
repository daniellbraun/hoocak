var statusMessages = {
    complete: "You have completed this game by seeing and hearing the Hoocąk word for each picture in each category.",
    partialComplete: "You've started learning Hoocąk words for each of the images, but have not yet completed every picture in every category.",
    unstarted: "This is your first time here! Choose a category and click on each image to see and hear the Hoocąk word for the displayed images."
};

updateStatusMessage();

var completionMessage;
var messageField;
var preloaded_assets;
var gameObjects = [];
var gameObjectMaxWidth = 200;
var game_pieces_data = gamez_data;
var gameProgress = 0;
var soundsIdsArray = [];

var catMenuContainer = new createjs.Container();
var game_pieces_container = new createjs.Container();

var currentCategoryID = gamez_categories[0].term_id;

var saveInit = true;

var moreButton = createButton(200, 50, '', '18px');
moreButton.x = canvas.width - moreButton.getBounds().width - 20;
moreButton.y = canvas.height - moreButton.getBounds().height - 20;
moreButton.on('click', nextPage);
moreButton.alpha = 0;
stage.addChild(moreButton);

var currentPage = 1;
var totalPages = 1;
var maxDisplay = 4;

function init() {

    shuffle(game_pieces_data);

    messageField = new createjs.Text("", "bold 24px Arial", "#FFFFFF");
    messageField.maxWidth = 1000;
    messageField.textAlign = "center";
    messageField.textBaseline = "middle";
    messageField.x = canvas.width / 2;
    messageField.y = canvas.height - 50;
    messageField.shadow = new createjs.Shadow('#000000', 3, 3, 3);
    stage.addChild(messageField);
    stage.addChild(game_pieces_container);

    var soundsURLsWithDuplicates = [];

    for (var i = 0; i < game_pieces_data.length; i++) {
        var audioURL = game_pieces_data[i].audio_url || audio_url + 'placeholder.mp3';
        soundsURLsWithDuplicates.push(audioURL);
    }

    soundsIdsArray = getAudioIDsList(soundsURLsWithDuplicates);

    var audio_manifest = getUniqueAudioManifest(soundsURLsWithDuplicates);

    var image_manifest = [];

    for (i = 0; i < game_pieces_data.length; i++) {
        if (game_pieces_data[i].image_url_acf == null) {
            image_manifest.push({id: "image" + i, src: image_url + 'missing-image.png'});
        }else{
            image_manifest.push({id: "image" + i, src: game_pieces_data[i].image_url_acf});
        }
    }
    createjs.Sound.alternateExtensions = ["mp3"];
    preloaded_assets = new createjs.LoadQueue();
    preloaded_assets.installPlugin(createjs.Sound);
    preloaded_assets.on("complete", handlePreloadComplete, this);
    preloaded_assets.on("fileload", fileLoaded, this);
    preloaded_assets.on("error", handleError, this);
    preloaded_assets.on("progress", progressHandler, this);

    preloaded_assets.loadManifest(audio_manifest);
    preloaded_assets.loadManifest(image_manifest);
    //greetUser();


    // var moreGamesButton = createButton(150, 70, "More Games", "22px");
    // moreGamesButton.x = canvas.width - moreGamesButton.getBounds().width - 20;
    // moreGamesButton.y = canvas.height - moreGamesButton.getBounds().height - 20;
    // stage.addChildAt(moreGamesButton, stage.children.length-1);
    // moreGamesButton.on('click', function(){
    //     window.location = location.origin + '/learning-materials/?tab=games';
    // })
}
function progressHandler(event) {
    var loadProgress = Math.round(100 * event.progress);
    messageField.text = "Loading: " + loadProgress + "%";
    messageField.x = canvas.width / 2;
    stage.update();
}
function handleError(event) {
    console.log("ERROR:", event.title);
}
function fileLoaded(event) {
    // console.log('fileloaded:', event.item.src);
}

function handlePreloadComplete() {
    var bitmap;

    for (var i = 0; i < game_pieces_data.length; i++) {

        bitmap = new createjs.Bitmap(preloaded_assets.getResult('image' + i));
        bitmap.shadow = new createjs.Shadow('#000000', 4, 4, 4);
        var imageW = bitmap.image.width;

        if (imageW > gameObjectMaxWidth) {
            bitmap.scaleX = bitmap.scaleY = gameObjectMaxWidth / imageW;
        }

        bitmap.regX = bitmap.image.width / 2;
        bitmap.regY = bitmap.image.height / 2;

        // GET CATEGORIES
        var catTermIds = [];
        for (var j = 0; j < game_pieces_data[i].category.length; j++) {
            catTermIds.push(game_pieces_data[i].category[j].term_id);
        }

        gameObjects.push(
            {
                "bitmap": bitmap,
                "categories": catTermIds
            }
        );

        bitmap.cursor = "pointer";

        bitmap.on("mousedown", gameObjectTouched, null, false,
            {
                "sound": soundsIdsArray[i],
                "word": game_pieces_data[i].word,
                "englishword": game_pieces_data[i].english_word,
                "index": i
            }
        );
    }
    createCategoryMenu();
    introAnimation();
}
var wordSoundInstance;
function gameObjectTouched(evt, data) {

    // UPDATING SESSION TO SHOW GAME IS IN PROGRESS
    // BUT NOT COMPLETE
    if (saveInit) {
        saveInit = false;
        if (game_status != 'in progress' && game_status != 'complete') {
            saveSession('in progress');
        }
    }

    var bitmap = evt.target;
    var bitmapOrigXScale = bitmap.scaleX;
    var bitmapOrigYScale = bitmap.scaleY;


    var foodClickTween = createjs.Tween.get(bitmap).to(
        {
            scaleX: bitmapOrigXScale + 0.2,
            scaleY: bitmapOrigYScale + 0.2
        },
        100,
        createjs.Ease.getPowInOut(2)).call(handleComplete1).to(
        {
            scaleX: bitmapOrigXScale,
            scaleY: bitmapOrigYScale
        },
        500,
        createjs.Ease.getPowInOut(2)).call(handleComplete2);

    foodClickTween.on('change', function () {
        stage.update();
    });
    function handleComplete1() {

        messageField.text = data.word || "";

        if (data.englishword) {
            messageField.text += " (" + data.englishword + ")";
        }

        if (wordSoundInstance) {
            wordSoundInstance.stop();
        }
        wordSoundInstance = createjs.Sound.play(data.sound,
            {
                interrupt: createjs.Sound.INTERRUPT_NONE,
                loop: 0,
                volume: 1
            }
        );
    }

    function handleComplete2() {

        if (!bitmap.touched) {
            bitmap.touched = true;
            gameProgress++;
            if (gameProgress == gameObjects.length && game_status != 'complete') {
                game_status = 'complete';
                dialogbox("Congratulations! You have completed this course. Feel free to continue learning.");
                saveSession('complete');
                //completionMessage.text = "Hey, it looks like you've already done this. Good for you!";
                //saveProgress();
            }
        }
    }
}

function introAnimation() {

    moreButton.mouseEnabled = false;

    var objectsInCategory = [];
    var objectsToDisplay = [];

    redrawOn = true;

    for (var i = 0; i < gameObjects.length; i++) {

        if (gameObjects[i].categories.indexOf(currentCategoryID) > -1) {
            objectsInCategory.push(gameObjects[i]);
        }
    }

    totalPages = Math.ceil(objectsInCategory.length / maxDisplay);

    if (totalPages > 1) {
        for (i = 0; i < maxDisplay; i++) {

            var indexOffset = i + (maxDisplay * (currentPage - 1));

            if (objectsInCategory[indexOffset]) {
                objectsToDisplay.push(objectsInCategory[indexOffset]);
            } else {
                break;
            }

        }
    } else {
        objectsToDisplay = objectsInCategory.slice();
    }

    var divisor = 2;

    if (objectsToDisplay.length == 1) {
        divisor = 2;
    } else if (objectsToDisplay.length == 2) {
        divisor = 3;
    } else if (objectsToDisplay.length == 3) {
        divisor = 4;
    } else if (objectsToDisplay.length >= 4) {
        divisor = 5;
    }
    var xPosInc = canvas.width / divisor;

    var tweenCount = objectsToDisplay.length;


    for (i = 0; i < gamez_categories.length; i++) {
        if (gamez_categories[i].term_id == currentCategoryID) {
            var categoryName = gamez_categories[i].name;
            break;
        }
    }

    if (totalPages > 1) {
        moreButton.alpha = 1;
        moreButton.textField.text = "More in: " + categoryName;
    } else {
        moreButton.alpha = 0;
    }

    for (i = 0; i < objectsToDisplay.length; i++) {

        objectsToDisplay[i].bitmap.y = -(objectsToDisplay[i].bitmap.image.height / 2);
        objectsToDisplay[i].bitmap.x = (i + 1) * xPosInc;

        game_pieces_container.addChild(objectsToDisplay[i].bitmap);

        createjs.Tween.get(objectsToDisplay[i].bitmap).wait(i * 250).to(
            {
                y: canvas.height / 2
            },
            500,
            createjs.Ease.getElasticIn(60, 35)).call(doneMoving);

        function doneMoving() {
            tweenCount--;
            if (tweenCount == 0) {
                stage.update();
                redrawOn = false;

                if (totalPages > 1) {
                    moreButton.mouseEnabled = true;
                    moreButton.alpha = true;
                    stage.update();
                }
                for (var i = 0; i < catMenuContainer.children.length; i++) {
                    if (catMenuContainer.children[i].indicator.alpha == 0) {
                        catMenuContainer.children[i].mouseEnabled = true;
                    }
                }
            }
        }
    }
    messageField.text = 'Click a picture to learn the Hoocąk word for it. Page ' +currentPage+ ' of ' +totalPages;
}
function nextPage() {

    moreButton.mouseEnabled = false;
    for (var i = 0; i < catMenuContainer.children.length; i++) {
        catMenuContainer.children[i].mouseEnabled = false;
    }

    currentPage++;
    currentPage = (currentPage > totalPages) ? 1 : currentPage;
    redrawOn = true;
    moreButton.mouseEnabled = false;
    createjs.Tween.get(game_pieces_container)
        .to(
            {
                'y': canvas.height
            },
            500, createjs.Ease.getPowInOut(2))
        .call(afterDrop);

    function afterDrop() {
        game_pieces_container.removeAllChildren();
        game_pieces_container.y = 0;
        introAnimation();
    }
}

function createCategoryMenu() {

    var containerWidth = 0;
    var buttonWidth = 150;
    var buttonHeight = 50;
    var xIncr = 0;
    var yIncr = 0;
    for (var i = 0; i < gamez_categories.length; i++) {
        var btn = createButton(buttonWidth, buttonHeight, gamez_categories[i].name, "16px");
        if (gamez_categories[i].term_id == currentCategoryID) {
            btn.indicator.alpha = 1;
            btn.mouseEnabled = false;
        }
        var padding = (i > 0) ? 10 : 0;
        btn.x = (xIncr * buttonWidth) + (padding * xIncr);
        btn.y = yIncr;
        xIncr++;

        if ((i+1)%7 == 0) {
            xIncr = 0;
            yIncr += buttonHeight+padding;
        }
        if (i < 7) {
            containerWidth += buttonWidth + 10;
        }

        catMenuContainer.addChild(btn);
        btn.on('click', newCategory, null, false,
            {
                'category': gamez_categories[i].term_id
            });
    }
    stage.addChild(catMenuContainer);
    catMenuContainer.y = 20;
    catMenuContainer.x = canvas.width / 2 - containerWidth / 2;
}

function newCategory(event, data) {

    currentPage = 1;

    for (var i = 0; i < catMenuContainer.children.length; i++) {

        catMenuContainer.children[i].mouseEnabled = false;

        if (event.currentTarget == catMenuContainer.children[i]) {
            //catMenuContainer.children[i].mouseEnabled = false;
            catMenuContainer.children[i].indicator.alpha = 1;
        } else {
            //catMenuContainer.children[i].mouseEnabled = true;
            catMenuContainer.children[i].indicator.alpha = 0;
        }
    }

    currentCategoryID = data.category;

    redrawOn = true;
    createjs.Tween.get(game_pieces_container)
        .to(
            {
                'y': canvas.height
            },
            500, createjs.Ease.getPowInOut(2))
        .call(afterDrop);

    function afterDrop() {
        game_pieces_container.removeAllChildren();
        game_pieces_container.y = 0;
        introAnimation();
    }
}
setTimeout(function () {
    init();
}, 2000);