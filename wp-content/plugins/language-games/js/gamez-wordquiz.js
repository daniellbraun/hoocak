var statusMessages = {
    complete: "You have completed the quiz for each of the categories. Good job!",
    partialComplete: "You've started these set of quizzes but have yet to complete them all.",
    unstarted: "This is your first time here! Choose a category and answer correctly each of the questions."
};

updateStatusMessage();

shuffle(word_quiz_data);

var numOfQuestions = word_quiz_data.length;
var currentQuestion = 0;

var currentCategoryID;
var currentCategoryName;
var currentQuestionSet = [];
var currentCatQuestionAudioIds = [];
var currentCatRolloverAudioIds_arrays = [];
var currentCatResponseAudioIds_arrays = [];
var currentCatImagesIds = [];

var questionField = new createjs.Text("", "30px Arial", "#FFF");
questionField.maxWidth = 1200;
questionField.textAlign = "center";
questionField.y = 15;
questionField.shadow = new createjs.Shadow('#000000', 2, 2, 2);

var responseField = new createjs.Text("", "25px Arial", "#FFF");
responseField.maxWidth = 700;
responseField.textAlign = "center";
responseField.y = canvas.height - 40;
responseField.shadow = new createjs.Shadow('#000000', 2, 2, 2);
var answers_container = new createjs.Container();
var nextButton;

var question_image_bitmap;
var question_background_image;

var questionProgressField = new createjs.Text("", "bold 16px Arial", "#FFF");
questionProgressField.maxWidth = 1200;
questionProgressField.textAlign = "center";
questionProgressField.y = 50;
questionProgressField.shadow = new createjs.Shadow('#000000', 2, 2, 2);

var preloaded_assets;
var preloaded_question_audio_assets;
var preloaded_rollover_audio_assets;
var preloaded_response_audio_assets;

var answerButtonHeight = 70;
var answerButtonWidth = 300;

var nextButtonHeight = 40;
var nextButtonWidth = 100;

var titleBar = new createjs.Shape();
titleBar.graphics.beginFill('#01a2a6');
titleBar.graphics.drawRect(0, 0, canvas.width, 75);
// titleBar.alpha = .8;

var responseBar = new createjs.Shape();
responseBar.graphics.beginFill('rgba(0,0,0,0.7)');
responseBar.graphics.drawRect(0, 0, canvas.width, 50);
responseBar.y = canvas.height;

var imagesIds = [];
var questionAudioIds;
var rolloverAudioIds_flat = [];
var responseAudioIds_flat = [];
var rolloverAudioIds_arrays = [];
var responseAudioIds_arrays = [];

var saveInit = true;

function init() {

    stage.addChild(titleBar);
    stage.addChild(questionField);
    stage.addChild(responseBar);
    stage.addChild(responseField);
    stage.addChild(answers_container);
    stage.addChild(questionProgressField);

    var originalQuestionAudioURLs = [];
    var originalRolloverAudioURLs = [];
    var originalResponseAudioURLs = [];

    for (var i = 0; i < numOfQuestions; i++) {

        var questionAudioURL = word_quiz_data[i].question_audio.url || audio_url + 'placeholder.mp3';

        originalQuestionAudioURLs.push(questionAudioURL);

        var rollover_arr = [];
        var response_arr = [];
        for (var ii = 0; ii < word_quiz_data[i].answers.length; ii++) {
            var rollover_url = word_quiz_data[i].answers[ii].answer_rollover_audio.url || audio_url + 'placeholder.mp3';
            var response_url = word_quiz_data[i].answers[ii].response_audio.url || audio_url + 'placeholder.mp3';
            rollover_arr.push(rollover_url);
            response_arr.push(response_url);
        }
        originalRolloverAudioURLs = originalRolloverAudioURLs.concat(rollover_arr);
        originalResponseAudioURLs = originalResponseAudioURLs.concat(response_arr);
    }
    questionAudioIds = getAudioIDsList(originalQuestionAudioURLs);

    //console.log('questionAudioIds:', questionAudioIds);

    var images_manifest = [];
    var rollover_audio_manifest = getUniqueAudioManifest(originalRolloverAudioURLs, 'rollover_');
    var response_audio_manifest = getUniqueAudioManifest(originalResponseAudioURLs, 'response_');
    var question_audio_manifest = getUniqueAudioManifest(originalQuestionAudioURLs);

    rolloverAudioIds_flat = getAudioIDsList(originalRolloverAudioURLs, 'rollover_');
    responseAudioIds_flat = getAudioIDsList(originalResponseAudioURLs, 'response_');

    // THIS BIT BELOW LOOPS THOUGH ALL OF THE AUDIO IDS AND SEPARATES THEM INTO
    // INDIVIDUAL ARRAYS AND THEN PLACES THEM INTO THE *_arrays 2D VERSION OF
    // rollover AND response AUDIO ID LISTS. THIS IS NECESSARY TO MATCH EACH SET
    // OF rollover AND response ANSWER AUDIO FILES TO THE CORRECT QUESTION LATER
    // i.e.: responseAudioIds_arrays[cq][i]
    var offset = 0;
    for (i = 0; i < numOfQuestions; i++) {
        var arr1 = [];
        var arr2 = [];
        for (ii = 0; ii < word_quiz_data[i].answers.length; ii++) {
            arr1.push(rolloverAudioIds_flat[offset]);
            arr2.push(responseAudioIds_flat[offset]);
            offset++;
        }
        rolloverAudioIds_arrays.push(arr1);
        responseAudioIds_arrays.push(arr2);
    }

    for (i = 0; i < numOfQuestions; i++) {

        imagesIds.push("question_image_" + i);

        if (word_quiz_data[i].image_url_acf == null) {
            images_manifest.push({id: "question_image_" + i, src: image_url + 'missing-image.png'});
        }else{
            images_manifest.push({id: "question_image_" + i, src: word_quiz_data[i].image_url_acf});
        }
    }

    // OK... SO IF YOU ADD THE SAME AUDIO FILE TWICE TO A LOADQUEUE, THAT
    // IS ALSO A PROBLEM. IT DOESN'T MATTER IF THE MANIFESTS ARE ALL UNIQUE
    // IF THE SAME FILE OCCURS IN 2 DIFFERENT MANIFESTS BEING ADDED TO THE SAME
    // QUEUE, THIS IS ALSO A PROBLEM

    preloaded_question_audio_assets = new createjs.LoadQueue();
    preloaded_question_audio_assets.installPlugin(createjs.Sound);
    preloaded_question_audio_assets.on("complete", handleQuestionAudioComplete, this);
    preloaded_question_audio_assets.on("progress", handleFileLoad, this);
    preloaded_question_audio_assets.loadManifest(question_audio_manifest);

    preloaded_rollover_audio_assets = new createjs.LoadQueue();
    preloaded_rollover_audio_assets.installPlugin(createjs.Sound);
    preloaded_rollover_audio_assets.on("complete", handleRolloverAudioComplete, this);
    preloaded_rollover_audio_assets.on("progress", handleFileLoad, this);
    preloaded_rollover_audio_assets.on("fileload", handleFileLoaded, this);

    preloaded_response_audio_assets = new createjs.LoadQueue();
    preloaded_response_audio_assets.installPlugin(createjs.Sound);
    preloaded_response_audio_assets.on("complete", handleResponseAudioComplete, this);
    preloaded_response_audio_assets.on("progress", handleFileLoad, this);
    preloaded_response_audio_assets.on("fileload", handleFileLoaded, this);

    function handleFileLoaded(event) {
        //console.log('event.item:', event.item);
    }

    preloaded_assets = new createjs.LoadQueue();
    preloaded_assets.installPlugin(createjs.Sound);
    preloaded_assets.on("complete", handlePreloadComplete, this);
    preloaded_assets.on("progress", handleFileLoad, this);

    //preloaded_assets.loadManifest(question_audio_manifest);
    //preloaded_assets.loadManifest(rollover_audio_manifest);
    //preloaded_assets.loadManifest(response_audio_manifest);

    function handleFileLoad(event) {
        var loadProgress = Math.round(100 * event.progress);
        //console.log(loadProgress);
        loadProgress = (loadProgress > 100) ? 100 : loadProgress;
        questionField.text = "Loading: " + loadProgress + "%";
        questionField.x = canvas.width / 2;
        stage.update();
    }
    function handleQuestionAudioComplete() {
        preloaded_rollover_audio_assets.loadManifest(rollover_audio_manifest);
        //console.log('question audio loaded');
    }
    function handleRolloverAudioComplete() {
        //console.log('response_audio_manifest:', response_audio_manifest);
        preloaded_response_audio_assets.loadManifest(response_audio_manifest);
        //console.log('rollover audio loaded');
    }
    function handleResponseAudioComplete() {
        preloaded_assets.loadManifest(images_manifest);
        //console.log('response audio loaded');
    }

    function handlePreloadComplete() {
        //console.log('images loaded');

        displayCategoryMenu();

    }

    for (i = 0; i < word_quiz_categories.length; i++) {
        word_quiz_categories[i].complete = false;
    }
}

function displayCategoryMenu() {
    var padding = 15;
    var containerHeight = 0;
    redrawOn = true;

    var catButtonWidth = 280;
    var origX = 0;
    var yIncr = 0;
    var columnsWidth = catButtonWidth;

    var tweenCount = word_quiz_categories.length;
    for (var i = 0; i < word_quiz_categories.length; i++) {

        var categoryName = word_quiz_categories[i].name;
        var categoryID = word_quiz_categories[i].term_id;
        var categoryButton = createButton(catButtonWidth, answerButtonHeight, categoryName);

        answers_container.addChild(categoryButton);

        if ((i+1)%6 == 0) {
            origX += catButtonWidth + padding;
            columnsWidth += catButtonWidth + padding;
            yIncr = 0;
        }

        categoryButton.y = (answerButtonHeight + padding) * yIncr;
        yIncr++;

        categoryButton.x = canvas.width;
        
        categoryButton.alpha = 0;
        createjs.Tween.get(categoryButton).wait(150 * i).to({x: origX}, 500, createjs.Ease.quartInOut);

        createjs.Tween.get(categoryButton).wait(150 * i).to({alpha: 1}, 700).call(
            function(){
                tweenCount--;
                if (tweenCount == 0) {
                    redrawOn = false;
                    stage.update();
                }
            }
        );

        // calculate button container height (containers don't have a height property?!)
        if (i < 5) {
            containerHeight += answerButtonHeight;
            if (i > 0) {
                containerHeight += padding;
            }
        }

        categoryButton.on("mousedown", setCurrentCategory, null, false,
            {
                "selected_category": categoryID,
                "selected_category_name" : categoryName
            }
        );
    }
    answers_container.x = canvas.width / 2 - columnsWidth / 2;
    answers_container.y = (canvas.height / 2 - containerHeight / 2) + 40;
    questionField.text = "Please choose the word category you would like to be quizzed on:"
}

function setCurrentCategory(event, data) {
    answers_container.removeAllChildren();
    currentCategoryID = data.selected_category;
    currentCategoryName = data.selected_category_name;
    getCurrentQuestionSet();
}

function getCurrentQuestionSet() {

    if (currentQuestionSet.length > 0) {
        currentQuestionSet = [];
        currentCatQuestionAudioIds = [];
        currentCatRolloverAudioIds_arrays = [];
        currentCatResponseAudioIds_arrays = [];
        currentCatImagesIds = [];
    }

    for (var i = 0; i < word_quiz_data.length; i++) {
        var cat_arr = word_quiz_data[i].category;
        for (var ii = 0; ii < cat_arr.length; ii++) {
            if (cat_arr[ii].term_id == currentCategoryID) {
                currentQuestionSet.push(word_quiz_data[i]);
                currentCatQuestionAudioIds.push(questionAudioIds[i]);
                currentCatRolloverAudioIds_arrays.push(rolloverAudioIds_arrays[i]);
                currentCatResponseAudioIds_arrays.push(responseAudioIds_arrays[i]);
                currentCatImagesIds.push(imagesIds[i]);
                break;
            }
        }
    }

    askQuestion(currentQuestion);
}


function askQuestion(cq) {

    questionField.text = currentQuestionSet[cq].question_text;
    questionField.x = canvas.width / 2;
    placeImage(cq);
    createjs.Sound.play(currentCatQuestionAudioIds[cq],
        {
            interrupt: createjs.Sound.INTERRUPT_NONE,
            loop: 0,
            volume: 1
        }
    );
    questionProgressField.text = currentCategoryName + ': Question ' + (cq+1) + ' of ' + currentQuestionSet.length;
    questionProgressField.x = canvas.width / 2;
}
function placeImage(cq) {
    question_image_bitmap = new createjs.Bitmap(preloaded_assets.getResult(currentCatImagesIds[cq]));
    question_image_bitmap.shadow = new createjs.Shadow('#000000', 4, 4, 4);
    question_image_bitmap.x = 190 + question_image_bitmap.image.width / 2;
    question_image_bitmap.y = (canvas.height / 2 - question_image_bitmap.image.height / 2) + question_image_bitmap.image.height / 2;
    stage.addChild(question_image_bitmap);

    question_image_bitmap.alpha = 0;
    question_image_bitmap.scaleX = 1.4;
    question_image_bitmap.scaleY = 1.4;
    question_image_bitmap.regX = question_image_bitmap.image.width / 2;
    question_image_bitmap.regY = question_image_bitmap.image.height / 2;

    redrawOn = true;
    createjs.Tween.get(question_image_bitmap).to({scaleX: 1, scaleY: 1, alpha: 1}, 500, createjs.Ease.getPowIn(2.2));

    placeAnswers(cq);
}
function placeAnswers(cq) {

    var answersPadding = 15;
    var containerHeight = 0;

    var tweenCount = currentQuestionSet[cq].answers.length;

    for (var i = 0; i < currentQuestionSet[cq].answers.length; i++) {

        var answer_word = currentQuestionSet[cq].answers[i].answer_word;
        var answerButton = createButton(answerButtonWidth, answerButtonHeight, answer_word);

        answers_container.addChild(answerButton);

        answerButton.y = (answerButtonHeight + answersPadding) * i;

        var origX = answerButton.x;
        answerButton.x = canvas.width;
        answerButton.alpha = 0;
        createjs.Tween.get(answerButton).wait(150 * i).to({x: origX}, 500, createjs.Ease.quartInOut);
        createjs.Tween.get(answerButton).wait(150 * i).to({alpha: 1}, 700).call(
            function(){
                tweenCount--;
                if(tweenCount == 0) {
                    redrawOn = false;
                }
            }
        );

        // calculate button container height (containers don't have a height property?!)
        containerHeight += (answerButtonHeight);
        if (i > 0) {
            containerHeight += answersPadding;
        }

        answerButton.on("mousedown", answerQuestion, null, false,
            {
                "response_audio": currentCatResponseAudioIds_arrays[cq][i],
                "response_text": currentQuestionSet[cq].answers[i].response_text,
                "correct": currentQuestionSet[cq].answers[i].the_correct_answer,
                "answerButton": answerButton
            }
        );
        // console.log('rolloverAudioIds_arrays[cq][i]:', rolloverAudioIds_arrays[cq][i]);
        answerButton.on("mouseover", answerOver, null, false,
            {
                "rollover_audio": currentCatRolloverAudioIds_arrays[cq][i]
            }
        );
    }
    answers_container.x = canvas.width / 1.7;
    answers_container.y = canvas.height / 2 - containerHeight / 2;
}

var response_audio;
function answerQuestion(event, data) {

    redrawOn = true;

    // UPDATING SESSION TO SHOW GAME IS IN PROGRESS
    // BUT NOT COMPLETE
    if (saveInit) {
        saveInit = false;
        if (game_status != 'in progress' && game_status != 'complete') {
            saveSession('in progress');
        }
    }

    if (response_audio) {
        response_audio.stop();
    }
    response_audio = createjs.Sound.play(data.response_audio,
        {
            interrupt: createjs.Sound.INTERRUPT_NONE,
            loop: 0,
            pan: -0.7,
            volume: 1,
            delay: 500
        }
    );
    responseField.text = data.response_text;
    responseField.x = canvas.width / 2;

    responseField.alpha = 0;

    if (data.correct == 'correct') {

        responseField.color = '#66ff33';

        createjs.Tween.get(responseBar, {override:true}).to({y:canvas.height-50}, 300);
        createjs.Tween.get(responseField, {override: true}).to({alpha: 1}, 500);

        removeWrongAnswers(data.answerButton);

        nextButton = createButton(nextButtonWidth, nextButtonHeight, "Next");
        nextButton.x = canvas.width - nextButtonWidth - 20;
        nextButton.y = canvas.height - nextButtonHeight - 60;
        stage.addChild(nextButton);
        nextButton.on('mousedown', startNextQuestion, null, false, {
            "data": ""
        });
    } else {
        responseField.color = 'red';
        createjs.Tween.get(responseBar, {override:true}).to({y:canvas.height-50}, 300).wait(3300).to({y: canvas.height}, 300);
        createjs.Tween.get(responseField, {override: true}).to({alpha: 1}, 500).wait(3000).to({alpha: 0}, 500).call(
            function(){
                redrawOn = false;
            }
        );
    }
}

var rolloversound;
function answerOver(event, data) {
    if (rolloversound) {
        rolloversound.stop();
    }
    //console.log('data.rollover_audio:', data.rollover_audio);
    rolloversound = createjs.Sound.play(data.rollover_audio,
        {
            interrupt: createjs.Sound.INTERRUPT_NONE,
            loop: 0,
            volume: 1,
            pan: 0.7
        }
    )
}
function removeWrongAnswers(buttonToKeep) {
    var removedCount = 0;
    for (var i = 0; i < answers_container.numChildren; i++) {
        var child = answers_container.children[i];
        if (child != buttonToKeep) {
            child.mouseEnabled = false;
            createjs.Tween.get(child).wait(150 * removedCount).to({alpha: 0}, 500);
            removedCount++;
        }
    }
    buttonToKeep.mouseEnabled = false;
}
function startNextQuestion(event, data) {
    createjs.Tween.get(responseBar).to({y: canvas.height}, 300);
    responseField.color = 'white';
    answers_container.removeAllChildren();
    stage.removeChild(question_image_bitmap);
    stage.removeChild(nextButton);
    responseField.text = "";
    currentQuestion++;

    if (currentQuestion == currentQuestionSet.length) {

        questionProgressField.text = "";

        for (var i = 0; i < word_quiz_categories.length; i++) {
            if (word_quiz_categories[i].term_id == currentCategoryID) {
                if (!word_quiz_categories[i].complete) word_quiz_categories[i].complete = true;
                var categoryName = word_quiz_categories[i].name;
                break;
            }
        }

        dialogbox("You have completed the quiz for the category: " + categoryName + ". Good job!", restartQuiz);

        for (i = 0; i < word_quiz_categories.length; i++) {
            if (!word_quiz_categories[i].complete) return;
        }

        if (game_status != 'complete') {
            saveSession('complete');
            game_status = 'complete';
        }

    } else {
        askQuestion(currentQuestion);
    }
}
function restartQuiz() {
    currentQuestion = 0;
    displayCategoryMenu();
}
init();