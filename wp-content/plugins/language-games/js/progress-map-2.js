(function () {

    // bring in path to image folder for progress map that was assigned in php file using wp_localize_script
    var image_path = image_url.path;

    var $progressMap = $('.progress-map');

    // create canvas tag as we only want it here when this script runs
    // this is a product of my lazily using page-plugin.twig instead of proper
    // sensei template overrides ;)

    $progressMap.append('<p>&nbsp;</p><canvas id="gamezCanvas" class="gamezCanvas" width=2000 height=800></canvas>');
    // create reference to new canvas tag and initialize it as hidden and out of the way
    // of download progress display
    var $gamez_canvas = $("#gamezCanvas");
    $gamez_canvas.css("height", 0);

    // $gamez_canvas.css("width", "100%");
    $gamez_canvas.css("opacity", 0);

    // IE stuff here?

    // Create stage object with createjs. Need a real element here so not using
    // $gamez_canvas as that is a jquery object
    var stage = new createjs.Stage(document.querySelector("#gamezCanvas"));

    createjs.Ticker.setFPS(60);
    createjs.Ticker.addEventListener("tick", stage);

    stage.enableMouseOver(60);
    stage.mouseMoveOutside = true; // keep tracking the mouse even when it leaves the $gamez_canvas

    // create a list of image files to preload so we can start animations and set positions
    // of graphics after they are downloaded to browser
    var imageManifest = [
        {
            id: 'background',
            src: image_path + 'progress-map-2/background.png'
        },
        {
            id: 'dude',
            src: image_path + 'progress-map-2/dude.png'
        },
        {
            id: 'star',
            src: image_path + 'progress-map-2/star.png'
        },
        {
            id: 'check',
            src: image_path + 'progress-map-2/checkbox.png'
        }
    ];

    // Value to offset the vertical position of the map
    var mapYoffset = 300;

    // Check positions
    var moduleCheckPositions = [
        {
            id: 0,
            checkXPos: 288,
            checkYPos: 415,
            lessons: 3,
            starXPos: 246,
            starYPos: 563
        },
        {
            id: 1,
            checkXPos: 98,
            checkYPos: 712,
            lessons: 3,
            starXPos: 257,
            starYPos: 877
        },
        {
            id: 2,
            checkXPos: 339,
            checkYPos: 911,
            lessons: 1,
            starXPos: 487,
            starYPos: 1032
        },
        {
            id: 3,
            checkXPos: 643,
            checkYPos: 811,
            lessons: 3,
            starXPos: 737,
            starYPos: 1029
        },
        {
            id: 4,
            checkXPos: 968,
            checkYPos: 883,
            lessons: 4,
            starXPos: 1243,
            starYPos: 1006
        },
        {
            id: 5,
            checkXPos: 1236,
            checkYPos: 695,
            lessons: 2,
            starXPos: 1423,
            starYPos: 840
        },
        {
            id: 6,
            checkXPos: 1486,
            checkYPos: 505,
            lessons: 1,
            starXPos: 1544,
            starYPos: 671
        }
    ];

    var dudePositions = [
        {
            xPos: 65,
            yPos: 550,
            path: [
                {
                    xPos: 0,
                    yPos: 0
                }
            ]

        },
        {
            xPos: 0,
            yPos: 0,
            path: [
                {
                    xPos: 0,
                    yPos: 0
                }
            ]

        },
        {
            xPos: 0,
            yPos: 0,
            path: [
                {
                    xPos: 0,
                    yPos: 0
                }
            ]

        },
        {
            xPos: 0,
            yPos: 0,
            path: [
                {
                    xPos: 0,
                    yPos: 0
                }
            ]

        },
        {
            xPos: 0,
            yPos: 0,
            path: [
                {
                    xPos: 0,
                    yPos: 0
                }
            ]

        },
        {
            xPos: 0,
            yPos: 0,
            path: [
                {
                    xPos: 0,
                    yPos: 0
                }
            ]

        },
        {
            xPos: 0,
            yPos: 0,
            path: [
                {
                    xPos: 0,
                    yPos: 0
                }
            ]

        },
        {
            xPos: 0,
            yPos: 0,
            path: [
                {
                    xPos: 0,
                    yPos: 0
                }
            ]

        }

    ];

    // this will hold what ever position dude needs to appear at
    var coords;

    // manage functions to run during during and after file downloads
    var imageQueue = new createjs.LoadQueue();
    imageQueue.on("complete", init);
    imageQueue.on("progress", loadProgress);
    imageQueue.loadManifest(imageManifest);

    // display download progress of above download list.
    function loadProgress(e) {
        var loadingText = "Loading Progress Map: " + Math.round(e.progress * 100) + '%';
        $('#progress-map-loading').html('<h3>' + loadingText + '</h3>');
    }

    // container for status of course completion
    var courseCompleted = false;

    // Stuff for those birds...
    var numOfBirds = 3;
    var birdScale = .3;
    var birdHomeCount = 0;
    var birdsArr = [];

    // initialization function to run after all assets have been pre-loaded
    function init() {

        // Attach bottom most layer of progress map
        var background = new createjs.Bitmap(imageQueue.getResult('background'));

        background.y = -mapYoffset;
        stage.addChild(background);

        // hide download percentage display
        $('#progress-map-loading').css('display', 'none');

        // bring height of map back up to size as it no longer needs to be out of the way of the
        // download progress, then fade it in.
        $gamez_canvas.css("height", "auto");
        $gamez_canvas.animate({
            opacity: 1
        }, 1000);

        // Place module stars
        var stars = []; // store stars for later reference when making them appear
        moduleCheckPositions.forEach(function (moduleCheckPosition) {
            var star = new createjs.Bitmap(imageQueue.getResult('star'));
            stars.push(star);
            star.name = 'star_' + i;
            star.regX = 38;
            star.regY = 37;
            star.x = moduleCheckPosition.starXPos;
            star.y = moduleCheckPosition.starYPos - 300;
            star.scale = 10;
            star.alpha = 0;
            stage.addChildAt(star, 1);
        });

        // Get count of IDs of completed modules and store in variable
        var completedModulesCount = progress_data[0].length;

        // Number of completed lessons (temp all)
        var completedLessonsCount = 0;

        // find how many lessons are completed and if all lessons have been completed
        progress_data[1].forEach(function (lesson) {
            if (lesson.completed === true) {
                completedLessonsCount++;
                if ( completedLessonsCount == progress_data[1].length ) {
                    courseCompleted = true;
                }
            }
        });

        // which module posisition to use
        var modulePos = 0;

        // which lesson in a module (in the loop below) are we placing a check for (start at 1)
        var lessonCount = 1;

        // how far to offset subsequent checkmarks vertically
        var yOffset = 22;

        for (var i = 0; i < completedLessonsCount; i++) {

            var checkmark = new createjs.Bitmap(imageQueue.getResult('check'));
            checkmark.name = 'check_' + i;
            checkmark.regX = 5;
            checkmark.regY = 5;
            checkmark.x = moduleCheckPositions[modulePos].checkXPos;
            checkmark.y = moduleCheckPositions[modulePos].checkYPos + ( (lessonCount - 1) * yOffset) - 300;
            checkmark.alpha = 0;
            checkmark.scale = 10;
            stage.addChild(checkmark);

            createjs.Tween.get(checkmark).wait((i + 1) * 500).to({alpha: 1}, 500);
            createjs.Tween.get(checkmark).wait((i + 1) * 500).to({scale: 1}, 500);

            // If the number of completed lessons so far in the loop equal the number of lessons
            // in this module (moduleCheckPositions[modulePos]), level up and place star
            if (lessonCount == moduleCheckPositions[modulePos].lessons) {

                // check to see if this is the last MODULE COMPLETED. If so, then place dude
                if (modulePos + 1 == completedModulesCount) {

                    coords = {
                        xPos: moduleCheckPositions[modulePos].starXPos,
                        yPos: moduleCheckPositions[modulePos].starYPos
                    };
                    createjs.Tween.get(stars[modulePos]).wait((i + 1) * 500).to({alpha: 1}, 500);
                    createjs.Tween.get(stars[modulePos]).wait((i + 1) * 500).to({scale: 1}, 500).call(placeDude);

                } else {
                    createjs.Tween.get(stars[modulePos]).wait((i + 1) * 500).to({alpha: 1}, 500);
                    createjs.Tween.get(stars[modulePos]).wait((i + 1) * 500).to({scale: 1}, 500);
                }

                // up increment the current module to refer to for coords and reset lesson count
                // for each module
                modulePos++;
                lessonCount = 1;
            } else {
                // we still havent reached the last lesson in this module so increment up the lesson
                lessonCount++;
            }

        }

        // have we not completed any modules yet? place dude at start
        if (completedModulesCount == 0) {
            coords = {
                xPos: moduleCheckPositions[0].starXPos - 170,
                yPos: moduleCheckPositions[0].starYPos
            };
            placeDude();
        }

        // Those birds...
        var spriteSheet;
        var animation;
        $.getJSON(image_path + 'progress-map/birdsprite_data.json', function(json) {
            spriteSheet = new createjs.SpriteSheet(json);
            animation = new createjs.Sprite(spriteSheet, "bird-is-the-word");
            animation.scaleX = birdScale;
            animation.scaleY = birdScale;
            startBirds(animation);
        });

    }

    function placeDude() {
        // The dude is added
        var dude = new createjs.Bitmap(imageQueue.getResult('dude'));
        dude.regX = 43;
        dude.regY = 210;

        // The dude is placed in appropriate place
        dude.x = coords.xPos;
        dude.y = coords.yPos - mapYoffset;
        stage.addChildAt(dude, stage.numChildren);

        // is the course complete? if so, show course completion text message
        if (courseCompleted) {
            displayCourseCompletion();
        }
    }

    function displayCourseCompletion() {

        var textContainer = new createjs.Container();
        var congratsText1 = new createjs.Text();
        var congratsText2 = new createjs.Text();
        var congratsText3 = new createjs.Text();

        congratsText1.set({
            text: "Congratulations!",
            font: "bold 52px Arial",
            color: "#01a2a6",
            textAlign: "center",
            y: 0
        });
        congratsText2.set({
            text: "Online Pathways Program 2.0",
            font: "bold 42px Arial",
            color: "#01a2a6",
            textAlign: "center",
            y: 70
        });
        congratsText3.set({
            text: "Completed!",
            font: "bold 72px Arial",
            color: "#01a2a6",
            textAlign: "center",
            y: 120
        });

        textContainer.addChild(congratsText1, congratsText2, congratsText3);
        stage.addChildAt(textContainer, stage.numChildren);
        var containerBounds = textContainer.getBounds();

        textContainer.set({
            shadow: new createjs.Shadow("#000000", 5, 5, 5),
            regY: containerBounds.height / 2,
            x: (1000 - (containerBounds.width / 2)) + (-containerBounds.x),
            y: 400 - (containerBounds.height / 2),
            scale: 10,
            alpha: 0
        });
        textContainer.cache(-(containerBounds.width / 2), 0, containerBounds.width, containerBounds.height, 10);
        createjs.Tween.get(textContainer).to({alpha: 1}, 500);
        createjs.Tween.get(textContainer).to({scale: 2}, 1000);
    }

    // Mess of functions below just for those birds...
    var canvas = document.querySelector("#gamezCanvas");
    function startBirds(animation) {

        var bluebird = new createjs.ColorMatrixFilter([
            0.30,0.30,0.30,0,0, // red component
            0.30,0.30,0.30,0,0, // green component
            0.8,0.8,0.8,0,0, // blue component
            0,0,0,1,0  // alpha
        ]);
        var greenbird = new createjs.ColorMatrixFilter([
            0.4,0.4,0.4,0,0, // red component
            0.4,0.4,0.4,0,0, // green component
            0.10,0.10,0.10,0,0, // blue component
            0,0,0,1,0  // alpha
        ]);
        var redbird = new createjs.ColorFilter(1,1,1,1,0,0,0,0);

        var filtersArr = [bluebird,greenbird,redbird];

        var startY = Math.round(100 + (Math.random() * (canvas.height - 200)));
        for (var i = 0; i < numOfBirds; i++) {
            var bird = animation.clone();
            bird.y = startY;
            bird.x = -100;

            var filterIndex = (i%filtersArr.length);

            bird.filters = [
                filtersArr[filterIndex]
            ];
            bird.cache(0, 0, 150, 150);

            stage.addChild(bird);
            birdsArr.push(bird);
            animateBird(bird);
        }
    }

    function restartBirds() {
        var startY = Math.round(100 + (Math.random() * (canvas.height - 200)));
        for (var i = 0; i < birdsArr.length; i++) {
            birdsArr[i].y = startY;
            animateBird(birdsArr[i]);
        }
    }

    function animateBird(bird) {

        var handleChangeArgs = {
            'bird': bird,
            'offsetY': bird.y,
            'count': 0,
            'freq': Math.round(25 + Math.random() * 5),
            'depth': Math.round(10 + Math.random() * 20)
        };

        var destination;

        if (bird.x > 0) {
            bird.direction = 'left';
            bird.scaleX = -birdScale;
            destination = {
                'x': -100
            }
        } else {
            bird.direction = 'right';
            bird.scaleX = birdScale;
            destination = {
                'x': canvas.width + 100
            }
        }

        createjs.Tween.get(bird)
            .wait((Math.random() * 4000)+4000)
            .to(destination, 4000 * Math.random() + 3000)
            .call(countBirds)
            .on('change', oscillateY , null, false, handleChangeArgs);

        function countBirds() {
            birdHomeCount++;
            if (birdHomeCount == birdsArr.length) {
                birdHomeCount = 0;
                restartBirds();
            }
        }
    }

    function oscillateY(event, data) {
        data.bird.y = data.offsetY + (data.depth * Math.sin(data.count++ / data.freq));
        data.bird.updateCache();
    }

})();