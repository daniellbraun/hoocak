(function () {
    var image_path = image_url.path;
    var $progressMap = $('.progress-map');

    $progressMap.addClass("row column");

    $progressMap.append('<p>&nbsp;</p><canvas id="gamezCanvas" class="gamezCanvas" width=1103 height=763></canvas>');

    var gamez_canvas = $("#gamezCanvas");

    gamez_canvas.css("height", "0");

    var isIE10 = false;
    /*@cc_on
     if (/^10/.test(@_jscript_version) || /^9/.test(@_jscript_version)) {
     isIE10 = true;
     }
     @*/

    if (isIE10) {
        gamez_canvas.css("width", "1103");
        gamez_canvas.css("height", "763");
    }

    var stage;
    var canvas;
    var c = createjs;

    canvas = document.getElementById("gamezCanvas");
    stage = new c.Stage(canvas);

    c.Ticker.setFPS(60);
    c.Ticker.addEventListener("tick", stage);

    stage.enableMouseOver(60);
    stage.mouseMoveOutside = true; // keep tracking the mouse even when it leaves the canvas

    var unitImageManifest = [
        {
            id: 'sky',
            src: image_path + 'progress-map/sky.png'
        },
        {
            id: 'cloud1',
            src: image_path + 'progress-map/cloud1.png'
        },
        {
            id: 'cloud2',
            src: image_path + 'progress-map/cloud2.png'
        },
        {
            id: 'cloud3',
            src: image_path + 'progress-map/cloud3.png'
        },
        {
            id: 'cloud4',
            src: image_path + 'progress-map/cloud4.png'
        },
        {
            id: 'background',
            src: image_path + 'progress-map/background.png'
        },
        {
            id: 'checkmark_bw',
            src: image_path + 'progress-map/checkmark_bw.png'
        },
        {
            id: 'checkmark_complete',
            src: image_path + 'progress-map/checkmark.png'
        },
        {
            id: 'unit-1',
            src: image_path + 'progress-map/unit-1.png'
        },
        {
            id: 'unit-2',
            src: image_path + 'progress-map/unit-2.png'
        },
        {
            id: 'unit-3',
            src: image_path + 'progress-map/unit-3.png'
        },
        {
            id: 'unit-4',
            src: image_path + 'progress-map/unit-4.png'
        },
        {
            id: 'unit-5',
            src: image_path + 'progress-map/unit-5.png'
        },
        {
            id: 'unit-6',
            src: image_path + 'progress-map/unit-6.png'
        },
        {
            id: 'unit-7',
            src: image_path + 'progress-map/unit-7.png'
        },
        {
            id: 'unit-8',
            src: image_path + 'progress-map/unit-8.png'
        },
        {
            id: 'tree1_1',
            src: image_path + 'progress-map/tree1_1.png'
        },
        {
            id: 'tree1_2',
            src: image_path + 'progress-map/tree1_2.png'
        },
        {
            id: 'tree1_3',
            src: image_path + 'progress-map/tree1_3.png'
        },
        {
            id: 'tree2_1',
            src: image_path + 'progress-map/tree2_1.png'
        },
        {
            id: 'tree2_2',
            src: image_path + 'progress-map/tree2_2.png'
        },
        {
            id: 'tree2_3',
            src: image_path + 'progress-map/tree2_3.png'
        },
        {
            id: 'tree3_1',
            src: image_path + 'progress-map/tree3_1.png'
        },
        {
            id: 'tree3_2',
            src: image_path + 'progress-map/tree3_2.png'
        },
        {
            id: 'tree3_3',
            src: image_path + 'progress-map/tree3_3.png'
        },
        {
            id: 'tree4_1',
            src: image_path + 'progress-map/tree4_1.png'
        },
        {
            id: 'tree4_2',
            src: image_path + 'progress-map/tree4_2.png'
        },
        {
            id: 'tree4_3',
            src: image_path + 'progress-map/tree4_3.png'
        }
    ];
    var units = [
        {
            point: {x: 269, y: 93}
        },
        {
            point: {x: 494, y: 171}
        },
        {
            point: {x: 314, y: 328}
        },
        {
            point: {x: 118, y: 657}
        },
        {
            point: {x: 359, y: 649}
        },
        {
            point: {x: 567, y: 465}
        },
        {
            point: {x: 722, y: 670}
        },
        {
            point: {x: 926, y: 452}
        }
    ];
    // var progressField = new c.Text("Loading Progress Map: ...", "bold 16px Arial", "#000");
    // progressField.textAlign = 'center';
    // progressField.x = canvas.width / 2;
    // stage.addChild(progressField);
    var imageQueue = new c.LoadQueue();
    imageQueue.on("complete", init);
    imageQueue.on("progress", loadProgress);
    imageQueue.loadManifest(unitImageManifest);


    var signWidth = 100;

    var popupContainer;
    var popupField;
    var popupFieldLineWidth = 280;

    var blowingLeaves = [];

    var coverFade = new c.Shape();
    coverFade.graphics.beginFill('#FFF');
    coverFade.graphics.drawRect(0, 0, canvas.width, canvas.height);

    var numOfBirds = 3;
    var birdScale = .3;
    var birdHomeCount = 0;
    var birdsArr = [];

    function loadProgress(e) {
        var loadingText = "Loading Progress Map: " + Math.round(e.progress*100) + '%';
        $('#progress-map-loading').html('<h3>' +loadingText+ '</h3>');
    }

    function init() {

        $('#progress-map-loading').css('display', 'none');
        //gamez_canvas.css("height", "763");

        gamez_canvas.animate({
            height: 763
        }, 1000);

        var spriteSheet;
        var animation;
        $.getJSON(image_path + 'progress-map/birdsprite_data.json', function(json) {
            spriteSheet = new createjs.SpriteSheet(json);
            animation = new createjs.Sprite(spriteSheet, "bird-is-the-word");
            animation.scaleX = birdScale;
            animation.scaleY = birdScale;
            startBirds(animation);
        });

        var module_data = progress_data[0];

        var sky = new c.Bitmap(imageQueue.getResult('sky'));
        stage.addChild(sky);

        var background = new c.Bitmap(imageQueue.getResult('background'));
        stage.addChild(background);

        for (i = 0; i < module_data.length; i++) {
            var layer = new c.Bitmap(imageQueue.getResult('unit-' + (i + 1)));
            stage.addChild(layer);
            layer.alpha = 0;
            layer.name = 'layer_' + i;
        }

        var lastOne = 'layer_' + (module_data.length - 1);
        setTimeout(function () {
            for (i = 0; i < module_data.length; i++) {
                c.Tween.get(layer).wait(i * 500).to({alpha: 1}, 500).wait(1000).call(fadeout);
            }
        }, 1000);
        function fadeout() {

            if (this.name != lastOne) {
                c.Tween.get(this).to({alpha: 0}, 500);
            }
        }

        // combine module IDs to units array
        var all_units = progress_data[2];

        for (var i = 0; i < all_units.length; i++) {
            units[i].id = all_units[i];
            units[i].lessons = [];
        }

        popupContainer = new c.Container();
        var g = new c.Graphics();
        g.beginStroke("#000000");
        g.beginFill("rgba(0,0,0,0.7)");
        g.drawRoundRect(0, 0, 300, 100, 10);
        var popupBackground = new c.Shape(g);
        popupBackground.name = 'background';
        popupBackground.setBounds(0, 0, 300, 100);
        popupField = new c.Text("", "bold 16px Arial", "#FFFFFF");
        popupField.lineWidth = popupFieldLineWidth;
        popupField.textBaseline = "top";
        popupField.y = 0;
        popupContainer.addChild(popupBackground);
        popupContainer.addChild(popupField);

        addLessonBlanks();
        clouds();
        trees();
    }

    function startBirds(animation) {

        var bluebird = new createjs.ColorMatrixFilter([
            0.30,0.30,0.30,0,0, // red component
            0.30,0.30,0.30,0,0, // green component
            0.8,0.8,0.8,0,0, // blue component
            0,0,0,1,0  // alpha
        ]);
        var greenbird = new createjs.ColorMatrixFilter([
            0.4,0.4,0.4,0,0, // red component
            0.4,0.4,0.4,0,0, // green component
            0.10,0.10,0.10,0,0, // blue component
            0,0,0,1,0  // alpha
        ]);
        var redbird = new createjs.ColorFilter(1,1,1,1,0,0,0,0);

        var filtersArr = [bluebird,greenbird,redbird];

        var startY = Math.round(100 + (Math.random() * (canvas.height - 200)));
        for (var i = 0; i < numOfBirds; i++) {
            var bird = animation.clone();
            bird.y = startY;
            bird.x = -100;

            var filterIndex = (i%filtersArr.length);

            bird.filters = [
                filtersArr[filterIndex]
            ];
            bird.cache(0, 0, 150, 150);

            stage.addChild(bird);
            birdsArr.push(bird);
            animateBird(bird);
        }
    }

    function restartBirds() {
        var startY = Math.round(100 + (Math.random() * (canvas.height - 200)));
        for (var i = 0; i < birdsArr.length; i++) {
            birdsArr[i].y = startY;
            animateBird(birdsArr[i]);
        }
    }

    function animateBird(bird) {

        var handleChangeArgs = {
            'bird': bird,
            'offsetY': bird.y,
            'count': 0,
            'freq': Math.round(25 + Math.random() * 5),
            'depth': Math.round(10 + Math.random() * 20)
        };

        var destination;

        if (bird.x > 0) {
            bird.direction = 'left';
            bird.scaleX = -birdScale;
            destination = {
                'x': -100
            }
        } else {
            bird.direction = 'right';
            bird.scaleX = birdScale;
            destination = {
                'x': canvas.width + 100
            }
        }

        createjs.Tween.get(bird)
            .wait((Math.random() * 4000)+4000)
            .to(destination, 4000 * Math.random() + 3000)
            .call(countBirds)
            .on('change', oscillateY , null, false, handleChangeArgs);

        function countBirds() {
            birdHomeCount++;
            if (birdHomeCount == birdsArr.length) {
                birdHomeCount = 0;
                restartBirds();
            }
        }
    }

    function oscillateY(event, data) {
        data.bird.y = data.offsetY + (data.depth * Math.sin(data.count++ / data.freq));
        data.bird.updateCache();
    }

    function addLessonBlanks() {
        var all_lessons = progress_data[1];

        // populate each unit's lesson array with lessons of that unit and their respective status
        // ...check out array.map() .. its cool!
        for (var i = 0; i < all_lessons.length; i++) {
            var module_id = all_lessons[i].module;
            var pos = units.map(
                function (unit_obj) {
                    return unit_obj.id;
                }
            ).indexOf(module_id);
            units[pos].lessons.push(all_lessons[i]);
        }

        for (i = 0; i < units.length; i++) {
            var yIncr = 0;
            var xIncr = 0;
            var checksContainer = new c.Container();

            if (units[i].lessons == undefined) {
                break;
            }

            for (var j = 0; j < units[i].lessons.length; j++) {

                var check;
                if (units[i].lessons[j].completed) {
                    check = new c.Bitmap(imageQueue.getResult('checkmark_complete'));
                } else {
                    check = new c.Bitmap(imageQueue.getResult('checkmark_bw'));
                    check.alpha = .7;
                }

                check.setBounds(
                    0,
                    0,
                    check.image.width,
                    check.image.height
                );

                check.x = xIncr;
                check.y = yIncr * check.image.height;
                check.cursor = 'pointer';

                var circShape = new c.Shape();
                circShape.name = 'circ_shape_' + i + '_' + j;
                circShape.x = xIncr;
                circShape.y = yIncr * check.image.height;
                circShape.graphics.setStrokeStyle(2);
                circShape.graphics.beginStroke("#FFFFFF");
                circShape.graphics.drawCircle(15, 15, 12);
                circShape.alpha = 0;

                yIncr++;
                if ((j + 1) % 5 == 0) {
                    yIncr = 0;
                    xIncr += check.image.width;
                }
                checksContainer.addChild(check);
                checksContainer.addChild(circShape);

                check.lessonTitle = 'Lesson ' + (j+1) + ': ' + units[i].lessons[j].lesson.post_title;
                check.thisCircShape = circShape;
                check.post_slug = units[i].lessons[j].lesson.post_name;
                check.on("mouseover", function () {
                    popupInfo(this.lessonTitle, this.thisCircShape);
                });
                check.on("mouseout", function () {
                    removePopup(this.thisCircShape);
                });
                check.on("click", function () {
                    window.location = '/lesson/' + this.post_slug;
                });
            }
            checksContainer.x = units[i].point.x - (checksContainer.getBounds().width + (signWidth / 2));
            checksContainer.y = units[i].point.y - checksContainer.getBounds().height / 2;

            stage.addChild(checksContainer);
        }
    }

    function popupInfo(msg, circShape) {

        var background = popupContainer.getChildByName('background');
        var g = background.graphics;

        g.clear();
        g.beginStroke("#000000");
        g.beginFill("rgba(0,0,0,0.7)");

        popupField.text = msg;
        g.drawRoundRect(0, 0, popupField.getBounds().width + 40, popupField.getMeasuredHeight() + 20, 10);
        background.setBounds(0, 0, popupField.getMeasuredWidth() + 20, popupField.getMeasuredHeight() + 20);
        popupField.x = 20;
        popupField.y = background.getBounds().height / 2 - popupField.getMeasuredHeight() / 2;
        popupContainer.alpha = 0;

        if (popupFieldLineWidth + stage.mouseX > canvas.width) {
            popupContainer.x = stage.mouseX - (popupField.getBounds().width + 40);
        } else {
            popupContainer.x = stage.mouseX;
        }

        if (popupContainer.getBounds().height + stage.mouseY > canvas.height) {
            popupContainer.y = stage.mouseY - popupContainer.getBounds().height;
        } else {
            popupContainer.y = stage.mouseY;
        }

        stage.addChild(popupContainer);
        var popupTween = c.Tween.get(popupContainer, {override: true})
            .to({'alpha': 1}, 300);
        circShape.alpha = 1;
    }

    function removePopup(circShape) {
        stage.removeChild(popupContainer);
        circShape.alpha = 0;
    }

    function pad(num, size) {
        var s = num + "";
        while (s.length < size) s = "0" + s;
        return s;
    }

    function clouds() {

        firstClouds();

        var numOfClouds = 20;

        for (var i = 0; i < numOfClouds; i++) {
            var randCloudNum = Math.round(Math.random() * 3) + 1;
            var cloud = new c.Bitmap(imageQueue.getResult('cloud' + randCloudNum));
            cloud.name = 'cloud_' + i;
            stage.addChildAt(cloud, 1);
            var maxWidth = cloud.image.width * 1.5;

            var props = returnCloudProps();
            cloud.x = canvas.width;
            cloud.y = props.startY;

            cloud.alpha = props.randAlpha;
            cloud.duration = props.duration;

            cloud.scaleY = props.scale;
            cloud.scaleX = props.scale;

            cloud.cloudTween = c.Tween.get(cloud, {loop: true}).wait(i * 5000).to({x: -maxWidth}, cloud.duration).call(
                function () {
                    var props = returnCloudProps();
                    this.y = props.startY;
                    this.scaleX = 1;
                    this.scaleY = 1;
                    this.scaleX = props.scale;
                    this.scaleY = props.scale;
                    this.alpha = props.randAlpha;
                }
            );
        }
    }

    function firstClouds() {
        var numOfClouds = 10;

        for (var i = 0; i < numOfClouds; i++) {
            var randCloudNum = Math.round(Math.random() * 3) + 1;
            var cloud = new c.Bitmap(imageQueue.getResult('cloud' + randCloudNum));

            cloud.name = 'cloud_' + i;
            stage.addChildAt(cloud, 1);
            var maxWidth = cloud.image.width * 1.5;

            var props = returnCloudProps();
            cloud.x = Math.random() * canvas.width;
            cloud.y = props.startY;

            cloud.alpha = props.randAlpha;
            cloud.duration = props.duration;

            cloud.scaleX = props.scale;
            cloud.scaleY = props.scale;

            var destination = (cloud.x - canvas.width) - maxWidth;

            cloud.cloudTween = c.Tween.get(cloud).to({x: destination}, cloud.duration);
        }
    }

    function returnCloudProps() {
        var randAlpha = Math.random() + .1;
        randAlpha = (randAlpha > 1) ? 1 : randAlpha;
        var randY = Math.random();
        if (randY < .5) {
            randY = -randY;
        }
        return {
            duration: (Math.random() * 60000) + 60000,
            scale: Math.random() + .5,
            randAlpha: randAlpha,
            startY: randY * 100
        }
    }

    function trees() {
        var numOfTrees = 4;
        var numOfLeaves = 3;
        for (var i = 0; i < numOfTrees; i++) {
            for (var j = 0; j < numOfLeaves; j++) {
                var leaves = new c.Bitmap(imageQueue.getResult('tree' + (i + 1) + '_' + (j + 1)));
                stage.addChild(leaves);
                blowingLeaves.push(
                    {
                        tree: 'tree' + (i + 1),
                        leaves: leaves
                    }
                );
            }
        }
        blowingLeaves.reverse();

        // AT THIS POINT WE CAN ADD THE WHITE COVER AND
        // FADE IT OUT TO FADE THE APP IN.
        stage.addChild(coverFade);
        c.Tween.get(coverFade).to({alpha: 0}, 1000);
        treeWind();
    }

    function newWindDelay() {
        var newDelay = (Math.random() * 6000) + 2000;
        setTimeout(treeWind, newDelay);
    }

    function treeWind() {
        var xMove = -Math.random() * 4;
        var yMove = Math.random() * 4;

        var treeCount = 0;
        for (var i = 0; i < blowingLeaves.length; i++) {
            var leaves = blowingLeaves[i].leaves;
            c.Tween.get(leaves).wait(i * 150)
                .to({x: xMove, y: yMove}, 1000, c.Ease.getBackInOut(1.5))
                .to({x: 0, y: 0}, 1000, c.Ease.getBackInOut(1.5)).call(function () {
                treeCount++;
                if (treeCount == blowingLeaves.length) {
                    newWindDelay();
                }
            });
        }
    }
})();