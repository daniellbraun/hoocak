<?php
/**
 * Plugin Name: Language Games
 * Description: This is a plugin for Language Games and Quizzes (Requires ACF)
 * Version: 1.0.0
 * Author: Dan Braun
 * Author URI: http://www.ad-lit.com
 */

// If this file is called directly, abort.
if ( ! defined( 'WPINC' ) ) {
    die;
}

/**
 * Load required dependencies for this plugin.
 */

// Define all custom post types
require_once plugin_dir_path( __FILE__ ) . 'includes/custom-post-types.php';
// Include taxonomies
require_once plugin_dir_path( __FILE__ ) . 'includes/word-category-taxonomies.php';
// Define all admin customizations
require_once plugin_dir_path( __FILE__ ) . 'includes/admin-customizations.php';
// Include game scripts
require_once plugin_dir_path( __FILE__ ) . 'includes/game-scripts.php';