# Theme Options #

This plugin adds an ACF options page with fields for address, phone, email, and social media links.

The social media links have an option for choosing the FontAwesome icon to use for each social media platform, which makes it easy to add/remove icons from a social media menu.

**A Note Regarding FontAwesome**: FontAwesome must be enqueued in the WordPress admin in order to see the icon options for each social media link. It's enqueued automatically as part of the [adl-site-functionality](https://bitbucket.org/adlit/adl-site-functionality) plugin, but if you're not using that you'll have to do it manually.

## Contact Info ##
```twig
{# Address #}
{{ option.address_street }}<br>
{{ option.address_city }} {{ option.address_state }}, {{ option.address_zip }}

{# Phone #}
{{ option.phone }}

{# Email #}
{{ option.email }}
```

### Adding Additional Fields ###
If you need more fields you can add them to the `acf-json/group_options.json` file. Or you can synchronize the fields and edit them like you normally would on the Custom Fields page.


### Shortcodes ###
There are also `[phone]`, `[address]`, and `[email]` shortcodes available to use in the WYSIWYG editor.

## Social Media Icons ##

### Social Media Nav with Icons Only ###
```twig
<nav class="nav-social-media">
	<ul class="menu">
		{% for icon in fn('get_social_media_icons') %}
			{% if icon.link %}
				<li>
					<a href="{{ icon.link }}" title="{{ icon.title }}">
						<i class="fa fa-2x fa-{{ icon.icon }}" aria-hidden="true"></i>
					</a>
				</li>
			{% endif %}
		{% endfor %}
	</ul>
</nav>
```

### Social Media Nav with Stacked Circle Icons ###
```twig
<nav class="nav-social-media">
    <ul class="menu">
        {% for icon in fn('get_social_media_icons') %}
            {% if icon.link %}
                <li>
                    <a href="{{ icon.link }}" title="{{ icon.title }}">
                        <span class="fa-stack" aria-hidden="true">
                          <i class="fa fa-stack-2x fa-circle-thin"></i>
                          <i class="fa fa-stack-1x fa-{{ icon.icon }}"></i>
                        </span>
                    </a>
                </li>
            {% endif %}
        {% endfor %}
    </ul>
</nav>
```

#### Changing Order of the Icons ####
There's no way to change the order of the icons from the WordPress dashboard right now.

To change the order manually, rearrange the order of the icon names in the array on line 159 in `theme-options.php`

```php
$icon_names = [
	'Facebook',
	'Twitter',
	'YouTube',
	'Trip Advisor',
	'Instagram',
	'Pinterest',
	'Yelp',
	'Google Plus',
];
```