<?php
/*
	Plugin Name: Theme Options
	Description: Adds custom fields for phone number, address, and social media icons.
	Author: Heather Brokmeier, Ad-Lit Inc.
	Version: 1.0.0
	Author URI: http://www.ad-lit.com/
*/

// Exit plugin if accessed directly.
if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

if ( ! class_exists( 'Theme_Options' ) ) {

	/**
	 * Class ADL_Options
	 */
	class Theme_Options {

		/**
		 * ADL_Options constructor.
		 */
		public function __construct() {
			// Add json load point.
			add_filter( 'acf/settings/load_json', array( $this, 'filter_acf_json_load_point' ) );

			// Add 'options' to Timber context.
			add_filter( 'timber_context', array( $this, 'add_to_context' ) );

			// Add options page (after ACF is loaded).
			add_action( 'plugins_loaded', array( $this, 'action_acf_register_options_pages' ) );

			// Add shortcodes for contact info.
			add_shortcode( 'phone', array( $this, 'shortcode_phone' ) );
			add_shortcode( 'email', array( $this, 'shortcode_email' ) );
			add_shortcode( 'address', array( $this, 'shortcode_address' ) );
		}


		/**
		 * Add ACF json load point relative to this plugin.
		 *
		 * All custom fields that you want stored as json should be kept here.
		 *
		 * @link https://www.advancedcustomfields.com/resources/local-json/
		 * @param  array $paths Load point paths.
		 * @return array
		 */
		public function filter_acf_json_load_point( $paths ) {
			// Append path, and allow theme to override it with custom json.
			$paths[] = plugin_dir_path( __FILE__ ) . 'acf-json';

			return $paths;
		}


		/**
		 * Add ACF options to context, checks to see if 'option' or 'options'
		 * is already defined in context.
		 *
		 * @param array $context Timber context.
		 * @return array Timber context.
		 */
		public function add_to_context( $context ) {
			if ( function_exists( 'get_fields' ) ) {
				if ( empty( $context['option'] ) ) {
					$context['option'] = get_fields( 'option' );
				}
			}

			return $context;
		}


		/**
		 * Add ACF options page to admin menu.
		 *
		 * @link https://www.advancedcustomfields.com/resources/options-page/
		 */
		public function action_acf_register_options_pages() {
			if ( function_exists( 'acf_add_options_page' ) ) {
				acf_add_options_page( array(
					'page_title' => 'Options',
					'menu_slug'  => 'options-page',
					'icon_url'   => 'dashicons-admin-generic',
					'redirect'   => false,
					'position'   => 29,
				) );
			}
		}


		/**
		 * Shortcode for accessing Phone Number.
		 *
		 * @param array $atts Shortcode attributes.
		 * @return bool|mixed|null|void
		 */
		function shortcode_phone( $atts ) {
			if ( function_exists( 'get_field' ) ) {
				$number = get_field( 'phone', 'option' );

				return '<a href="tel:' . format_phone( $number ) . '">' . $number . '</a>';
			}

			return null;
		}


		/**
		 * Shortcode for accessing Phone Number.
		 *
		 * @param array $atts Shortcode attributes.
		 *
		 * @return bool|mixed|null|void
		 */
		function shortcode_email( $atts ) {
			if ( function_exists( 'get_field' ) ) {
				$email = get_field( 'email', 'option' );

				return '<a href="mailto:' . $email . '">' . $email . '</a>';
			}

			return null;
		}


		/**
		 * Shortcode for accessing Phone Number.
		 *
		 * @param array $atts Shortcode attributes.
		 * @return bool|mixed|null|void
		 */
		function shortcode_address( $atts ) {
			$address = '';

			if ( function_exists( 'get_field' ) ) {
				$address .= get_field( 'address_street', 'option' ) . '<br>';
				$address .= get_field( 'address_city', 'option' ) . ' ';
				$address .= get_field( 'address_state', 'option' ) . ', ';
				$address .= get_field( 'address_zip', 'option' );
			}

			return $address;
		}
	}

	$theme_options = new Theme_Options;
}


/**
 * Returns list of all icons that have a link associated with them.
 *
 * @return array $icons Array with icon name, FontAwesome icon name, and URL.
 */
function get_social_media_icons() {
	$icons = [];

	/*
	 * A list of all icons as defined in the group_options.json field group.
	 *
	 * These names should match the ACF field label, and be capitalized correctly
	 * because they can be used to display the name of the icon.
	 *
	 * The corresponding ACF field name for each of these should be all lowercase
	 * with spaces removed.
	 */
	$icon_names = [
		'Facebook',
		'Twitter',
		'YouTube',
		'Trip Advisor',
		'Instagram',
		'Pinterest',
		'Yelp',
		'Google Plus',
	];

	if ( function_exists( 'get_field' ) ) {
		foreach ( $icon_names as $icon ) {
			// Icon name converted to lowercase with spaces removed.
			$field_name = strtolower( str_replace( ' ', '', $icon ) );

			if ( get_field( $field_name, 'option' ) ) {
				$icons[] = [
					'name' => $icon,
					'icon' => get_field( $field_name . '_icon', 'option' ),
					'link' => get_field( $field_name, 'option' ),
				];
			}
		}
	}

	return $icons;
}

/**
 * Strip out non-numeric characters, so phone number can be used in telephone link.
 *
 * @param string $phone This is the non-formated phone number.
 * @return mixed
 */
function format_phone( $phone ) {
	return preg_replace('/[^0-9]/', '', $phone );
}
