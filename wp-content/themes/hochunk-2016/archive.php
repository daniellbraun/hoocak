<?php
/**
 * The template for displaying Archive pages.
 *
 * Used to display archive-type pages if nothing more specific matches a query.
 * For example, puts together date-based pages if no date.php file exists.
 *
 * Learn more: http://codex.wordpress.org/Template_Hierarchy
 *
 * Methods for TimberHelper can be found in the /lib sub-directory
 *
 * @package  WordPress
 * @subpackage  Timber
 * @since   Timber 0.2
 */

$context = Timber\Timber::get_context();

/*
 * We use a switch statement here to get pages that correspond to our archive pages
 * so that we can use the title, editor, and custom fields from a page on an
 * archive page, but still use the archive page templates.
 */

switch ( get_post_type() ) {
	case 'cpt_slug_1' :
        // ID of the page that contains the title/content for this archive.
		$t_post = new Timber\Post( 12 );
		$context['post'] = $t_post;
        $context['posts'] = Timber\Timber::get_posts();
        
        // You can add other stuff to the context here, if you need to.
        $context['other_value'] = 'Custom value for the cpt_slug_1 archive.';
		break;

    case 'cpt_slug_2' :
		$t_post = new Timber\Post( 24 );
		$context['post']  = $t_post;
		$context['posts'] = Timber\Timber::get_posts();
		break;

	default:
		$context['posts'] = Timber\Timber::get_posts();
		break;
}

Timber\Timber::render( array(
	'archive-' . get_post_type() . '.twig',
	'archive.twig',
	'index.twig',
), $context );
