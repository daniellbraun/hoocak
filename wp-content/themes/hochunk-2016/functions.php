<?php
/**
 * Sage includes
 *
 * The $sage_includes array determines the code library included in your theme.
 * Add or remove files to the array as needed. Supports child theme overrides.
 *
 * Please note that missing files will produce a fatal error.
 *
 * @link https://github.com/roots/sage/pull/1042
 */
$sage_includes = [
    'includes/timber.php',    // Twig magic
    'includes/assets.php',    // Scripts and stylesheets
    'includes/extras.php',    // Custom functions
    'includes/setup.php',     // Theme and widget setup
];

if (!class_exists('Timber')) {
    add_action('admin_notices', function () {
        echo '<div class="error"><p>Timber not activated. Make sure you activate the plugin in <a href="' . esc_url(admin_url('plugins.php#timber')) . '">' . esc_url(admin_url('plugins.php')) . '</a></p></div>';
    });
    return;
}

Timber::$dirname = array('templates', 'views');

foreach ($sage_includes as $file) {
    if (!$filepath = locate_template($file)) {
        trigger_error(sprintf('Error locating %s for inclusion', $file), E_USER_ERROR);
    }
    require_once $filepath;
}
unset($file, $filepath);

add_filter('tiny_mce_before_init', 'tinymce_add_chars');
function tinymce_add_chars($settings)
{
    $new_chars = json_encode(array(
        array('261', 'a WITH OGONEK'),
        array('260', 'A WITH OGONEK'),
        array('287', 'g WITH CARON'),
        array('286', 'G WITH CARON'),
        array('303', 'i WITH OGONEK'),
        array('302', 'I WITH OGONEK'),
        array('353', 's WITH CARON'),
        array('352', 'S WITH CARON'),
        array('371', 'u WITH OGONEK'),
        array('370', 'U WITH OGONEK'),
        array('382', 'z WITH CARON'),
        array('381', 'Z WITH CARON'),
    ));
    $settings['charmap_append'] = $new_chars;
    return $settings;
}

function echo_something($value)
{
    echo '<br><pre>';
    print_r($value);
    echo '</pre><br>';
}

//add_filter('sensei_single_course_content_inside_after', 'check_logged_in_or_remove_lesson_list');
function check_logged_in_or_remove_lesson_list()
{
    if (!is_user_logged_in()): ?>

        <script>
            (function ($) {

                var $moduleElements = $('.course.post-15 article.module');
                var $unitsHeader = $("h2:contains('Modules')");
                $moduleElements.hide();
                $unitsHeader.hide();

            })(jQuery);
        </script>

        <?php
    endif;
}