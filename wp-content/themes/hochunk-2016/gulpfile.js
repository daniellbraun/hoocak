'use strict';

var plugins = require('gulp-load-plugins');
var yargs = require('yargs');
var browser = require('browser-sync');
var gulp = require('gulp');
var yaml = require('js-yaml');
var fs = require('fs');
var path = require('path');
var svgstore = require('gulp-svgstore');
var svgmin = require('gulp-svgmin');
var fontBlast = require('font-blast');

// Load all Gulp plugins into one variable
var $ = plugins();

// Check for --production flag
var PRODUCTION = !!(yargs.argv.production);

// Load settings from settings.yml
// const { COMPATIBILITY, PORT, PROXY, PATHS } = loadConfig();
var config = loadConfig();
var COMPATIBILITY = config.COMPATIBILITY;
var PORT = config.PORT;
var PROXY = config.PROXY;
var PATHS = config.PATHS;

function loadConfig() {
    var ymlFile = fs.readFileSync('config.yml', 'utf8');
    return yaml.load(ymlFile);
}

// Build the "dist" folder by running all of the below tasks
gulp.task('build',
    gulp.series(gulp.parallel(sass, javascript, svg)));

// Build the site, run the server, and watch for file changes
gulp.task('default',
    gulp.series('build', server, watch));

// Watch for file changes w/out BrowserSync started.
gulp.task('watch', watch);

// Combine & minify all SVGs into SVG sprite.
gulp.task('svg', svg);

// Generate FontAwesome icons as SVGs.
gulp.task('fa', fontAwesome);

// Compile Sass into CSS
// In production, the CSS is compressed
function sass() {
    return gulp.src('src/scss/app.scss')
        .pipe($.sourcemaps.init())
        .pipe($.sass({
            includePaths: PATHS.sass
        })
            .on('error', $.sass.logError))
        .pipe($.autoprefixer({
            browsers: COMPATIBILITY
        }))
        // Comment in the pipe below to run UnCSS in production
        .pipe($.if(PRODUCTION, $.cssnano({
            autoprefixer: { browsers: COMPATIBILITY, add: true }
        })))
        .pipe($.if(!PRODUCTION, $.sourcemaps.write()))
        .pipe(gulp.dest('css'))
        .pipe($.touch())
        .pipe(browser.stream());
}

// Combine JavaScript into one file
// In production, the file is minified
function javascript() {
    return gulp.src(PATHS.javascript)
        .pipe($.sourcemaps.init())
        .pipe($.babel({ignore: ['photoswipe.js', 'photoswipe-ui-default', 'what-input.js']}))
        .pipe($.concat('app.js'))
        .pipe($.if(PRODUCTION, $.uglify()
            .on('error', function(e) { console.log(e) })))
        .pipe($.if(!PRODUCTION, $.sourcemaps.write()))
        .pipe(gulp.dest('js'));
}

// Convert folder of individual SVGs into an SVG sprite.
function svg() {
    return gulp
        .src('src/svg/*.svg')
        .pipe(svgmin(function (file) {
            var prefix = path.basename(file.relative, path.extname(file.relative));
            return {
                plugins: [{
                    cleanupIDs: {
                        prefix: prefix + '-',
                        minify: true
                    }
                }]
            }
        }))
        .pipe(svgstore({ inlineSvg: true }))
        .pipe(gulp.dest('img/svg'));
}

/*
 * Generates all SVGs from the FontAwesome font.
 * Modified from this example:
 * https://github.com/eugene1g/font-blast-examples/blob/master/popular-fonts.js#L49
 *
 * Download the new font file adn icons.yml for a new version of FontAwesome:
 * - fontawesome-webfont.svg from: https://github.com/FortAwesome/Font-Awesome/tree/master/fonts
 * - icons.yml from: https://github.com/FortAwesome/Font-Awesome/tree/master/src
 */
function fontAwesome(done) {
    var ymlFile = fs.readFileSync('src/font-awesome/icons.yml', 'utf8');
    var iconNamingConventions = yaml.load(ymlFile).icons;

    var convertFilenames = {};
    iconNamingConventions.forEach(function (icon) {
        convertFilenames[icon.unicode] = icon.id;
    });

    fontBlast('src/font-awesome/fontawesome-webfont.svg', 'src/font-awesome', {
        filenames: convertFilenames
    });
    done();
}

// Start a server with BrowserSync to preview the site in
function server(done) {
    browser.init({
        proxy: PROXY,
        port: PORT
    });
    done();
}

// Watch for changes to static assets, pages, Sass, and JavaScript
function watch() {
    gulp.watch('*.php').on('all', browser.reload);
    gulp.watch('templates/**/*.twig').on('all', browser.reload);
    gulp.watch('src/scss/**/*.scss').on('all', gulp.series(sass));
    gulp.watch('src/svg/*.svg').on('all', gulp.series(svg));
    gulp.watch('src/js/**/*.js').on('all', gulp.series(javascript, browser.reload));
}
