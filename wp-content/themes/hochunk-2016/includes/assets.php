<?php

namespace AdLit\Sage\Assets;

/**
 * Get paths for assets
 */
function asset_path($filename)
{
    $dist_path = get_template_directory_uri() . '/';
    $directory = dirname($filename) . '/';
    $file = basename($filename);
    return $dist_path . $directory . $file;
}

/**
 * Theme assets
 */
function assets()
{
    /*
     * Styles
     */
    wp_register_style( 'app/css', asset_path('css/app.css'), [], '1.1' );
    wp_register_style( 'fontawesome', 'https://maxcdn.bootstrapcdn.com/font-awesome/4.4.0/css/font-awesome.min.css', '4.4.0' );
    wp_register_style( 'fonts', 'https://fonts.googleapis.com/css?family=Source+Sans+Pro&subset=latin,latin-ext' );

    wp_enqueue_style([
        'fonts',
        'fontawesome',
        'app/css',
    ]);

    /*
     * Scripts
     */
    wp_deregister_script( 'jquery' ); // Deregister the jquery version bundled with wordpress
    wp_register_script( 'jquery', 'https://ajax.googleapis.com/ajax/libs/jquery/2.1.4/jquery.min.js', array(), '2.1.4' );

    wp_register_script('app/js', asset_path( 'js/app.js' ), ['jquery'], null, true);
    wp_register_script('google-maps/js', 'https://maps.googleapis.com/maps/api/js?key=AIzaSyCn_CLx3VelYA2fuR8jH5Su1h68j6CVmOk', [], 1.0, true);
    wp_register_script('google-client-library/js', 'https://apis.google.com/js/api.js', [], null, true);

    wp_register_script('fb-widget/js', asset_path( 'js/fb.js' ), [], null, true);
    wp_register_script('google-maps-frontpage/js', asset_path( 'js/google-maps.js' ), [], null, true);

    // Conditional scripts
    if (is_front_page() || is_page('locations')) {
        wp_enqueue_script([
            'jquery',
            'google-client-library/js',
            'google-maps/js',
	        'google-maps-frontpage/js',
	        'fb-widget/js',
            'app/js'
        ]);
    } else {
        wp_enqueue_script([
            'jquery',
            'google-client-library/js',
            'app/js'
        ]);
    }

    if (is_single() && comments_open() && get_option('thread_comments')) {
        wp_enqueue_script('comment-reply');
    }

}

add_action('wp_enqueue_scripts', __NAMESPACE__ . '\\assets', 100);
