<?php

namespace AdLit\Sage\Extras;

use AdLit\Sage\Setup;


/**
 * Clean up the_excerpt()
 */
function excerpt_more()
{
    return ' &hellip; <a href="' . get_permalink() . '">Continued</a>';
}

add_filter('excerpt_more', __NAMESPACE__ . '\\excerpt_more');

//* Redirect WordPress Logout to Home Page
add_action('wp_logout', create_function('', 'wp_redirect(home_url());exit();'));

function force_splash_page()
{
    if (!is_user_logged_in() && !is_front_page()) {
        wp_redirect(home_url());
        exit();
    }
}
//add_action('wp_head', __NAMESPACE__ . '\\force_splash_page');

/**
 * Latitude and Longitude validation for ACF to Google Maps fields
 */

function returnInvalidMsg($msg) {
    $coord_val_length_message = 'This coordinate is too long. Please reduce the number of digits after the decimal. 6 is enough.';
    $coord_val_missing_val_message = 'You must enter both a latitude and longitude';
    $coord_val_format_message = 'The supplied longitude coordinate is not correctly formatted';
    switch ($msg) {
        case 'too_long' :
            return $coord_val_length_message;
            break;
        case 'missing_val' :
            return $coord_val_missing_val_message;
            break;
        case 'bad_format' :
            return $coord_val_format_message;
            break;
        default:
            return 'Invalid entry';
    }
}

function locations_validate_lng_value($valid, $value, $field, $input)
{
    // bail early if value is already invalid
    if (!$valid) {
        return $valid;
    }
    $value_trimmed = trim($value);

    $arr = explode('.', $value_trimmed);
    if (strlen($arr[1]) > 12) {
        return $valid = returnInvalidMsg('too_long');
    }

    $locations_repeater_fields = $_POST['acf']['field_57f27eb0bae17'];

    foreach ($locations_repeater_fields as $repeater_field) {

        if (trim($repeater_field['field_57f27efabae1b']) == $value_trimmed) {

            $lat_field = trim($repeater_field['field_57f27ee5bae1a']);
            if ($value_trimmed != '' && $lat_field == '') {
                return $valid = returnInvalidMsg('missing_val');
            }
            break;
        }
    }
    if ((preg_match('/^-?([1]?[1-7][1-9]|[1]?[1-8][0]|[1-9]?[0-9])\.{1}\d{1,17}$/', $value_trimmed) === 1) || $value_trimmed == '') {
        // return
        return $valid;
    } else {
        $valid = returnInvalidMsg('bad_format');
        return $valid;
    }
}

function locations_validate_lat_value($valid, $value, $field, $input)
{
    // bail early if value is already invalid
    if (!$valid) {
        return $valid;
    }
    $value_trimmed = trim($value);

    $arr = explode('.', $value_trimmed);
    if (strlen($arr[1]) > 12) {
        return $valid = returnInvalidMsg('too_long');
    }

    $locations_repeater_fields = $_POST['acf']['field_57f27eb0bae17'];

    foreach ($locations_repeater_fields as $repeater_field) {

        if (trim($repeater_field['field_57f27ee5bae1a']) == $value_trimmed) {

            $lng_field = trim($repeater_field['field_57f27efabae1b']);
            if ($value_trimmed != '' && $lng_field == '') {
                return $valid = returnInvalidMsg('missing_val');
            }
            break;
        }
    }

    if ((preg_match('/^-?([1-8]?[1-9]|[1-9]0)\.{1}\d{1,17}$/', $value_trimmed) === 1) || $value_trimmed == '') {
        // return
        return $valid;
    } else {
        $valid = returnInvalidMsg('bad_format');
        return $valid;
    }
}

function homepage_validate_lat_value($valid, $value, $field, $input)
{
    // bail early if value is already invalid
    if (!$valid) {
        return $valid;
    }
    $value_trimmed = trim($value);

    $arr = explode('.', $value_trimmed);
    if (strlen($arr[1]) > 12) {
        return $valid = returnInvalidMsg('too_long');
    }

    $lng_field = trim($_POST['acf']['field_57f27e5a91226']);
    if ($value_trimmed != '' && $lng_field == '') {
        return $valid = returnInvalidMsg('missing_val');
    }

    if ((preg_match('/^-?([1-8]?[1-9]|[1-9]0)\.{1}\d{1,17}$/', $value_trimmed) === 1) || $value_trimmed == '') {
        // return
        return $valid;
    } else {
        $valid = returnInvalidMsg('bad_format');
        return $valid;
    }
}

function homepage_validate_lng_value($valid, $value, $field, $input)
{
    // bail early if value is already invalid
    if (!$valid) {
        return $valid;
    }
    $value_trimmed = trim($value);

    $arr = explode('.', $value_trimmed);
    if (strlen($arr[1]) > 12) {
        return $valid = returnInvalidMsg('too_long');
    }

    $lat_field = trim($_POST['acf']['field_57f27e4391225']);
    if ($value_trimmed != '' && $lat_field == '') {
        return $valid = returnInvalidMsg('missing_val');
    }

    if ((preg_match('/^-?([1]?[1-7][1-9]|[1]?[1-8][0]|[1-9]?[0-9])\.{1}\d{1,17}$/', $value_trimmed) === 1) || $value_trimmed == '') {
        // return
        return $valid;
    } else {
        $valid = returnInvalidMsg('bad_format');
        return $valid;
    }
}

add_filter('acf/validate_value/name=longitude', __NAMESPACE__ . '\\locations_validate_lng_value', 10, 4);
add_filter('acf/validate_value/name=latitude', __NAMESPACE__ . '\\locations_validate_lat_value', 10, 4);
add_filter('acf/validate_value/name=main_location_latitude', __NAMESPACE__ . '\\homepage_validate_lat_value', 10, 4);
add_filter('acf/validate_value/name=main_location_longitude', __NAMESPACE__ . '\\homepage_validate_lng_value', 10, 4);

// Custom shortcode for placing GET var in post from admin CMS
function registrant_name_func( $atts )
{
    if (isset($_GET['registrant_name'])) {
        return strip_tags($_GET['registrant_name']);
    } else {
        return '';
    }
}
add_shortcode( 'registrant_name', __NAMESPACE__ . '\\registrant_name_func' );