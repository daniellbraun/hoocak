<?php

namespace AdLit\Sage\Setup;

use AdLit\Sage\Assets;

/**
 * Theme setup
 */
function setup() {
	// Add theme support
    add_theme_support( 'post-formats' );
    add_theme_support( 'menus' );
	add_theme_support( 'title-tag' );
	add_theme_support( 'post-thumbnails' );
	add_theme_support( 'html5', ['caption', 'comment-form', 'comment-list', 'gallery', 'search-form'] );
//	add_theme_support( 'post-formats', ['aside', 'gallery', 'link', 'image', 'quote', 'video', 'audio'] );

	// Register wp_nav_menu() menus
	register_nav_menus([
		'primary_navigation' => 'Primary Navigation',
        'logged_in_navigation' => 'Logged In Menu'
	]);

	// Use main stylesheet for visual editor
	// This imports the main stylesheet into the visual editor, with some
	// fixes for Foundation styles that messed it up.
	// To add custom styles, add them to this file.
	add_editor_style( Assets\asset_path( 'css/admin-app.css' ) );
}
add_action('after_setup_theme', __NAMESPACE__ . '\\setup');


/**
 * Register sidebars
 */
function widgets_init() {
	register_sidebar([
		'name'          => 'Primary',
		'id'            => 'sidebar-primary',
		'before_widget' => '<section class="widget %1$s %2$s">',
		'after_widget'  => '</section>',
		'before_title'  => '<h3>',
		'after_title'   => '</h3>'
	]);

	register_sidebar([
		'name'          => 'Footer',
		'id'            => 'sidebar-footer',
		'before_widget' => '<section class="widget %1$s %2$s">',
		'after_widget'  => '</section>',
		'before_title'  => '<h3>',
		'after_title'   => '</h3>'
	]);
}
add_action('widgets_init', __NAMESPACE__ . '\\widgets_init');


/**
 * Determine which pages should NOT display the sidebar
 */
function display_sidebar() {
	static $display;

	isset($display) || $display = !in_array(true, [
		// The sidebar will NOT be displayed if ANY of the following return true.
		// @link https://codex.wordpress.org/Conditional_Tags
		is_404(),
		// is_front_page(),
		is_page_template('template-custom.php'),
	]);

	return apply_filters('sage/display_sidebar', $display);
}

function adl_hide_bar() {
    if (!current_user_can('edit_posts')) {
        show_admin_bar(false);
    }
}
add_action('set_current_user', __NAMESPACE__ . '\\adl_hide_bar');