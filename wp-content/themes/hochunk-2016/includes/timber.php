<?php
use AdLit\Sage\Setup;

/**
 * Check if Timber is activated
 */

if (!class_exists('Timber')) {

    add_action('admin_notices', function () {
        echo '<div class="error"><p>Timber not activated. Make sure you activate the plugin in <a href="' . esc_url(admin_url('plugins.php#timber')) . '">' . esc_url(admin_url('plugins.php')) . '</a></p></div>';
    });
    return;

}

// Define path to template files
Timber::$dirname = array('templates', 'views');

/**
 * Timber
 */
class StarterTheme extends TimberSite
{
    function __construct()
    {

        add_filter('timber_context', array($this, 'add_to_context'));
        add_filter('get_twig', array($this, 'add_to_twig'));

        add_action('init', array($this, 'register_post_types'));
        add_action('init', array($this, 'register_taxonomies'));

        parent::__construct();
    }

    function register_post_types()
    {
        //this is where you can register custom post types
    }

    function register_taxonomies()
    {
        //this is where you can register custom taxonomies
    }

    function add_to_context($context)
    {
        /* Add extra data */
        $context['logged_in'] = is_user_logged_in();
        $context['login_url'] = get_site_url() . '/log-in/';
        $context['register_url'] = get_site_url() . '/register/';
        $context['logout_url'] = wp_logout_url();

        /* Menu */
        if (is_user_logged_in()) {
            $context['menu'] = new TimberMenu('logged_in_navigation');
        } else {
            $context['menu'] = new TimberMenu('primary_navigation');
        }
        $context['footer_menu'] = new TimberMenu(33);

        /* Site info */
        $context['site'] = $this;

        /* Sidebars */
        $context['display_sidebar'] = Setup\display_sidebar();
        $context['sidebar_primary'] = Timber::get_widgets('sidebar-primary');

        return $context;
    }

    function add_to_twig($twig)
    {
        /* this is where you can add your own functions to twig */
        $twig->addExtension(new Twig_Extension_StringLoader());
        return $twig;
    }
}

new StarterTheme();