/**
 * Code from ACF maps documentation.
 *
 * @link https://www.advancedcustomfields.com/resources/google-map/
 */


/**
 *  new_map
 *
 *  This function will render a Google Map onto the selected jQuery element
 *
 *  @type    function
 *  @date    8/11/2013
 *  @since    4.3.0
 *
 *  @param    $el (jQuery element)
 *  @return    n/a
 */

function new_map($el) {

    // var
    var $markers = $el.find('.marker');
    var numOfMarkers = $markers.length;

    if (geocoder == null) {
        geocoder = new google.maps.Geocoder();
    }

    var disableUI = $markers.attr('data-disableDefaultUI') != 0;

    // vars
    var args = {
        zoom: 1,
        center: new google.maps.LatLng(0, 0),
        mapTypeId: google.maps.MapTypeId.ROADMAP,
        disableDefaultUI: disableUI,
        scrollwheel: $markers.attr('data-scrollwheel')
    };


    // create map
    var map = new google.maps.Map($el[0], args);


    // add a markers reference
    map.markers = [];

    // add infowindows reference
    map.infowindows = [];

    // add markers
    $markers.each(function () {

        add_marker($(this), map, numOfMarkers);

    });

    // center map
    //center_map(map, $markers.attr('data-center-lat'), $markers.attr('data-center-lng'));


    // return
    return map;

}

/**
 *  add_marker
 *
 *  This function will add a marker to the selected Google Map
 *
 *  @type    function
 *  @date    8/11/2013
 *  @since    4.3.0
 *
 *  @param    $marker (jQuery element)
 *  @param    map (Google Map object)
 *  @param    numOfMarkers (if more than one, don't show infowindow)
 *  @return    n/a
 */

function add_marker($marker, map, numOfMarkers) {

    // var
    var latlng;

    if ($marker.attr('data-lat') != '' && $marker.attr('data-lng') != '') {
        latlng = new google.maps.LatLng($marker.attr('data-lat'), $marker.attr('data-lng'));
    }

    var address = $marker.attr('data-address');

    if (!latlng) {

        if (geocoder) {
            geocoder.geocode({'address': address}, function (results, status) {
                if (status == google.maps.GeocoderStatus.OK) {
                    if (status != google.maps.GeocoderStatus.ZERO_RESULTS) {

                        createMarker($marker, results[0].geometry.location, numOfMarkers, map);

                    } else {
                        console.log("NO RESULTS FOUND FOR:", address);
                    }
                } else {
                    console.log("THERE WAS A PROBLEM WITH GEOCODER:", status);
                }
            })
        } else {
            console.log("Geocoder is not defined");
        }

    } else {

        createMarker($marker, latlng, numOfMarkers, map);

    }

}

/**
 *  createMarker
 *
 *  This function will create a marker depending on either address data or lat long data
 *
 *  @type    function
 *
 *  @param    map (Google Map object)
 *  @param  $marker current marker html element that holds data- attributes
 *  @param  location google latlng object
 *  @param  numOfMarkers used to determine if this is a single marker map, if so show infowindow
 *  @return    n/a
 */

function createMarker($marker, location, numOfMarkers, map) {
    // create marker
    var marker = new google.maps.Marker({
        position: location,
        map: map
    });

    // add to array
    map.markers.push(marker);

    // if marker contains HTML, add it to an infoWindow
    if ($marker.html()) {
        // create info window

        var title = $marker.attr('data-title');
        var titleLink = 'https://www.google.com/maps/place/' +
            location.lat() + '+' + location.lng() + '/@' + location.lat() +
            ',' + location.lng() + ',15z';
        var titleContent = '<h4><a href="' + titleLink +
            '" target="_blank">' + title + '</a></h4>';

        var $titleSpan = $('.location-block span:contains(' + title + ')');
        $titleSpan.contents().wrap('<a href="' + titleLink + '" target="_blank"></a>');

        var infowindow = new google.maps.InfoWindow(
            {
                content: titleContent + $marker.html()
            });
        map.infowindows.push(infowindow);
        if (numOfMarkers == 1) {
            infowindow.open(map, marker);
        }
        // show info window when marker is clicked

        google.maps.event.addListener(marker, 'click', function () {

            // first close any open infowindows on this map
            for (var i = 0; i < map.infowindows.length; i++) {
                map.infowindows[i].close();
            }

            infowindow.open(map, marker);

        });
    }
    center_map(map);
}

/**
 *  center_map
 *
 *  This function will center the map, showing all markers attached to this map
 *
 *  @type    function
 *  @date    8/11/2013
 *  @since    4.3.0
 *
 *  @param    map (Google Map object)
 *  @param  offsetLat (lat of first (only) marker used if only 1 marker present on map
 *  @param  offsetLng (lng of first (only) marker used if only 1 marker present on map
 *  @return    n/a
 */

function center_map(map, offsetLat, offsetLng) {

    // vars
    var bounds = new google.maps.LatLngBounds();

    // loop through all markers and create bounds
    $.each(map.markers, function (i, marker) {

        var latlng = new google.maps.LatLng(marker.position.lat(), marker.position.lng());

        bounds.extend(latlng);

    });

    // only 1 marker?
    if (map.markers.length == 1) {

        // set center of map
        if (offsetLat != null && offsetLng != null) {

            map.setCenter(new google.maps.LatLng(offsetLat, offsetLng));

        } else {

            map.setCenter(bounds.getCenter());

        }

        map.setZoom(14);
    }
    else {
        // fit to bounds
        map.fitBounds(bounds);
    }

}

/**
 *  document ready
 *
 *  This function will render each map when the document is ready (page has loaded)
 *
 *  @type    function
 *  @date    8/11/2013
 *  @since    5.0.0
 *
 *  @param    n/a
 *  @return    n/a
 */
// global var
var map = null;
var geocoder = null;

function initMap() {

    $('.acf-map').each(function () {

        // create map
        map = new_map($(this));

    })
}

initMap();
