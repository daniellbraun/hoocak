<?php
/**
 * The template for displaying all pages.
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site will use a
 * different template.
 *
 * To generate specific templates for your pages you can use:
 * /mytheme/views/page-mypage.twig
 * (which will still route through this PHP file)
 * OR
 * /mytheme/page-mypage.php
 * (in which case you'll want to duplicate this file and save to the above path)
 *
 * Methods for TimberHelper can be found in the /lib sub-directory
 *
 * @package  WordPress
 * @subpackage  Timber
 * @since    Timber 0.1
 */
$context = Timber\Timber::get_context();
$post = new Timber\Post();
$context['post'] = $post;


if ( is_page( 'learning-materials' ) ) {
	// these queries only need to run on the learning-materials page
	$args = array(
		'post_type' => 'adl_learning',
		'orderby'   => 'menu_order'
	);
	$context['learning_activity'] = Timber\Timber::get_posts( $args );
	$context['difficulty']        = Timber\Timber::get_terms( 'adl_learning_difficulty' );

}

if ( $post->post_name == 'register' ) {

	// check if class maximum students reached

	$learners = array();

	$args = array(
		'post_type' => 'course',
		'field'     => array( 'ids' )
	);

	$courses = Timber\Timber::get_posts( $args );

	$users = get_users( array(
		'fields' => 'ids',
		'role'   => 'subscriber'
	) );

	$context['number_of_learners'] = count( $users );
}

if ( is_front_page() ) {

	Timber\Timber::render( array( 'front-page.twig', 'page-' . $post->post_name . '.twig', 'page.twig' ), $context );

} elseif ( is_page( 'log-in' ) ) {

	if ( is_user_logged_in() ) {

		wp_redirect( get_site_url() . '/hoocak-academy/my-courses/' );

	} else {

		$context['login_form'] = wp_login_form( array(
			'echo' => false,
		) );

		Timber\Timber::render( array(
			'page-login.twig',
			'page-' . $post->post_name . '.twig',
			'page.twig'
		), $context );
	}

} else {

	Timber\Timber::render( array( 'page-' . $post->post_name . '.twig', 'page.twig' ), $context );

}