<?php
/**
 * Content wrappers Start
 *
 * All support theme wrappers can be found in includes/theme-integrations
 *
 * @author 		WooThemes
 * @package 	WooCommerce/Templates
 * @version     1.6.4
 */
?>
<div class="progress-map">
</div>
<div id="progress-map-loading" class="row column text-center">
</div>
<div id="content" class="column row sensei-content-start">
    <div id="main">
        <?php
            if ( ! is_user_logged_in() ) : ?>

            <p>Please <a href="<?php get_site_url(); ?>/log-in/">log in</a> or <a href="<?php get_site_url(); ?>/register/">register</a> to begin lessons.</p>

            <?php endif; ?>