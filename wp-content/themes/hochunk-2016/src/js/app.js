(function ($) {
    $(document).foundation();

    // Re-run equalizer after a tab switch.
    var elem = new Foundation.Equalizer($('.content-inner'));
    $('.tabs').on('change.zf.tabs', function () {
        Foundation.reInit('equalizer');
    });

    // Give a scroll to the top of the learning material tabs.
    $(".to-top").click(function () {
        $('html, body').animate({
            scrollTop: $("#learning-tabs").offset().top
        }, 400);
    });


    // If there is a #TABNAME in the address bar, open that tab.
    var chosenTab = GetURLParameter('tab');

    // Create variable to get titles of the Tabs
    var optionTabs = [];
    $("[data-tabs] li>a").each(function () {
        optionTabs.push($(this).attr('aria-controls'))
    });

    // Check the chosenTab to what tabs are available.
    if (jQuery.inArray(chosenTab, optionTabs) !== -1) {
        new Foundation.Tabs($('[data-tabs]')).selectTab($('#' + chosenTab));
    }

    // Check the URL to see if variable is used.
    function GetURLParameter(sParam) {
        var sPageURL = window.location.search.substring(1);
        var sURLVariables = sPageURL.split('&');
        for (var i = 0; i < sURLVariables.length; i++) {
            var sParameterName = sURLVariables[i].split('=');
            if (sParameterName[0] == sParam) {
                return sParameterName[1];
            }
        }
    }

    // When appropriate (function exists) remove the "Complete Lesson"
    // button from a sensei lesson screen
    if (typeof removeCompleteButtonForm == 'function') {
        removeCompleteButtonForm();
    }

})(jQuery);