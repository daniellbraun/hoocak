(function ($) {
    var surveyWidget = $('.survey-widget');

    // Toggle widget on open/close.
    $('.survey-widget-button').click( function() {
        surveyWidget.toggleClass('open');
        surveyWidget.toggleClass('sticky');

        var width = $(window).width();
        if (width < 1024) {
            $("html, body").animate({ scrollTop: 0 }, "slow");
        }

    });

    $('.survey-widget-form button.close').click( function() {
        surveyWidget.removeClass('open');
    });


    // Stick the res widget bar to top, after you scroll past the nav.
    $(window).scroll(function() {

        var scroll = $(window).scrollTop();

        if (scroll > 44) {
            if (!surveyWidget.hasClass("open")) {
                surveyWidget.addClass("sticky"); // you don't need to add a "." in before your class name
            }

            // $('body').addClass('sticky-header');
        } else {
            surveyWidget.removeClass("sticky");
            // $('body').removeClass('sticky-header');
        }
    });
})(jQuery);