(function ($) {

    var $playlists = $('#video');

    // If the element we need to attach videos to is not present, get out of here.
    if ($playlists.length == 0) return;

    var playlistsParams = {
        part: 'contentDetails,id,snippet',
        channelId: 'UCIsT207S9iCdPpwxbNWqJ4Q',
        maxResults: 50
    };

    function init() {

        // 2. Initialize the JavaScript client library.
        gapi.client.init({
            'apiKey': 'AIzaSyCMUy8kgV7h57fbUHXQjmqj1u5nF_RTSQU',
            'discoveryDocs': ['https://www.googleapis.com/discovery/v1/apis/youtube/v3/rest']

        }).then(function () {
            // 3. Initialize and make the API request.

            return gapi.client.youtube.playlists.list(playlistsParams);
        }).then(function (resp) {
            listPlaylists(resp.result.items);

        }, function (reason) {
            console.log('Error: ' + reason.result.error.message);
        });

    }

    // function setupPaginationUI() {
    //     $playlistContainer.append(
    //         '<div class="button previous-videos">Previous Page</div>'
    //     );
    //     $('.previous-videos').on('click', function () {
    //         listVideos(playlistId, title, prevPageToken);
    //     });
    //
    //     $playlistContainer.append(
    //         '<div class="button next-videos">Next Page</div>'
    //     );
    //     $('.next-videos').on('click', function () {
    //         console.log("click?");
    //         listVideos(playlistId, title, nextPageToken);
    //     });
    //
    // }

    function listPlaylists(playlists) {

        $.each(playlists, function (index, playlist) {
            var title = playlist.snippet.title;
            listVideos(playlist.id, title);
        });
    }

    function listVideos(playlistId, title, pageToken) {

        var playlistParams = {
            part: 'contentDetails,id,snippet',
            maxResults: 50,
            playlistId: playlistId,
            pageToken: pageToken
        };

        gapi.client.youtube.playlistItems.list(playlistParams).then(function (resp) {

            var playlist = resp.result;

            var nextPageToken = playlist.nextPageToken;
            var prevPageToken = playlist.prevPageToken;

            $playlists.append('<div class="row column youtube-playlist-heading text-center"><h2>' + title + '</h2></div>');
            $playlists.append('<div id="' + playlistId + '" class="row small-up-1 medium-up-2 large-up-3 text-center" ' +
                'data-equalize-on="medium" data-equalizer data-equalize-by-row="true"></div>');

            var playlistDivId = '#' + playlistId;
            var $playlistContainer = $(playlistDivId);

            $playlistContainer.empty();

            $.each(playlist.items, function (index, item) {
                var id = item.contentDetails.videoId;
                var title = item.snippet.title;

                $playlistContainer.append(
                    '<div class="columns text-center">' +
                    '<div class="content" data-equalizer-watch>' +
                    '<div class="top-image-featured-content">' +
                    '<div class="featured-video flex-video">' +
                    '<iframe class= "lazyload" width="200" height="113" data-src="https://www.youtube.com/embed/' + id + '" frameborder="0" allowfullscreen></iframe>' +
                    '</div>' +
                    '<div class="small-pattern-divider"></div>' +
                    '</div>' +
                    '<div class="text-featured-content">' +
                    '<h4 class="text-black">' + title + '</h4>' +
                    '</div>' +
                    '<div class="info-card-link text-upper">' +
                    '<a target="_blank" href="https://www.youtube.com/watch?v=' + id + '">WATCH ON YOUTUBE</a>' +
                    '</div>' +
                    '</div>' +
                    '</div>');
            });
            if (prevPageToken) {
                $playlistContainer.append(
                    '<div class="button previous-videos ' + playlistId + '">Previous Page</div>'
                );
                $('.previous-videos ' + playlistId).on('click', function () {
                    listVideos(playlistId, title, prevPageToken);
                });
            }
            if (nextPageToken) {
                $playlistContainer.append(
                    '<div class="button next-videos ' + playlistId + '">Next Page</div>'
                );
                $('.next-videos ' + playlistId).on('click', function () {
                    listVideos(playlistId, title, nextPageToken);
                });
            }
        });
    }

    // 1. Load the JavaScript client library.
    gapi.load('client', init);

})(jQuery);