<?php
/**
 * Template Name: Games Template
 *
 * The template for games page
 */

$context = Timber\Timber::get_context();
$post = new Timber\Post();
$context['post'] = $post;

$args = array(
    'post_type' => 'adl_learning',
);

$context['learning_activity'] = Timber\Timber::get_posts($args);
$context['difficulty'] = Timber\Timber::get_terms('adl_learning_difficulty');

if (is_page('family-in-the-home')) {
    $context['width'] = 1200;
    $context['height'] = 787;
    $context['bg_class'] = "gamez-family";
} else {
    $context['width'] = 1200;
    $context['height'] = 600;
    $context['bg_class'] = "gamez";
}

if ($_SESSION['game_progress']) {
    $context['game_progress'] = $_SESSION['game_progress'];
}

Timber\Timber::render(array('template-games.twig', 'page.twig'), $context);