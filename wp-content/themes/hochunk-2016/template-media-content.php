<?php
/**
 * The template for displaying the grill or bar menu page.
 *
 * @package WordPress
 */

/*
 Template Name: Media and Content
*/

$context         = Timber\Timber::get_context();
$post            = new Timber\Post();
$context['post'] = $post;

Timber\Timber::render( array( 'template-media-content.twig', 'page.twig' ), $context );
